using System.Reflection;
using UnityEngine;

namespace VimCore.Runtime.MVVM
{
    [HierarchyTag]
    [DisallowMultipleComponent]
    public abstract class ModelBehaviour: MonoBehaviour
    {
        private const BindingFlags BindingAttr = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
        
        public Observable GetObservable(string key)
        {
            var info = GetType().GetProperty(key, BindingAttr);
            if (info == null)
            {
                Debug.LogWarning($"ObservableChannel {key} in {GetType().Name} not found (Property required)",this);
                return null;
            }
            return info.GetValue(this) as Observable;
        }
        public Observable<T> GetObservable<T>(string key)
        {
            var info = GetType().GetProperty(key, BindingAttr);
            if (info == null)
            {
                Debug.LogWarning($"ObservableData<{typeof(T).Name}> {key} in {GetType().Name} not found (Property required)",this);
                return null;
            }
            
            return info.GetValue(this) as Observable<T>;
        }
    }
}