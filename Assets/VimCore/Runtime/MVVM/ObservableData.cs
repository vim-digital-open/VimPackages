using System;
using System.Collections.Generic;

namespace VimCore.Runtime.MVVM
{
    public class ObservableData<T>: Observable<T>
    {
        private T _value;
        private Action<T> _onValue;

        public ObservableData(T value = default) => _value = value;

        public T Value
        {
            get => _value;
            set 
            {
                if (EqualityComparer<T>.Default.Equals(_value, value)) return;
                _value = value;
                if (Equals(_onValue, null)) return;
                _onValue.Invoke(_value);
            }
        }        
        public override void Invoke(T value) => Value = value;

        public override void AddListener(Action<T> listener, bool silent = false)
        {
            _onValue -= listener;
            _onValue += listener;
            if(silent)return;
            listener?.Invoke(_value);
        }

        public void Touch() => _onValue?.Invoke(_value);        

        public bool NotNull => !Equals(_value, null);
        public bool IsNull => Equals(_value, null);
    }
}