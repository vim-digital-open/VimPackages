using UnityEngine.UI;

namespace VimCore.Runtime.MVVM.ViewModels
{
    public class VMScrollRectEnabled : AViewModel<bool>
    {
        private ScrollRect _rect;
        public ScrollRect Rect => _rect ??= GetComponent<ScrollRect>();

        public override void OnValue(bool value)
        {
            Rect.enabled = value;
        }
    }
}