using DG.Tweening;
using UnityEngine;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels.Tweening
{
    public class VMTweenBounceScaleRotate : AViewModel
    {
        public ParticleSystem fx;
        
        public override void OnValue()
        {
            var Tf = transform;
            var initRot = Tf.localEulerAngles;
            var upperRot = initRot + Vector3.up * 360f;

            DOTween.Sequence().AppendLerp(0.5f, ez =>
            {
                Tf.localEulerAngles = Vector3.Lerp(initRot, upperRot, ez.QuintOut);
                Tf.localScale = Vector3.LerpUnclamped(Vector3.one, Vector3.one*2, ez.Bounce);
            }).AppendCallback(() =>
            {
                if (fx)
                    fx.PlayOneShot(Tf.position);
            }).AppendLerp(ez =>
            {
                Tf.localScale = new Vector3(1 + ez.Bounce / 3, 1 - ez.Bounce / 2, 1 + ez.Bounce / 3);
            });
        }
    }
}