using DG.Tweening;
using UnityEngine;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels.Tweening
{
    public class VMTweenBounce : AViewModel
    {
        private Sequence _sequence;

        public override void OnValue()
        {
            var tf = transform;

            _sequence?.Kill();
            _sequence = DOTween.Sequence().AppendLerp(ez =>
            {
                tf.localScale = new Vector3(1 + ez.Bounce / 3, 1 - ez.Bounce / 2, 1 + ez.Bounce / 3);
            });
        }
    }
}