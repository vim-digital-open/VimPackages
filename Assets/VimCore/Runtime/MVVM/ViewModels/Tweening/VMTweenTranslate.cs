using DG.Tweening;
using UnityEngine;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels.Tweening
{
    public class VMTweenTranslate : AViewModel
    {
        public float duration = 0.3f;
        public AnimationCurve xCurve;
        public AnimationCurve yCurve;
        public AnimationCurve zCurve;

        private Sequence _ez;

        public override void OnValue()
        {
            _ez.Kill(true);
            _ez = DOTween.Sequence().AppendLerp(duration, ez =>
            {
                var t = ez.Linear;
                var x = xCurve.Evaluate(t);
                var y = yCurve.Evaluate(t);
                var z = zCurve.Evaluate(t);
                transform.localPosition = new Vector3(x, y, z);
            });
        }
    }
}
