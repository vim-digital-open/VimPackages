using DG.Tweening;
using UnityEngine;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels.Tweening
{
    public class VMTweenToggleAngle: AViewModel<bool>
    {
        public float duration = 0.3f;
        public Vector3 onTrue;
        public Vector3 onFalse;
        
        private Sequence _ez;

        public override void OnValue(bool value)
        {
            _ez?.Kill(true);
            var angleFrom = transform.localEulerAngles;
            var angleTo = value ? onTrue : onFalse;
            _ez = DOTween.Sequence().AppendLerp(duration, ez =>
            {
                transform.localEulerAngles = Vector3.LerpUnclamped(angleFrom, angleTo, ez.BackOut);
            });
        }
    }
}