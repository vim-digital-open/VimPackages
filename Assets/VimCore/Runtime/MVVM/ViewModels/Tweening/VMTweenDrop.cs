using DG.Tweening;
using UnityEngine;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels.Tweening
{
    public class VMTweenDrop : AViewModel
    {
        public UnityEngine.ParticleSystem fx;
        
        public override void OnValue()
        {
            var Tf = transform;
            var initPos = Tf.localPosition;
            var initRot = Tf.localEulerAngles;
            var upperPos = initPos + Vector3.up * 2;
            var upperRot = initRot + Vector3.forward * 45f;

            Tf.localPosition = upperPos;
            DOTween.Sequence().AppendLerp(ez =>
            {
                Tf.localPosition = Vector3.Lerp(initPos, upperPos, ez.QuintOut);
                Tf.localEulerAngles = Vector3.Lerp(initRot, upperRot, ez.QuadOut);
                Tf.localScale = Vector3.LerpUnclamped(Vector3.zero, Vector3.one, ez.BackOut);
            }).AppendLerp(0.15f, ez =>
            {
                Tf.localPosition = Vector3.Lerp(upperPos, initPos, ez.QuadOut);
                Tf.localEulerAngles = Vector3.Lerp(upperRot, initRot, ez.QuadOut);
            }).AppendCallback(() =>
            {
                if (fx)
                    fx.PlayOneShot(Tf.position);
            }).AppendLerp(ez =>
            {
                Tf.localScale = new Vector3(1 + ez.Bounce / 3, 1 - ez.Bounce / 2, 1 + ez.Bounce / 3);
            });
        }
    }
}
