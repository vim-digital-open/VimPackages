using DG.Tweening;
using UnityEngine;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels.Tweening
{
    public class VMTweenToggleScale: AViewModel<bool>
    {
        public float duration = 0.3f;
        public Vector3 onTrue;
        public Vector3 onFalse;
        
        private Sequence _sequence;

        public override void OnValue(bool value)
        {
            _sequence?.Kill();
            var scaleFrom = transform.localScale;
            var scaleTo = value ? onTrue : onFalse;
            _sequence = DOTween.Sequence().AppendLerp(duration, ez =>
            {
                transform.localScale = Vector3.LerpUnclamped(scaleFrom, scaleTo, ez.BackOut);
            });
        }
    }
}