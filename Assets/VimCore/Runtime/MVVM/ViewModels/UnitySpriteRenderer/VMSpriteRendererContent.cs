using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.UnitySpriteRenderer
{
    public class VMSpriteRendererContent : AViewModel<Sprite>
    {

        private SpriteRenderer _renderer;
        public SpriteRenderer Renderer => _renderer ??= GetComponent<SpriteRenderer>();

        public override void OnValue(UnityEngine.Sprite value)
        {
            Renderer.sprite = value;
        }
    }
}