﻿using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.UnitySpriteRenderer
{
    public class VMSpriteColor : AViewModel<Color>
    {

        private SpriteRenderer _renderer;
        public SpriteRenderer Renderer => _renderer ??= GetComponent<SpriteRenderer>();

        public override void OnValue(Color value)
        {
            Renderer.color = value;
        }
    }
}
