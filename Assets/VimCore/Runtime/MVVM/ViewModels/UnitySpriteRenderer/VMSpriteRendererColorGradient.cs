using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.UnitySpriteRenderer
{
    public class VMSpriteRendererColorGradient : AViewModel<float>
    {

        private SpriteRenderer _renderer;
        public SpriteRenderer Renderer => _renderer ??= GetComponent<SpriteRenderer>(); 
        
        public Gradient gradient;
        public override void OnValue(float value) => Renderer.color = gradient.Evaluate(value);
    }
}