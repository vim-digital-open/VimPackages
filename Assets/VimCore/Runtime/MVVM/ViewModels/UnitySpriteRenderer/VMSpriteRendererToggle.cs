using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.UnitySpriteRenderer
{
    public class VMSpriteRendererToggle : AViewModel<bool>
    {
        public Sprite onTrue;
        public Sprite onFalse;


        private SpriteRenderer _renderer;
        public SpriteRenderer Renderer => _renderer ??= GetComponent<SpriteRenderer>();

        public override void OnValue(bool value)
        {
            Renderer.sprite = value ? onTrue : onFalse;
        }
    }
}