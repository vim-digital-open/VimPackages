using UnityEngine.UI;

namespace VimCore.Runtime.MVVM.ViewModels
{
    public class VMLayoutGroupEnabled : AViewModel<bool>
    {
        private LayoutGroup _group;
        public LayoutGroup Group => _group ??= GetComponent<LayoutGroup>();

        public override void OnValue(bool value)
        {
            Group.enabled = value;
        }
    }
}