using Cysharp.Threading.Tasks;
using UnityEngine.UI;

namespace VimCore.Runtime.MVVM.ViewModels.CanvasSlider
{
    public class VMBindSlider: AViewModel<float>
    {
        private Slider _component;
        protected Slider Component => _component ??= GetComponent<Slider>();
        private async void Awake()
        {
            await UniTask.Yield();
            Component.onValueChanged.AddListener(OnSlider);
        }
        
        private void OnSlider(float value) => Observable.Invoke(value);

        public override void OnValue(float value)
        {
            Component.value = value;
        }
    }
}
