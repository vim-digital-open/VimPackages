using System;

namespace VimCore.Runtime.MVVM.ViewModels
{
    public class VMActiveInt : AViewModel<int>
    {
        public ComparsionMode mode = ComparsionMode.Exact;
        public int condition;

        public override void OnValue(int value)
        {
            var newState = mode switch
            {
                ComparsionMode.Exact => value == condition,
                ComparsionMode.Min => value >= condition,
                ComparsionMode.Max => value <= condition,
                _ => throw new ArgumentOutOfRangeException()
            };

            if (gameObject == null) return;

            gameObject.SetActive(newState);
        }
    }
}
