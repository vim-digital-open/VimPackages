using System;
using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Particles
{
    public class VMParticleActiveFloat : AViewModel<float>
    {
        public ComparsionMode mode = ComparsionMode.Exact;
        public float condition;

        private ParticleSystem _system;
        public ParticleSystem System => _system ??= GetComponent<ParticleSystem>();

        public override void OnValue(float value)
        {
            var newState = mode switch
            {
                ComparsionMode.Exact => Math.Abs(value - condition) < float.Epsilon,
                ComparsionMode.Min => value >= condition,
                ComparsionMode.Max => value <= condition,
                _ => throw new ArgumentOutOfRangeException()
            };
            if (newState)
            {
                if(System.isPlaying)return;
                System.Play();
            }
            else
            {
                if(!System.isPlaying)return;
                System.Stop();
            }
        }
            
    }
}