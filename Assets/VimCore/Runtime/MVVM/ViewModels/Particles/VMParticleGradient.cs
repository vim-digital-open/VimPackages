using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Particles
{
    public class VMParticleGradient : AViewModel<float>
    {
        public Gradient colorRange;

        private ParticleSystem _system;
        public ParticleSystem System => _system ??= GetComponent<ParticleSystem>();

        public override void OnValue(float value)
        {
            var main = System.main;
            main.startColor = colorRange.Evaluate(value);
        }
    }
}
