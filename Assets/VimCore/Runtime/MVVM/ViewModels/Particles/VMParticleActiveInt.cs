using System;
using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Particles
{
    public class VMParticleActiveInt : AViewModel<int>
    {
        public ComparsionMode mode = ComparsionMode.Exact;
        public int condition;

        private ParticleSystem _system;
        public ParticleSystem System => _system ??= GetComponent<ParticleSystem>();

        public override void OnValue(int value)
        {
            var newState = mode switch
            {
                ComparsionMode.Exact => value == condition,
                ComparsionMode.Min => value >= condition,
                ComparsionMode.Max => value <= condition,
                _ => throw new ArgumentOutOfRangeException()
            };
            if (newState)
            {
                if(System.isPlaying)return;
                System.Play();
            }
            else
            {
                if(!System.isPlaying)return;
                System.Stop();
            }
        }
            
    }
}