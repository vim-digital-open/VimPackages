using UnityEngine;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels.Particles
{
    public class VMParticleOneShot : AViewModel
    {
        public ParticleSystem fx;
        public override async void OnValue()
        {
            await fx.PlayOneShot(transform.position,transform.rotation,transform.lossyScale);
        }
    }
}