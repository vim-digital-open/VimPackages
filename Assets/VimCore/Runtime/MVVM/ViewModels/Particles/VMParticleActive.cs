﻿using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Particles
{
    public class VMParticleActive : AViewModel<bool>
    {
        public bool invert;

        private ParticleSystem _system;
        public ParticleSystem System => _system ??= GetComponent<ParticleSystem>();

        public override void OnValue(bool value)
        {
            if (value!=invert)
                System.Play();
            else
                System.Stop();
        }
    }
}