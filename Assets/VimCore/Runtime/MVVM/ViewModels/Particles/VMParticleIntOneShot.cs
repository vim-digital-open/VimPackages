using System;
using UnityEngine;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels.Particles
{
    public class VMParticleIntOneShot : AViewModel<int>
    {
        public ParticleSystem fx;
        public ComparsionMode mode = ComparsionMode.Exact;
        public int condition;

        public override async void OnValue(int value)
        {
            var newState = mode switch
            {
                ComparsionMode.Exact => value == condition,
                ComparsionMode.Min => value >= condition,
                ComparsionMode.Max => value <= condition,
                _ => throw new ArgumentOutOfRangeException()
            };
            if (newState) 
                await fx.PlayOneShot(transform.position, transform.rotation, transform.lossyScale);
        }
    }
}