using UnityEngine;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels.Particles
{
    public class VMParticleBoolOneShot : AViewModel<bool>
    {
        public ParticleSystem fx;
        public bool condition;

        public override async void OnValue(bool value)
        {
           
            if (value == condition) 
                await fx.PlayOneShot(transform.position, transform.rotation, transform.lossyScale);
        }
    }
}