using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Particles
{
    public class VMParticleColor : AViewModel<Color>
    {

        private ParticleSystem _system;
        public ParticleSystem System => _system ??= GetComponent<ParticleSystem>();

        public override void OnValue(Color value)
        {
            var main = System.main;
            main.startColor = value;
        }
    }
}
