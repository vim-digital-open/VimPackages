﻿using TMPro;
using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Label
{
    public class VMLabel : AViewModel<string>
    {
        [TextArea]
        public string mask = "{0}";

        private TMP_Text _label;
        public TMP_Text Label => _label ??= GetComponent<TMP_Text>(); 

        private void Awake()
        {
            Label.isTextObjectScaleStatic = true;
        }

        public override void OnValue(string value)
        {
            if (Label)
                Label.text = string.Format(mask, value);
            Label.isTextObjectScaleStatic = false;
        }
    }
}