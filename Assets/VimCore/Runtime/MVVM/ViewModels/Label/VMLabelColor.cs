using TMPro;
using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Label
{
    public class VMLabelColor : AViewModel<Color>
    {

        private TMP_Text _label;
        public TMP_Text Label => _label ??= GetComponent<TMP_Text>();

        public override void OnValue(Color value)
        {
            Label.color = value;
        }
    }
}