﻿using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels
{
    public class VMActive : AViewModel<bool>
    {
        public bool invert;

        public override void OnValue(bool value)
        {
            gameObject.SetActive(value != invert);
        }
    }
}