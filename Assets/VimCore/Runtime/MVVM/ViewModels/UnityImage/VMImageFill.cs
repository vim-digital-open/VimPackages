using UnityEngine.UI;

namespace VimCore.Runtime.MVVM.ViewModels.UnityImage
{
    public class VMImageFill : AViewModel<float>
    {
        private Image _img;
        public Image Img => _img ??= GetComponent<Image>();

        public override void OnValue(float value)
        {
            Img.fillAmount = value;
        }
    }
}
