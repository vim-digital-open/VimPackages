﻿using UnityEngine;
using UnityEngine.UI;

namespace VimCore.Runtime.MVVM.ViewModels.UnityImage
{
    public class VMImageColor : AViewModel<Color>
    {
        private Image _img;
        public Image Img => _img ??= GetComponent<Image>();

        public override void OnValue(Color value)
        {
            Img.color = value;
        }

    }
}