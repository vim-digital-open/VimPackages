using UnityEngine;
using UnityEngine.EventSystems;

namespace VimCore.Runtime.MVVM.ViewModels.UnityImage
{
    public class VMImageClick: AViewModel, 
        IPointerDownHandler, 
        IPointerUpHandler, 
        IPointerClickHandler
    {
        private CanvasRenderer _renderer;
        public CanvasRenderer Renderer => _renderer ??= GetComponent<CanvasRenderer>(); 
        public void OnPointerDown(PointerEventData eventData) => Renderer.SetColor(Color.gray);
        public void OnPointerUp(PointerEventData eventData) => Renderer.SetColor(Color.white);
        public void OnPointerClick(PointerEventData eventData) => Observable.Invoke();
    }
}