﻿using UnityEngine;
using UnityEngine.UI;

namespace VimCore.Runtime.MVVM.ViewModels.UnityImage
{
    public class VMImageColorGradient : AViewModel<float>
    {
        public Gradient gradient;
        private Image _img;
        public Image Img => _img ??= GetComponent<Image>();
        public override void OnValue(float value) => Img.color = gradient.Evaluate(value);
    }
}