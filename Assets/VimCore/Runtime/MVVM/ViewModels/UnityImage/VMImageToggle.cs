﻿using UnityEngine.UI;

namespace VimCore.Runtime.MVVM.ViewModels.UnityImage
{
    public class VMImageToggle : AViewModel<bool>
    {
        public UnityEngine.Sprite onTrue;
        public UnityEngine.Sprite onFalse;

        private Image _img;
        public Image Img => _img ??= GetComponent<Image>();

        public override void OnValue(bool value)
        {
            Img.sprite = value ? onTrue : onFalse;
        }
    }
}
