﻿using UnityEngine.UI;

namespace VimCore.Runtime.MVVM.ViewModels.UnityImage
{
    public class VMImageContent : AViewModel<UnityEngine.Sprite>
    {
        private Image _img;
        public Image Img => _img ??= GetComponent<Image>();

        public override void OnValue(UnityEngine.Sprite value)
        {
            Img.sprite = value;
        }
    }
}
