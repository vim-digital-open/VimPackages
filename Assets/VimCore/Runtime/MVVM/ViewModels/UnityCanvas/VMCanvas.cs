using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace VimCore.Runtime.MVVM.ViewModels.UnityCanvas
{
    
    public class VMCanvas : AViewModel<bool>
    {
        protected CanvasGroup Cg;
        protected CanvasScaler Cs;
        protected BaseRaycaster Gr;
        
        private UnityEngine.Canvas _canvas;
        public UnityEngine.Canvas Canvas => _canvas ??= GetComponent<UnityEngine.Canvas>(); 

        private void Awake()
        {
            if(!gameObject.TryGetComponent(out Cg))
                Cg = gameObject.AddComponent<CanvasGroup>();
            gameObject.TryGetComponent(out Cs);
            gameObject.TryGetComponent(out Gr);
            Canvas.enabled = false;
            Cg.enabled = false;
            if(Cs)
                Cs.enabled = false;
            if (Gr) 
                Gr.enabled = false;
        }

        public override void OnValue(bool value)
        {
            if(Canvas.enabled == value) return;
            Canvas.enabled = value;
            Cg.enabled = value;
            if(Cs)
                Cs.enabled = value;
            if (Gr) 
                Gr.enabled = value;
        }
    }
}