using DG.Tweening;
using UnityEngine;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels.UnityCanvas
{
    public class VMCanvasTranslate : VMCanvas
    {
        public Vector2 offscreen;

        private Sequence _sequence;
        
        private RectTransform _window;
        protected RectTransform Window => _window ??= transform.GetChild(0) as RectTransform;

        public override void OnValue(bool value)
        {
            if (Canvas.enabled == value) return;
            Canvas.enabled = true;
            Cg.enabled = true;
            if (Cs) Cs.enabled = true;
            if (Gr) Gr.enabled = false;

            var colorFrom = Cg.alpha;
            var posFrom = value ? offscreen :  Vector2.zero;

            var colorTo = value ? 1 : 0;
            var posTo = value ? Vector2.zero : offscreen;
            
            _sequence?.Kill(true);
            _sequence = DOTween.Sequence().AppendLerp(ez =>
            {
                Window.anchoredPosition = Vector2.LerpUnclamped(posFrom, posTo, ez.BackInOut);
                Cg.alpha = Mathf.Lerp(colorFrom, colorTo, ez.Linear);
            }).AppendCallback(() =>
            {
                Window.anchoredPosition = value ? Vector2.zero : offscreen;
                Canvas.enabled = value;
                Cg.enabled = value;
                Cg.alpha = value ? 1 : 0;
                if (Gr) Gr.enabled = value;
                if (Cs) Cs.enabled = value;
            });
        }
    }
}