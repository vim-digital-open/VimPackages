using UnityEngine.UI;

namespace VimCore.Runtime.MVVM.ViewModels
{
    public class VMLayoutElementExclude : AViewModel<bool>
    {
        public bool invert;
        private LayoutElement _element;
        public LayoutElement Element => _element ??= GetComponent<LayoutElement>();

        public override void OnValue(bool value)
        {
            Element.ignoreLayout = value != invert;
        }
    }
}
