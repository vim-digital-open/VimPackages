using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Animation
{
    public class VMAnimatorIKGoal : AViewModel<bool>
    {
        private float _targetWeight;
        private float _weight;

        public AvatarIKGoal goal;
        public Transform target;
        
        private Animator _anim;
        public Animator Anim => _anim ??= GetComponent<Animator>();


        public override void OnValue(bool value) => _targetWeight = value ? 1 : 0;

        private void OnAnimatorIK(int layerIndex)
        {
            if (!target) return;
            _weight = Mathf.MoveTowards(_weight, _targetWeight, Time.deltaTime*5);
            Anim.SetIKPosition(goal, target.position);
            Anim.SetIKRotation(goal, target.rotation);
            Anim.SetIKPositionWeight(goal, _weight);
            Anim.SetIKRotationWeight(goal, _weight);
        }
    }
}
