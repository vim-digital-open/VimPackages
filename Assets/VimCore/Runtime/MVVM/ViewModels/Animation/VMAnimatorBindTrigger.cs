using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Animation
{
    public class VMAnimatorBindTrigger : AViewModel
    {
        public string parameter;
        
        private Animator _anim;
        public Animator Anim => _anim ??= GetComponent<Animator>(); 

        public override void OnValue() => Anim.SetTrigger(parameter);
    }
}