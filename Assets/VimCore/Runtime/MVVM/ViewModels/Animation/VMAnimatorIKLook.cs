using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Animation
{
    public class VMAnimatorIKLook : AViewModel<Transform>
    {
        public float lookWeight = 0.8f;

        private Transform _lookTarget;
        private Vector3 _lookPos;
        
        private Animator _anim;
        public Animator Anim => _anim ??= GetComponent<Animator>();


        public override void OnValue(Transform value)
        {
            _lookTarget = value;
        }

        private void LateUpdate()
        {
            _lookPos = Vector3.Lerp(_lookPos, _lookTarget.position, Time.deltaTime);
        }

        private void OnAnimatorIK(int layerIndex)
        {
            if (!_lookTarget) return;
            Anim.SetLookAtPosition(_lookPos);
            Anim.SetLookAtWeight(lookWeight);
        }
    }
}
