using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Animation
{
    public class VMAnimatorBindFloat : AViewModel<float>
    {
        public string parameter;
        
        private Animator _anim;
        public Animator Anim => _anim ??= GetComponent<Animator>();


        public override void OnValue(float value) => Anim.SetFloat(parameter, value);
    }
}