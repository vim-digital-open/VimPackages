using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Animation
{
    public class VMAnimatorBindInteger : AViewModel<int>
    {
        public string parameter;
        
        private Animator _anim;
        public Animator Anim => _anim ??= GetComponent<Animator>();


        public override void OnValue(int value) => Anim.SetInteger(parameter, value);
    }
}