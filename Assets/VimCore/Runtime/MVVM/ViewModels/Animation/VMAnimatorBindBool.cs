using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Animation
{
    public class VMAnimatorBindBool : AViewModel<bool>
    {
        public string parameter;
        
        private Animator _anim;
        public Animator Anim => _anim ??= GetComponent<Animator>();


        public override void OnValue(bool value) => Anim.SetBool(parameter, value);
    }
}