using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels
{
    public class VMCameraLayerMask : AViewModel<bool>
    {
        public LayerMask onTrue, onFalse;

        private Camera _cam;
        public Camera Cam => _cam ??= GetComponent<Camera>();

        public override void OnValue(bool value)
        {
            Cam.cullingMask = value ? onTrue : onFalse;
        }
    }
}
