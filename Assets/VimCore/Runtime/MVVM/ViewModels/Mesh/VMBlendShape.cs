using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Mesh
{
    public class VMBlendShape : AViewModel<float>
    {
        [SerializeField] private int index;
        [SerializeField] private float minValue = 0;
        [SerializeField] private float maxValue = 100;

        private SkinnedMeshRenderer _renderer;
        public SkinnedMeshRenderer Renderer => _renderer ??= GetComponent<SkinnedMeshRenderer>();

        public override void OnValue(float value)
        {
            var target = Mathf.Lerp(minValue,maxValue,value);
            Renderer.SetBlendShapeWeight(index,target);
        }
    }
}
