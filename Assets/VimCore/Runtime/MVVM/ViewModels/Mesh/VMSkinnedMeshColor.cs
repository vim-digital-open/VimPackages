using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Mesh
{
    public class VMSkinnedMeshColor : AViewModel<Color>
    {
        public int matIndex;
        private SkinnedMeshRenderer _renderer;
        public SkinnedMeshRenderer Renderer => _renderer ??= GetComponent<SkinnedMeshRenderer>();

        public override void OnValue(Color value)
        {
            Renderer.materials[matIndex].color = value;
        }
    }
}
