using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Mesh
{
    public class VMSkinnedMesh : AViewModel<UnityEngine.Mesh>
    {
        private SkinnedMeshRenderer _renderer;
        public SkinnedMeshRenderer Renderer => _renderer ??= GetComponent<SkinnedMeshRenderer>();

        public override void OnValue(UnityEngine.Mesh value)
        {
            Renderer.sharedMesh = value;
        }
    }
}
