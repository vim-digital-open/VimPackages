using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Mesh
{
    public class VMMesh : AViewModel<UnityEngine.Mesh>
    {
        private MeshFilter _renderer;
        public MeshFilter Filter => _renderer ??= GetComponent<MeshFilter>();

        public override void OnValue(UnityEngine.Mesh value)
        {
            Filter.sharedMesh = value;
        }
    }
}
