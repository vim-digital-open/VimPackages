using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Mesh
{
    public class VMMeshMaterial : AViewModel<Material>
    {
        private MeshRenderer _renderer;
        public MeshRenderer Renderer => _renderer ??= GetComponent<MeshRenderer>();

        public override void OnValue(Material value)
        {
            Renderer.material = value;
        }
    }
}
