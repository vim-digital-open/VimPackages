using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Mesh
{
    public class VMSkinnedMaterial : AViewModel<Material>
    {
        private SkinnedMeshRenderer _renderer;
        public SkinnedMeshRenderer Renderer => _renderer ??= GetComponent<SkinnedMeshRenderer>();

        public override void OnValue(Material value)
        {
            Renderer.material = value;
        }
    }
}
