using UnityEngine;

namespace VimCore.Runtime.MVVM.ViewModels.Mesh
{
    public class VMMeshColor : AViewModel<Color>
    {
        [SerializeField] private int matIndex;

        private MeshRenderer _renderer;
        public MeshRenderer Renderer => _renderer ??= GetComponent<MeshRenderer>();

        public override void OnValue(Color value)
        {
            Renderer.materials[matIndex].color = value;
        }
    }
}