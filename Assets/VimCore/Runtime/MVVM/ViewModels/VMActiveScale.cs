using DG.Tweening;
using UnityEngine;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCore.Runtime.MVVM.ViewModels
{
    public class VMActiveScale : AViewModel<bool>
    {
        public bool invert;
        public float duration = 0.3f;
        public ParticleSystem fxShow;
        public ParticleSystem fxHide;
        
        private Sequence _sequence;
        
        public override void OnValue(bool value)
        {
            var targetState = value != invert;
            gameObject.SetActive(true);
            var scaleFrom = transform.localScale;
            var scaleTo = targetState ? Vector3.one : Vector3.zero;
            var fx = targetState ? fxShow : fxHide;
            if(fx)
                fx.PlayOneShot(transform.position);
            
            _sequence?.Kill();
            _sequence = DOTween.Sequence().AppendLerp(duration, ez =>
            {
                transform.localScale = Vector3.Lerp(scaleFrom,scaleTo, ez.Linear);
            }).AppendCallback(() =>
            {
                gameObject.SetActive(targetState);
            });
        }
    }
}