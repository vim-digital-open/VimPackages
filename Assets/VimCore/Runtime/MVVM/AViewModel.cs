using UnityEngine;

namespace VimCore.Runtime.MVVM
{

    [HierarchyTag]
    public abstract class AViewModel : MonoBehaviour
    {
        [SerializeField]
        private string property;
        
        private ModelBehaviour _model;
        public ModelBehaviour Model => _model ??= GetComponentInParent<ModelBehaviour>(true);

        private Observable _observable;
        public Observable Observable => _observable ??= Model ? Model.GetObservable(property) : null;
        
        private void Start() => Observable?.AddListener(OnValue);
        private void OnDestroy() => Observable?.RemoveListener(OnValue);

        public virtual void OnValue() { }
    }

    [HierarchyTag]
    public abstract class AViewModel<TData> : MonoBehaviour
    {
        [SerializeField]
        private string property;

        private ModelBehaviour _model;
        public ModelBehaviour Model => _model ??= GetComponentInParent<ModelBehaviour>(true);

        private Observable<TData> _observable;
        public Observable<TData> Observable => _observable ??= Model ? Model.GetObservable<TData>(property) : null;
        
        private void Start() => Observable?.AddListener(OnValue);
        private void OnDestroy() => Observable?.RemoveListener(OnValue);

        public virtual void OnValue(TData value) { }
    }
}