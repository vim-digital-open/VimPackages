using System;

namespace VimCore.Runtime.MVVM
{
    public class Observable
    {
        private Action _onValue;
        
        public virtual void AddListener(Action listener, bool silent = false)
        {
            _onValue -= listener;
            _onValue += listener;
        }

        public void RemoveListener(Action listener)
        {
            _onValue -= listener;
        }

        public void Invoke() => _onValue?.Invoke();
    }
    
    public class Observable<T>
    {
        private Action<T> _onValue;
        
        public virtual void AddListener(Action<T> listener, bool silent = false)
        {
            _onValue -= listener;
            _onValue += listener;
        }

        public void RemoveListener(Action<T> listener)
        {
            _onValue -= listener;
        }
        
        public virtual void Invoke(T value) => _onValue?.Invoke(value);
    }
}