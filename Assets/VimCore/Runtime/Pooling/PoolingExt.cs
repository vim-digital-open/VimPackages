using System;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace VimCore.Runtime.Pooling
{
    public static class PoolingExt
    {
        public static T SpawnPoolable<T>(this T prefab) where T : Component
        {
            var pool = PrefabPool<T>.Instance(prefab);
            return pool.Spawn();
        }

        public static void RemovePoolable<T>(this T prefab, T poolable) where T : Component
        {
            var pool = PrefabPool<T>.Instance(prefab);
            pool.Remove(poolable);
        }
        
        public static async void PlayOneShot(this ParticleSystem prefab, Vector3 position)
        {
            var pool = PrefabPool<ParticleSystem>.Instance(prefab);
            var result = pool.Spawn();
            result.transform.position = position;
            result.Play();
            await UniTask.Delay(TimeSpan.FromSeconds(prefab.main.duration));
            pool.Remove(result);
        }

        public static async Task PlayOneShot(this ParticleSystem prefab, Vector3 position, Quaternion rotation, Vector3 scale)
        {
            var pool = PrefabPool<ParticleSystem>.Instance(prefab);
            var result = pool.Spawn();
            var tf = result.transform;
            tf.position = position;
            tf.rotation = rotation;
            tf.localScale = scale;
            result.Play();
            await UniTask.Delay(TimeSpan.FromSeconds(prefab.main.duration));
            pool.Remove(result);
        }
    }
}