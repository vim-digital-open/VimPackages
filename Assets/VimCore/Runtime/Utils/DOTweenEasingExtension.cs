using System;
using DG.Tweening;
using UnityEngine;

namespace VimCore.Runtime.Utils
{
    public static class DOTweenEasingExtension
    {
        public static Sequence PrependLerp(this Sequence self, float duration, Action<EasingData> action)
        {
            return self.Prepend(DOTween.To(OnTick, 0, 1, duration));
            void OnTick(float progress) => action?.Invoke(new EasingData(progress));
        }

        public static Sequence PrependLerp(this Sequence self, Action<EasingData> action) => self.PrependLerp(0.3f, action);
        
        public static Sequence AppendLerp(this Sequence self, float duration, Action<EasingData> action)
        {
            return self.Append(DOTween.To(OnTick, 0, 1, duration));
            void OnTick(float progress) => action?.Invoke(new EasingData(progress));
        }

        public static Sequence AppendLerp(this Sequence self, Action<EasingData> action) => self.AppendLerp(0.3f, action);

        public class EasingData
        {
            public float Linear { get; }
        
            public float Bounce => EasingCurve.Bounce(Linear);
            public float QuadIn => EasingCurve.QuadIn(Linear);
            public float QuadOut => EasingCurve.QuadOut(Linear);
            public float QuadInOut => EasingCurve.QuadInOut(Linear);
            public float QuadOutIn => EasingCurve.QuadOutIn(Linear);

            public float QuintIn => EasingCurve.QuintIn(Linear);
            public float QuintOut => EasingCurve.QuintOut(Linear);
            public float QuintInOut => EasingCurve.QuintInOut(Linear);
            public float QuintOutIn => EasingCurve.QuintOutIn(Linear);

            public float BackIn => EasingCurve.BackIn(Linear);
            public float BackOut => EasingCurve.BackOut(Linear);
            public float BackInOut => EasingCurve.BackInOut(Linear);
            public float BackOutIn => EasingCurve.BackOutIn(Linear);

            public EasingData(float progress)
            {
                Linear = progress;
            }
        }
        public static class EasingCurve
        {
            //Quad
            public static float QuadIn(float t) => t * t;
            public static float QuadOut(float t) => 1 - (1 - t) * (1 - t);
            public static float QuadInOut(float t) => Mathf.Lerp(QuadIn(t),QuadOut(t),t);
            public static float QuadOutIn(float t) => Mathf.Lerp(QuadIn(t),QuadOut(t),t-1);

            //Quint
            public static float QuintIn(float t) => t * t * t * t * t;
            public static float QuintOut(float t) => 1 - (1 - t) * (1 - t) * (1 - t) * (1 - t) * (1 - t);
            public static float QuintInOut(float t) => Mathf.Lerp(QuintIn(t),QuintOut(t),t);
            public static float QuintOutIn(float t) => Mathf.Lerp(QuintIn(t),QuintOut(t),t-1);

            //Back
            private const float B1 = 1.70158f;
            private const float B2 = 2.70158f;
            public static float BackIn(float t) => B2 * t * t * t - B1 * t * t;
            public static float BackOut(float t) => 1 - BackIn(1 - t);
            public static float BackInOut(float t) => Mathf.Lerp(BackIn(t),BackOut(t),t);
            public static float BackOutIn(float t) => Mathf.Lerp(BackIn(t),BackOut(t),t-1);

            public static float Bounce(float t) => t * (1 - t);
        }
    }
}