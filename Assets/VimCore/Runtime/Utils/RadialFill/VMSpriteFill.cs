using UnityEngine;
using VimCore.Runtime.MVVM;

namespace VimCore.Runtime.Utils.RadialFill
{
    public class VMSpriteFill : AViewModel<float>
    {
        public bool invert;

        private static readonly int Arc1 = Shader.PropertyToID("_Arc1");

        private SpriteRenderer _renderer;
        public SpriteRenderer Renderer => _renderer ??= GetComponent<SpriteRenderer>();

        public override void OnValue(float value)
        {
            if (invert)
                value = 1 - value;
        
            var angle = Mathf.Lerp(0, 360, value);
            Renderer.material.SetFloat(Arc1, angle);
        }

    }
}
