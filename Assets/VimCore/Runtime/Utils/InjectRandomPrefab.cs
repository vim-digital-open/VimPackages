using UnityEngine;

namespace VimCore.Runtime.Utils
{
    public class InjectRandomPrefab : MonoBehaviour
    {
        public Transform[] variants;

        private void Awake()
        {
            var selected = variants.GetRandomItem();
            var instance = Instantiate(selected, transform);
            instance.localPosition = Vector3.zero;
            instance.localRotation = Quaternion.identity;
            instance.localScale = Vector3.one;
            Destroy(this);
        }
    }
}