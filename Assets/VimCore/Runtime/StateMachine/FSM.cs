using System;

namespace VimCore.Runtime.StateMachine
{
    public class FSM<TSubject, TState> where TState: AState<TSubject, TState>
    {
        internal TSubject Subject { get; private set; }
        private TState _state;

        public void Init(TSubject ctx, TState initState)
        {
            Subject = ctx;
            EnterState(initState);
        }

        private void EnterState(TState newState)
        {
            _state = newState;
            _state.Init(this);
            _state?.Enter();
        }

        public TState State
        {
            get
            {
                if (_state == default)
                    throw new Exception("FSM not initialized");
                return _state;
            }
            set
            {
                _state?.Exit();
                EnterState(value);
            }
        }
    }
}