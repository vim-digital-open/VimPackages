namespace VimCore.Runtime.StateMachine
{
    public class AState<TSubject, TState> where TState: AState<TSubject, TState>
    {
        private FSM<TSubject, TState> _fsm;
        protected TSubject Subject => _fsm.Subject;
        internal void Init(FSM<TSubject, TState> fsm) => _fsm = fsm;

        protected void ChangeState(TState nextState) => _fsm.State = nextState;
        protected void ChangeState<TNewState>() where TNewState : TState, new() => ChangeState(new TNewState());

        public virtual void Enter() { }

        public virtual void Exit() { }
    }
}