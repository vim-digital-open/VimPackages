namespace VimCore.Runtime.DependencyManagement
{
    public static class Locator
    {
        private static class Storage<TInstance>
        {
            public static readonly ServiceContainer<TInstance> Single = new();
            public static readonly Filter<TInstance> Filter = new();
        }
        
        public static ServiceContainer<TInstance> Single<TInstance>() => Storage<TInstance>.Single;
        public static Filter<TInstance> Filter<TInstance>() => Storage<TInstance>.Filter;

        public static TInstance Resolve<TInstance>() => Single<TInstance>().Instance;
    }
}