using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using VimCore.Runtime.MVVM;

namespace VimCore.Editor
{
    [InitializeOnLoad]
    public static class HierarchyTags
    {
        private static GUIStyle _labelStyle;
        public static GUIStyle LabelStyle => _labelStyle ??= new GUIStyle(EditorStyles.label)
        {
            alignment = TextAnchor.MiddleRight,
            fontSize = 8
        };
        
        static HierarchyTags()
        {
            EditorApplication.hierarchyWindowItemOnGUI -= HighlightItems;
            EditorApplication.hierarchyWindowItemOnGUI += HighlightItems;
        }

        private static void HighlightItems(int instanceID, Rect selectionRect)
        {
            var target = EditorUtility.InstanceIDToObject(instanceID) as GameObject;
            if (target == null) return;
            var tags = GetTags(target);
            var labelText = string.Join(", ", tags);
            GUI.Label(selectionRect, labelText, LabelStyle);
        }

        private static string[] GetTags(GameObject target) => target.GetComponents<Component>()
            .Where(c=> c)
            .Select(component => component.GetType())
            .Where(type => type.GetCustomAttributes<HierarchyTagAttribute>(true).Any())
            .Select(type => type.Name)
            .ToArray();
    }
}
