using System;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace VimCore.Editor
{
    public static class TerminalUtil
    {
        public static void Say(string message) => Run($"say {message}");

        public static void Run(string argument)
        {
            try
            {
                var myProcess = new Process
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = "/bin/bash",
                        UseShellExecute = false,
                        RedirectStandardError = true,
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true,
                        Arguments = " -c \"" + argument + " \""
                    }
                };
                myProcess.Start();
                myProcess.WaitForExit();
                myProcess.StandardOutput.ReadToEnd();
                            
                Debug.Log(myProcess.StandardOutput.ReadToEnd());
                if(!myProcess.StandardError.EndOfStream)
                    Debug.LogError(myProcess.StandardError.ReadToEnd());
            }
            catch(Exception e)
            {
                Debug.Log(e);
            }
        }
    }
}