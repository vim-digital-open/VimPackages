using System;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace VimCore.Editor
{
    public class BuildPreprocessor : IPreprocessBuildWithReport
    {
        public int callbackOrder => int.MinValue;
    
        public void OnPreprocessBuild(BuildReport report)
        {
            PlayerSettings.SetAdditionalIl2CppArgs("--generic-virtual-method-iterations=5");
            PlayerSettings.companyName = "VIM Digital";
            PlayerSettings.Android.useCustomKeystore = true;
            PlayerSettings.Android.keyaliasPass = "qweasd123";
            PlayerSettings.Android.keystorePass = "qweasd123";
            if (EditorUserBuildSettings.buildAppBundle)
            {
                PlayerSettings.Android.bundleVersionCode += 1;
                PlayerSettings.bundleVersion = $"{PlayerSettings.Android.bundleVersionCode / 100f}";
            }
            else
            {
                var timeToken = DateTime.Now.ToString("MMdd-HHmm");
                PlayerSettings.bundleVersion = $"{PlayerSettings.Android.bundleVersionCode / 100f}({timeToken})";
            }
        }
    }
}