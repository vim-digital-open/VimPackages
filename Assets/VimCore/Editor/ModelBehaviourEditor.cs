using System;
using System.Reflection;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using VimCore.Runtime.MVVM;

namespace VimCore.Editor
{
    [CustomEditor(typeof(ModelBehaviour),true)]
    [CanEditMultipleObjects]
    public class ModelBehaviourEditor: UnityEditor.Editor
    {
        private bool visible;
        private const BindingFlags BindingAttr = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
        public override void OnInspectorGUI()
        {
            if(!PrefabUtility.IsPartOfPrefabInstance(target))
                ComponentUtility.MoveComponentUp(target as ModelBehaviour);
            GUILayout.BeginHorizontal();
            var color = GUI.color;
            GUI.color = Color.green;
            var mode = visible ? "hide" : "show";
            GUILayout.Label("ModelBehaviour");
            GUILayout.FlexibleSpace();
            if (GUILayout.Button($"{mode}")) 
                visible = !visible;
            GUI.color = color;
            GUILayout.EndHorizontal();
            if (visible) 
                DrawModelGUI();

            GUILayout.Space(8);
            DrawDefaultInspector();
        }

        private void DrawModelGUI()
        {
            var properties = target.GetType().GetProperties(BindingAttr);
            foreach (var item in properties)
            {
                try
                {
                    var type = item.GetGetMethod(true).ReturnType;
                    if (type == typeof(Observable))
                    {
                        GUILayout.BeginHorizontal(GUILayout.Height(20));
                        GUILayout.Label("Observable", GUILayout.MinWidth(100));
                        if (GUILayout.Button($"{item.Name}", GUILayout.MinWidth(100)))
                            EditorGUIUtility.systemCopyBuffer = item.Name;
                        GUILayout.EndHorizontal();
                    }
                    
                    if (type.GetGenericTypeDefinition() == typeof(Observable<>))
                    {
                        var typeArgument = type.GenericTypeArguments[0];
                        GUILayout.BeginHorizontal(GUILayout.Height(20));
                        GUILayout.Label($"Observable<{typeArgument.Name}>", GUILayout.MinWidth(100));
                        if (GUILayout.Button($"{item.Name}", GUILayout.MinWidth(100)))
                            EditorGUIUtility.systemCopyBuffer = item.Name;
                        GUILayout.EndHorizontal();
                    }

                    if (type.GetGenericTypeDefinition() == typeof(ObservableData<>))
                    {
                        var typeArgument = type.GenericTypeArguments[0];
                        GUILayout.BeginHorizontal(GUILayout.Height(20));
                        GUILayout.Label($"ObservableData<{typeArgument.Name}>", GUILayout.MinWidth(100));
                        if (GUILayout.Button($"{item.Name}", GUILayout.MinWidth(100)))
                            EditorGUIUtility.systemCopyBuffer = item.Name;
                        GUILayout.EndHorizontal();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            GUILayout.Space(8);
        }
    }
}