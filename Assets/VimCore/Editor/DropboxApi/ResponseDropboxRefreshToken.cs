using System;

namespace VimCore.Editor.DropboxApi
{
    [Serializable]
    public class ResponseDropboxRefreshToken
    {
        public string access_token;
        public int expires_in;
    }
}