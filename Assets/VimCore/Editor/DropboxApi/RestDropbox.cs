using System;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace VimCore.Editor.DropboxApi
{
    public class RestDropbox
    {
        public RestDropbox(string discordRefreshToken, string clientId, string clientSecret)
        {
            RefreshToken = discordRefreshToken;
            ClientId = clientId;
            ClientSecret = clientSecret;
        }

        private string ClientId { get; }
        private string RefreshToken { get; }
        private string ClientSecret { get; }

        private const string UploadUrl = "https://content.dropboxapi.com/2/files/upload";
        private const string TokenRefreshUrl = "https://api.dropbox.com/oauth2/token";
        private const string CloudUrl = "https://www.dropbox.com/sh/dm8077hmojsilcq/AAAg0KfiKLjkIy9eoRKkKqeea?dl=0";

        private const string TimeSpanFormatString = @"O";
        private static readonly string DefaultDate = DateTime.MinValue.ToString(TimeSpanFormatString);


        private async Task<string> GetToken()
        {
            var expireString = EditorPrefs.GetString("dropbox_token_expiry",DefaultDate);
            var expireTime = DateTime.ParseExact(expireString, TimeSpanFormatString, null);

            if (expireTime > DateTime.Now)
                return EditorPrefs.GetString("dropbox_token","");

            Debug.Log("Refreshing dropbox access token");
            
            var form = new WWWForm();
            form.AddField("grant_type", "refresh_token");
            form.AddField("refresh_token", RefreshToken);
            form.AddField("client_id", ClientId);
            form.AddField("client_secret", ClientSecret);
            
            var request = UnityWebRequest.Post(TokenRefreshUrl, form);
            request.SendWebRequest();
            while (!request.isDone) await Task.Delay(1000);
            if (request.error != null)
                throw new Exception(request.error + request.downloadHandler.text);

            var response = JsonUtility.FromJson<ResponseDropboxRefreshToken>(request.downloadHandler.text);
            if (response == null) 
                throw new Exception("parse error");
            
            expireTime = DateTime.Now.AddSeconds(response.expires_in);
            expireString = expireTime.ToString(TimeSpanFormatString);
            EditorPrefs.SetString("dropbox_token_expiry",expireString);
            
            EditorPrefs.SetString("dropbox_token",response.access_token);
            request.Dispose();
            return response.access_token;
        }

        public async Task<string> Upload(byte[] file, string fileName)
        {
            var token = await GetToken();
            Debug.Log($"Uploading {fileName} to Dropbox");
            var request = UnityWebRequest.PostWwwForm(UploadUrl, "POST");
            request.SetRequestHeader("Authorization", $"Bearer {token}");
            request.SetRequestHeader("Dropbox-API-Arg", $"{{\"autorename\": true, \"path\": \"/Builds/{fileName}\"}}");
            request.uploadHandler = new UploadHandlerRaw(file);
            request.SendWebRequest();

            while (request.uploadProgress<1)
            {
                Debug.Log($"Upload progress: {request.uploadProgress:P}");
                await Task.Delay(1000);
            }
            while (!request.isDone) await Task.Delay(100);
            if (request.error != null)
                throw new Exception(request.error + request.downloadHandler.text);

            var response = JsonUtility.FromJson<ResponseDropboxUploadFile>(request.downloadHandler.text);
            var message = $"{response?.name} was uploaded to {CloudUrl}";
            Debug.Log(message);
            request.Dispose();
            return message;
        }
    }
}