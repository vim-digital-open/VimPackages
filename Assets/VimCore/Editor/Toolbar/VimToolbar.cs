using System;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace VimCore.Editor.Toolbar
{
    [InitializeOnLoad]
    public class VimToolbar
    {
        private static readonly int UILayers = ~LayerMask.GetMask();

        static VimToolbar()
        {
            ToolbarExtender.LeftToolbarGUI += LeftBar;
            ToolbarExtender.RightToolbarGUI += RightBar;
        }

        public static void Entry(string label, Action onClick)
        {
            if(GUILayout.Button(label,ToolbarExtender.BtnStyle))
                onClick?.Invoke();
        }
        private static void LeftBar()
        {
            Entry("Project Settings", ShowProjectSettings);
            if (UnityEditor.Tools.visibleLayers != UILayers)
                Entry("Show UI", ShowUI);
            else
                Entry("Hide UI", HideUI);
            GUILayout.FlexibleSpace();
            Entry("Scenes", ListScenes);
        }

        private static void ShowProjectSettings()
        {
            EditorApplication.ExecuteMenuItem("Edit/Project Settings...");
        }

        private static void ShowUI()
        {
            Tools.visibleLayers = UILayers;
            SceneView.RepaintAll();
        }

        private static void HideUI()
        {
            Tools.visibleLayers = ~LayerMask.GetMask("UI");
            SceneView.RepaintAll();
        }


        private static void ListScenes()
        {
            var menu = new GenericMenu();
            for (var i = 0; i < EditorBuildSettings.scenes.Length; i++)
            {
                var scene = EditorBuildSettings.scenes[i];
                var name = scene.path.Split("/").Last();
                menu.AddItem(new GUIContent(name+"/Open"), false, () => SceneHelper.StartScene(scene.path));
                menu.AddItem(new GUIContent(name+"/Locate"), false, () => SceneHelper.FocusScene(scene.path));
            }
            menu.ShowAsContext();
        }

        private static void RightBar() 
        {
            TimeScale();
            GUILayout.FlexibleSpace();
        }

        private static void TimeScale()
        {
            if (EditorApplication.isPlaying)
            {
                Entry("Clean Restart", CleanRestart);
                GUILayout.BeginHorizontal(ToolbarExtender.BoxStyle);
                GUILayout.Label($"Speed: {Time.timeScale:f}", GUILayout.MinWidth(80));
                var sqrt = GUILayout.HorizontalSlider(Mathf.Sqrt(Time.timeScale), 0, 2, GUILayout.MinWidth(128));
                Time.timeScale = sqrt * sqrt;
                if (GUILayout.Button("Reset",ToolbarExtender.BtnStyle, GUILayout.MinWidth(80)))
                    Time.timeScale = 1;
                
                GUILayout.EndHorizontal();
            }
            else
            {
                Entry("Clean Run", CleanRun);
                Time.timeScale = 1;
            }
        }


        private static async void CleanRestart()
        {
            EditorApplication.ExitPlaymode();
            await Task.Delay(10);
            await Task.Yield();
            CleanRun();
        }

        private static void CleanRun()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
            EditorApplication.EnterPlaymode();
        }
    }
}