using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace VimCore.Editor.Toolbar
{
	public static class ToolbarExtender
	{
		private static readonly Type ToolbarType = typeof(UnityEditor.Editor).Assembly.GetType("UnityEditor.Toolbar");
		private static ScriptableObject _currentToolbar;

		private static GUIStyle _btnStyle;
		private static GUIStyle _boxStyle;

		public static GUIStyle BtnStyle => _btnStyle ??= new GUIStyle("toolbarbutton")
		{
			fontSize = 12,
			fontStyle = FontStyle.Bold,
			margin = new RectOffset(12,12,0,0),
			padding = new RectOffset(12,12,0,0)
		};

		public static GUIStyle BoxStyle => _boxStyle ??= new GUIStyle("toolbar")
		{
			fontSize = 12,
			fontStyle = FontStyle.Bold,
			padding = new RectOffset(12,12,0,0)
		};

		public static event Action LeftToolbarGUI;
		public static event Action RightToolbarGUI;

		static ToolbarExtender()
		{
			EditorApplication.update -= OnUpdate;
			EditorApplication.update += OnUpdate;
		}

		private static void OnUpdate()
		{
			if (_currentToolbar != null) return;
			var toolbars = Resources.FindObjectsOfTypeAll(ToolbarType);
			_currentToolbar = toolbars.Length > 0 ? (ScriptableObject) toolbars[0] : null;
			if (_currentToolbar == null) return;
			var fieldInfo = _currentToolbar.GetType().GetField("m_Root", BindingFlags.NonPublic | BindingFlags.Instance);
			if (fieldInfo == null) return;
			var rawRoot = fieldInfo.GetValue(_currentToolbar);
			var mRoot = rawRoot as VisualElement;

			var leftZone = mRoot.Q("ToolbarZoneLeftAlign");
			RegisterCallback(leftZone, OnGUILeft);
			
			var rightZone = mRoot.Q("ToolbarZoneRightAlign");
			RegisterCallback(rightZone, OnGUIRight);
		}
		
		private static void RegisterCallback(VisualElement toolbarZone, Action cb) 
		{
			var parent = new VisualElement
			{
				style = {
					flexGrow = 1,
					flexDirection = FlexDirection.Row
				}
			};
			var container = new IMGUIContainer();
			container.style.flexGrow = 1;
			container.onGUIHandler = cb; 
			parent.Add(container);
			toolbarZone.Add(parent);
		}
		
		private static void OnGUILeft() {
			GUILayout.BeginHorizontal();
			GUILayout.Space(12);
			LeftToolbarGUI?.Invoke();
			GUILayout.EndHorizontal();
		}

		private static void OnGUIRight() {
			GUILayout.BeginHorizontal();
			RightToolbarGUI?.Invoke();
			GUILayout.Space(12);
			GUILayout.EndHorizontal();
		}
	}
}