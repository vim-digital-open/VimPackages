using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace VimCore.Editor.Toolbar
{
    internal static class SceneHelper
    {
        private static string _scenePath;
        public static void StartScene(string path)
        {
            EditorApplication.isPlaying = false;
            _scenePath = path;
            EditorApplication.update += OnUpdate;
        }

        private static void OnUpdate()
        {
            if (EditorApplication.isPlaying) return;
            if (EditorApplication.isPaused) return;
            if (EditorApplication.isPlayingOrWillChangePlaymode) return;
            if (EditorApplication.isCompiling) return;

            EditorApplication.update -= OnUpdate;

            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                if (!SceneManager.GetSceneByPath(_scenePath).isLoaded)
                    EditorSceneManager.OpenScene(_scenePath);
            
            _scenePath = string.Empty;
        }

        public static void FocusScene(string path)
        {
            var obj = AssetDatabase.LoadAssetAtPath(path, typeof(Object));
            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }
    }
}