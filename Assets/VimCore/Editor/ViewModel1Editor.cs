using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using VimCore.Runtime.MVVM;

namespace VimCore.Editor
{
    [CustomEditor(typeof(AViewModel<>),true)]
    [CanEditMultipleObjects]
    public class ViewModel1Editor: UnityEditor.Editor
    {
        private const BindingFlags BindingAttr = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;

        private ModelBehaviour _cachedModel;
        public override void OnInspectorGUI() => OnInspectorGUI((dynamic)target);

        private void OnInspectorGUI<TData>(AViewModel<TData> viewModel)
        {
            var model = _cachedModel ??= viewModel.GetComponentInParent<ModelBehaviour>();
            if (model)
                DrawInspectorForModel<TData>(model);
            else
                DrawModelNotFoundInspector();
        }

        private void DrawInspectorForModel<TData>(ModelBehaviour model)
        {
            GUILayout.BeginHorizontal();
            var color = GUI.color;
            GUI.color = Color.green;
            if (GUILayout.Button("Model", GUILayout.MaxWidth(48)))
                EditorGUIUtility.PingObject(model);
            GUI.color = color;
            var prop = serializedObject.FindProperty("property");
            prop.stringValue = GUILayout.TextField(prop.stringValue);
            var menu = new GenericMenu();
            var info = model.GetType().GetProperties(BindingAttr);
            foreach (var item in info)
            {
                try
                {
                    var type = item.GetGetMethod(true).ReturnType;
                    if (type.IsEquivalentTo(typeof(Observable<>))) continue;
                    var typeArgument = type.GenericTypeArguments[0];
                    if (typeof(TData) != typeArgument) continue;
                    menu.AddItem(new GUIContent(item.Name), false, () =>
                    {
                        serializedObject.FindProperty("property").stringValue = item.Name;
                        serializedObject.ApplyModifiedProperties();
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            if (EditorGUILayout.DropdownButton(new GUIContent("Pick"), FocusType.Passive, GUILayout.MaxWidth(48)))
                menu.ShowAsContext();
            GUILayout.EndHorizontal();
            DrawPropertiesExcluding(serializedObject, "m_Script", "property");
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawModelNotFoundInspector()
        {
            GUILayout.Label("MODEL NOT FOUND");
            DrawDefaultInspector();
        }
    }
}