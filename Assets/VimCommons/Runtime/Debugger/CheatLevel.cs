using VimCommons.Runtime.LevelManagement.Core;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Debugger
{
    public class CheatLevel: ACheat
    {
        private static ServiceLevelManager LevelManager => Locator.Resolve<ServiceLevelManager>();

        protected override string Command => "level";
        protected override string Description => Command;

        protected override void OnApply()
        {
            LevelManager.FinishLevel();
        }
    }
}
