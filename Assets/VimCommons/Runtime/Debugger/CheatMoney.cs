using VimCommons.Runtime.Looting.Core;
using VimCommons.Runtime.Looting.Inventory;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Debugger
{
    public class CheatMoney: ACheat<int>
    {
        public string command;
        public string description;
        public LootableDefinition loot;
        
        private static ServiceInventory Inventory => Locator.Resolve<ServiceInventory>();

        protected override string Command => command;
        protected override string Description => description;

        protected override void OnApply(int amount) => Inventory.Receive(loot, amount);
    }
}