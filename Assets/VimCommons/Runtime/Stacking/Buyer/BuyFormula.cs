using System;
using System.Collections.Generic;
using VimCommons.Runtime.Looting.Core;
using VimCommons.Runtime.Stacking.Core;

namespace VimCommons.Runtime.Stacking.Buyer
{
    [Serializable]
    public class BuyFormula
    {
        public StackableDefinition definition;
        public int price;
    }
}