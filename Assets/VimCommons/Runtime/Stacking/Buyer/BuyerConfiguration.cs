using System.Collections.Generic;
using UnityEngine;

namespace VimCommons.Runtime.Stacking.Buyer
{
    [CreateAssetMenu]
    public class BuyerConfiguration: ScriptableObject
    {
        public List<BuyFormula> formulas;
    }
}