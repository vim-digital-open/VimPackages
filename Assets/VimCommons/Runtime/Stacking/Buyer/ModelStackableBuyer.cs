using System.Linq;
using UnityEngine;
using VimCommons.Runtime.Looting.Core;
using VimCommons.Runtime.Stacking.Core;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Stacking.Buyer
{
    public class ModelStackableBuyer : ModelBehaviour
    {
        public BuyerConfiguration config;
        public Transform appendSource;

        private ModelLootableBatch _batch;
        public ModelLootableBatch Batch => _batch ??= GetComponentInChildren<ModelLootableBatch>(); 
        public Observable<ModelStack> ChannelStack { get; } = new();

        private void Awake() => ChannelStack.AddListener(OnStack);

        public void OnStack(ModelStack stack)
        {
            var variants = config.formulas.Select(i => i.definition).ToArray();
            if (stack.TryExtractAny(variants, out var stackable))
            {
                stackable.TweenRemove(appendSource.position, stack.vibrate);
                var cost = config.formulas.First(i => i.definition == stackable.Definition).price;
                Batch.Append(appendSource, cost);
            }
        }
    }
}