using DG.Tweening;
using UnityEngine;
using VimCommons.Runtime.Progression.Building;
using VimCommons.Runtime.Stacking.StackableSource;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;
using Random = UnityEngine.Random;

namespace VimCommons.Runtime.Stacking.Delivery
{
    public class ModelDeliveryCar : ProgressionBuilding
    {
        public float initialDelaySpread = 5;
        public ModelStackableSource source;
        public float workTime = 10;
        public Vector3 outPos;
        public int moveDuration = 5;
        public Transform car; 

        private Sequence _sequence;
    
        public ObservableData<bool> TruckOpen { get; } = new();
        public ObservableData<bool> GateOpen { get; } = new();

        private void Awake()
        {
            NodeLevel.AddListener(OnLevel);
        }

        private void OnLevel(int obj)
        {
            _sequence?.Kill();
            source.Muted = true;
            car.localPosition = outPos;

            if (obj < 1) return;
            _sequence = DOTween.Sequence()
                .SetLoops(-1)
                .AppendInterval(Random.value * initialDelaySpread)
                .AppendLerp(moveDuration, ez => {
                    car.localPosition = Vector3.Lerp(outPos, Vector3.zero, ez.Linear);
                }).AppendCallback(() =>
                {
                    GateOpen.Value = true;
                }).AppendInterval(1).AppendCallback(() =>
                {
                    TruckOpen.Value = true;
                    source.Muted = false;
                })
                .AppendInterval(workTime).AppendCallback(() => 
                {
                    source.Muted = true;
                    TruckOpen.Value = false;
                }).AppendInterval(1).AppendCallback(() =>
                {
                    GateOpen.Value = false;
                }).AppendLerp(moveDuration, ez => {
                    car.localPosition = Vector3.Lerp(Vector3.zero, outPos, ez.Linear);
                });
        }

        private void OnDestroy() => _sequence?.Kill();
    }
}
