using UnityEngine;
using VimCommons.Runtime.Progression.Building;
using VimCommons.Runtime.Stacking.Core;
using VimCommons.Runtime.Triggers.TriggerSystem;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Stacking.StackableConverter
{
    public class ModelStackableConverter : ProgressionBuilding
    {
        public ProgressionChannel<StackableConverterLevel> channel;
        public ProgressionChannel<StackableConverterOperatorLevel> channelOperator;
        public Transform conversionPoint;
        public StackableConverterEntry[] source;
        public ModelStackableBatch result;
        public string activatorAnimation;

        public ObservableData<int> OperatorLevel => channelOperator.NodeLevel;
        public ObservableData<bool> IsWork { get; } = new();
        public ObservableData<bool> IsLoaded { get; } = new();
        public ObservableData<float> Progress { get; } = new();
        private Observable<SignalTrigger> ChannelHitbox { get; } = new();

        private void Awake()
        {
            channel.NodeLevel.AddListener(OnLevel);
            ChannelHitbox.AddListener(OnHitbox);
        }

        private void OnLevel(int obj)
        {
            var level = channel.Level;
            foreach (var entry in source)
            {
                entry.batch.Capacity.Value = Mathf.FloorToInt(entry.capacity * level.capacityMultiplier);
            }
        }


        private void Update()
        {
            LoadIfNeeded();
            if (channelOperator.NodeLevel.Value > 0)
            {
                IsWork.Value = IsLoaded.Value;
                if (!IsLoaded.Value) return;
                TickConversion();
            }
        }

        public void OnHitbox(SignalTrigger sig)
        {
            if (NodeLevel.Value < 1) return;
            switch (sig.State)
            {
                case TriggerState.Enter:
                    IsWork.Value = true;
                    break;

                case TriggerState.Stay:
                    TickConversion();
                    break;

                case TriggerState.Exit:
                    IsWork.Value = false;
                    Progress.Value = 0;
                    break;
            }
        }

        private void TickConversion()
        {
            Progress.Value += Time.deltaTime / channel.Level.duration;
            if (Progress.Value < 1) return;
            Progress.Value = 0;
            FinishConversion();
        }


        private void LoadIfNeeded()
        {
            if (IsLoaded.Value) return;
            foreach (var entry in source)
                if (entry.CanConvert)
                    return;

            var posTo = conversionPoint.position;
            foreach (var entry in source)
                entry.Pop(posTo, IsWork.Value);
            IsLoaded.Value = true;
        }

        private void FinishConversion()
        {
            var stackable = result.definition.Spawn();
            stackable.Init(conversionPoint);
            result.Push(stackable, IsWork.Value);
            IsLoaded.Value = false;
        }
    }
}
