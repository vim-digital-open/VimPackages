using System;
using UnityEngine;
using VimCommons.Runtime.Stacking.Core;

namespace VimCommons.Runtime.Stacking.StackableConverter
{
    [Serializable]
    public struct StackableConverterEntry
    {
        public ModelStackableBatch batch;
        public int requirement;
        public int capacity;
        
        public bool CanConvert => batch.Amount.Value < requirement;

        public void Pop(Vector3 posTo, bool vibrate)
        {
            for (int i = 0; i < requirement; i++)
            {
                var stackable = batch.Pop();
                stackable.TweenRemove(posTo, vibrate);
            }
        }
    }
}