using UnityEngine;
using VimCommons.Runtime.Progression.Building;
using VimCommons.Runtime.Stacking.Core;
using VimCommons.Runtime.Triggers.TriggerSystem;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Stacking.StackableConverter
{
    public class ModelStackableAutoConverter : ProgressionBuilding
    {
        public ProgressionChannel<StackableConverterLevel> channel;
        public Transform conversionPoint;
        public ModelStackableBatch result;
        public float _conversionTime = 3f;
        public ObservableData<bool> IsWork { get; } = new();
        public ObservableData<bool> IsLoaded { get; } = new();
        public ObservableData<float> Progress { get; } = new();
        private Observable<SignalTrigger> ChannelHitbox { get; } = new();

        private bool _isFull = false;
        private void IsFull(bool isFull)
        {
            _isFull = isFull;
        }

        private void Awake()
        {
            IsWork.Value = false;
            //result.Amaount.Value = 4f;
            //Amount.Value = Content.Count;
            result.IsFull.AddListener(IsFull);
            //result.Capacity.Value = channel.NodeLevel.Capacity.Value;
            channel.NodeLevel.AddListener(OnLevel);
        }

        private void OnLevel(int obj)
        {
            var level = channel.Level;
            result.Capacity.Value = Mathf.FloorToInt(level.capacityMultiplier);
            _conversionTime = level.duration;
        }

        private void Update() => Conversion();

        private void TickConversion()
        {
            Progress.Value += Time.deltaTime;
            if (Progress.Value < _conversionTime) return;
            Progress.Value = 0;
            FinishConversion();
        }

        private void Conversion()
        {
            if (NodeLevel.Value < 1) return;
            if (_isFull)
            {
                IsWork.Value = false;
                return;
            }
            IsWork.Value = true;
            TickConversion();
        }

        private void FinishConversion()
        {
            var stackable = result.definition.Spawn();
            stackable.Init(conversionPoint);
            result.Push(stackable, true);
            //result.Pop(lootable,alue = 4f;
        }
    }
}
