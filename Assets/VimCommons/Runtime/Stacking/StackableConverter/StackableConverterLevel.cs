using System;

namespace VimCommons.Runtime.Stacking.StackableConverter
{
    [Serializable]
    public struct StackableConverterLevel
    {
        public float duration;
        public float capacityMultiplier;
    }
}