using System.Collections.Generic;
using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Stacking.Core
{
    public class AreaHideStack : MonoBehaviour
    {
        
        private static readonly Filter<ModelStack> Filter = Locator.Filter<ModelStack>();

        private Collider _area;
        public Collider Area => _area ??= GetComponent<Collider>(); 

        private HashSet<ModelStack> Hidden { get; } = new();
        
        private void Update()
        {
            var bounds = Area.bounds;
            foreach (var stack in Filter)
            {
                if (bounds.Contains(stack.Transform.position))
                {
                    if (Hidden.Add(stack)) 
                        stack.SetHidden(true);
                }
                else
                {
                    if(Hidden.Remove(stack))
                        stack.SetHidden(false);
                }
            } 
        }

        private void OnDisable()
        {
            foreach (var stack in Hidden) 
                stack.SetHidden(false);
            Hidden.Clear();
        }
    }
}