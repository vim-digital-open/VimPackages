using DG.Tweening;
using UnityEngine;
using VimCommons.Runtime.Vibration.ServiceVibration;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Stacking.Core
{
    public class ModelStackable : ModelBehaviour
    {
        public float height = 0.5f;

        private static ServiceVibration Vibration => Locator.Resolve<ServiceVibration>();
        private Transform _transform;
        public Transform Transform => _transform ??= transform;

        public StackableDefinition Definition { get; set; }
        public float Weight => Definition.weight;
        public ObservableData<bool> Visible { get; set; } = new();

        public void Build(StackableDefinition definition)
        {
            Definition = definition;

            Transform.localScale = Vector3.one;
            Transform.rotation = Quaternion.identity;
            Visible.Value = true;
        }

        public void Init(Vector3 posSrc, Vector3 posDest)
        {
            Transform.position = posSrc;

            DOTween.Sequence().AppendLerp(0.5f, ez =>
            {
                var t = ez.Linear;
                Transform.localPosition = Helper.LerpParabolic(posSrc, posDest, t, 2);
                Transform.localScale = Vector3.one * ez.BackOut;
            });
        }

        public void Init(Transform spawnPoint)
        {
            Transform.position = spawnPoint.position;
            Transform.rotation = spawnPoint.rotation;
        }

        public void TweenRemove(Vector3 posTo, bool vibrate = false)
        {
            var posFrom = Transform.localPosition;
            DOTween.Sequence().AppendLerp(ez =>
            {
                Transform.localScale = Vector3.one * (1 - ez.BackIn / 2);
                Transform.localPosition = Helper.LerpParabolic(posFrom, posTo, ez.QuadIn);
            }).AppendCallback(() =>
            {
                if (vibrate)
                    Vibration.Light();
                Definition.Remove(this);
            });
        }

        public void Remove() => Definition.Remove(this);
    }
}