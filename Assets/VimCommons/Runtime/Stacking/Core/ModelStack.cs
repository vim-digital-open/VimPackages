using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using VimCommons.Runtime.Progression.Building;
using VimCommons.Runtime.Vibration.ServiceVibration;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Stacking.Core
{
    public class ModelStack : ModelBehaviour
    {
        public Transform anchor;
        public float lerpForce = 30;
        public float cooldown = 0.1f;
        public bool vibrate;
        public ProgressionChannel<int> channelCapacity;

        private static readonly Filter<ModelStack> Filter = Locator.Filter<ModelStack>();
        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);

        private static readonly Filter<StackInteractor> Interactors = Locator.Filter<StackInteractor>();
        private static ServiceVibration Vibration => Locator.Resolve<ServiceVibration>();

        private Transform _transform;
        public Transform Transform => _transform ??= transform;

        public ObservableData<bool> IsHolding { get; } = new();
        public ObservableData<bool> IsFull { get; } = new();

        public bool Hidden { get; set; }


        private List<ModelStackable> Stack { get; } = new();
        private readonly Vector3[] _items = new Vector3[300];

        private float _pendingWeight;
        private float _readyTime;

        public event Action<List<ModelStackable>> OnUpdate;
        public int Capacity => channelCapacity.Level;
        public int Count => Stack.Count;
        public bool Empty => TotalWeight() <= 0;
        public bool Full => TotalWeight() >= Capacity;

        public void SetHidden(bool state)
        {
            Hidden = state;
            IsHolding.Value = false;
            foreach (var stackable in Stack)
                stackable.Visible.Value = !state;
        }

        private void Update()
        {
            if (Hidden) return;
            IsHolding.Value = Stack.Count > 0;

            if (Time.realtimeSinceStartup < _readyTime) return;
            _readyTime = Time.realtimeSinceStartup + cooldown;

            foreach (var interactor in Interactors)
                interactor.Interact(this);
        }

        private void LateUpdate()
        {
            if (Hidden) return;
            var vOffset = 0f;
            var root = anchor.position;
            var up = anchor.up;
            var rot = anchor.rotation;
            for (var i = 0; i < Stack.Count; i++)
            {
                var item = Stack[i];
                var targetPos = root + up * vOffset;
                vOffset += item.height;
                var lerpArg = Time.deltaTime * lerpForce / Mathf.Sqrt(vOffset);
                _items[i] = Vector3.Lerp(_items[i], targetPos, lerpArg);
                item.Transform.position = _items[i];
                item.Transform.rotation = rot;
            }
        }

        private Vector3 TopPoint(float resHeight)
        {
            var offset = resHeight;
            foreach (var stackable in Stack)
                offset += stackable.height;
            return anchor.position + anchor.up * offset;
        }

        private float TotalWeight()
        {
            var sum = 0f;
            foreach (var stackable in Stack)
                sum += stackable.Weight;
            return sum + _pendingWeight;
        }

        public bool CanAccept(StackableDefinition definition)
        {
            if (TotalWeight() < 1)
                return true;
            return TotalWeight() + definition.weight <= Capacity;
        }

        public int CountAny(params StackableDefinition[] variants)
        {
            var amount = 0;
            foreach (var stackable in Stack)
                if (variants.Contains(stackable.Definition))
                    amount++;
            return amount;
        }

        public bool ContainsAny(params StackableDefinition[] variants)
        {
            foreach (var stackable in Stack)
                if (variants.Contains(stackable.Definition))
                    return true;
            return false;
        }

        public bool TryExtract(StackableDefinition definition, out ModelStackable result)
        {
            foreach (var stackable in Stack)
                if (Equals(definition, stackable.Definition))
                {
                    Stack.Remove(stackable);
                    OnUpdate?.Invoke(Stack);
                    IsFull.Value = false;
                    result = stackable;
                    return true;
                }
            result = null;
            return false;
        }

        public bool TryExtractAny(StackableDefinition[] variants, out ModelStackable result)
        {
            foreach (var stackable in Stack)
                if (variants.Contains(stackable.Definition))
                {
                    Stack.Remove(stackable);
                    OnUpdate?.Invoke(Stack);
                    IsFull.Value = false;
                    result = stackable;
                    return true;
                }
            result = null;
            return false;
        }

        public ModelStackable ExtractAny(params StackableDefinition[] variants)
        {
            foreach (var stackable in Stack)
                if (variants.Contains(stackable.Definition))
                {
                    Stack.Remove(stackable);
                    OnUpdate?.Invoke(Stack);
                    IsFull.Value = false;
                    return stackable;
                }
            return null;
        }

        public void Append(ModelStackable stackable)
        {
            var posFrom = stackable.Transform.position;
            var rotFrom = stackable.Transform.rotation;
            _pendingWeight += stackable.Weight;
            DOTween.Sequence().AppendLerp(ez =>
            {
                stackable.Transform.position = Helper.LerpParabolic(posFrom, TopPoint(stackable.height), ez.QuadOutIn, 2);
                stackable.Transform.rotation = Quaternion.Lerp(rotFrom, anchor.rotation, ez.Linear);
                stackable.Transform.localScale = Vector3.one * ez.BackOut;
            }).AppendCallback(() =>
            {
                _pendingWeight -= stackable.Weight;
                _items[Stack.Count] = TopPoint(stackable.height);
                Stack.Add(stackable);
                OnUpdate?.Invoke(Stack);
                IsFull.Value = Full;
                if (vibrate)
                    Vibration?.Light();
            });
        }

        public ModelStackable Peek() => Stack.LastOrDefault();
    }
}