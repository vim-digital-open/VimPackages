using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using VimCommons.Runtime.Vibration.ServiceVibration;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;
using Random = UnityEngine.Random;

namespace VimCommons.Runtime.Stacking.Core
{
    public class ModelStackableBatch : ModelBehaviour
    {
        public StackableDefinition definition;
        public Transform anchor;
        public Vector3 size = Vector3.one;
        public Vector2Int table = new(2, 2);
        public Transform pushPoint;
        public Transform popPoint;
        public bool vibrateOnPush;
        private static ServiceVibration Vibration => Locator.Resolve<ServiceVibration>();

        private Stack<ModelStackable> Content { get; } = new();

        public ObservableData<int> Capacity { get; } = new();
        public ObservableData<int> Amount { get; } = new();

        public ObservableData<bool> IsFull { get; } = new();
        public ObservableData<bool> IsEmpty { get; } = new();
        public ObservableData<string> Label { get; } = new();

        public Observable Bounce { get; } = new();

        public Observable<ModelStack> ChannelStackPop { get; } = new();
        public Observable<ModelStack> ChannelStackPush { get; } = new();

        public int FreeSlots => Capacity.Value - Amount.Value;
        public float FillPercent => Amount.Value * 1f / Capacity.Value;
        public float TotalWeight => Amount.Value * definition.weight;

        private void Awake()
        {
            Amount.AddListener(RefreshLabel);
            Capacity.AddListener(RefreshLabel);

            ChannelStackPop.AddListener(OnStackPop);
            ChannelStackPush.AddListener(OnStackPush);
        }

        private void OnDestroy()
        {
            foreach (var stackable in Content)
                if (definition != null)
                    definition.Remove(stackable);
        }

        private void RefreshLabel(int _)
        {
            IsEmpty.Value = Amount.Value < 1;
            IsFull.Value = Amount.Value == Capacity.Value;
            Label.Value = $"{Amount.Value}/{Capacity.Value}";
            Bounce.Invoke();
        }

        public void OnStackPop(ModelStack stack)
        {
            if (Amount.Value < 1) return;
            if (!stack.CanAccept(definition)) return;
            var stackable = Pop();
            stack.Append(stackable);
        }

        public ModelStackable Pop()
        {
            if (Content.TryPop(out var stackable))
            {
                Amount.Value = Content.Count;
                return stackable;
            }
            return null;
        }

        public void OnStackPush(ModelStack stack)
        {
            if (Amount.Value >= Capacity.Value) return;
            if (stack.TryExtract(definition, out var stackable))
                Push(stackable, vibrateOnPush && stack.vibrate);
        }

        public void Spawn(Transform spawnPoint)
        {
            var stackable = definition.Spawn();
            stackable.Init(spawnPoint);
            Push(stackable);
        }

        public void Clear()
        {
            while (Content.TryPop(out var stackable))
                stackable.Remove();
            Amount.Value = 0;
        }

        public void Push(ModelStackable stackable, bool vibrate = false)
        {
            Assert.AreEqual(definition, stackable.Definition);

            var posFrom = stackable.Transform.position;
            var rotFrom = stackable.Transform.eulerAngles;
            var posTo = anchor.position + anchor.rotation * GridPos(Amount.Value);
            var rotTo = anchor.eulerAngles + Vector3.up * Random.Range(-5, 5);

            Content.Push(stackable);
            Amount.Value = Content.Count;

            DOTween.Sequence().AppendLerp(ez =>
            {
                stackable.Transform.position = Helper.LerpParabolic(posFrom, posTo, ez.QuadIn);
                stackable.Transform.eulerAngles = Vector3.Lerp(rotFrom, rotTo, ez.QuadIn);
            }).AppendCallback(() =>
            {
                if (vibrate)
                    Vibration.Light();
            });
        }

        private Vector3 GridPos(int a)
        {
            var b = a % table.x;
            a /= table.x;
            var c = a % table.y;
            a /= table.y;

            var grid = new Vector3(
                c - 0.5f * (table.y - 1),
                a,
                b - 0.5f * (table.x - 1));

            return Vector3.Scale(grid, size);
        }
    }
}