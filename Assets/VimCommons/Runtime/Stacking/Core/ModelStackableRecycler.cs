using UnityEngine;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Stacking.Core
{
    public class ModelStackableRecycler : ModelBehaviour
    {
        public Transform unstackPoint;
        public StackableDefinition[] recyclable;

        public Observable<ModelStack> ChannelStack { get; } = new();

        private void Awake() => ChannelStack.AddListener(OnStack);

        public void OnStack(ModelStack stack)
        {
            var stackable = stack.ExtractAny(recyclable);
            if (!stackable) return;
            stackable.TweenRemove(unstackPoint.position, stack.vibrate);
        }
    }
}
