using UnityEngine;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Stacking.Core
{
    [CreateAssetMenu]
    public class StackableDefinition: ScriptableObjectWithGuid
    {
        public ModelStackable prefab;
        public Sprite icon;
        public float weight;

        public ModelStackable Spawn()
        {
            var instance = prefab.SpawnPoolable();
            instance.Build(this);
            return instance;
        }
        public void Remove(ModelStackable instance)
        {
            prefab.RemovePoolable(instance);
        }
    }
}