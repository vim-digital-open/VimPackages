using UnityEngine;
using VimCommons.Runtime.Progression.Building;
using VimCommons.Runtime.Stacking.Core;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Stacking.StackableSource
{
    public class ModelStackableSource : ProgressionBuilding
    {
        public ProgressionChannel<StackableSourceLevel> channel;
        public Transform spawnPoint;
        public ModelStackableBatch batch;
        public int initialCount;

        public ObservableData<bool> IsWork { get; } = new();


        public bool Muted { get; set; }
        private float Timer { get; set; }

        private void Awake()
        {
            NodeLevel.AddListener(OnLevel);
            channel.NodeLevel.AddListener(OnUpgrade);
        }

        private void OnUpgrade(int obj)
        {
            batch.Capacity.Value = channel.Level.maxAmount;
        }

        private void OnLevel(int obj)
        {
            if (NodeLevel.Value < 1) return;
            for (int i = 0; i < initialCount; i++)
                batch.Spawn(batch.anchor);
            initialCount = 0;
        }


        public void Update()
        {
            if (Muted) return;
            if (NodeLevel.Value < 1) return;
            TickConversion();
        }

        private void TickConversion()
        {
            IsWork.Value = batch.Amount.Value < channel.Level.maxAmount;

            if (!IsWork.Value) return;

            Timer += Time.deltaTime;
            if (Timer < channel.Level.cooldown) return;
            Timer = 0;

            FinishConversion();
        }

        private void FinishConversion()
        {
            batch.Spawn(spawnPoint);
        }
    }
}