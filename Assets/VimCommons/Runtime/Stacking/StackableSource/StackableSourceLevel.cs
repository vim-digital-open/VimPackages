using System;

namespace VimCommons.Runtime.Stacking.StackableSource
{
    [Serializable]
    public struct StackableSourceLevel
    {
        public int maxAmount;
        public float cooldown;
    }
}