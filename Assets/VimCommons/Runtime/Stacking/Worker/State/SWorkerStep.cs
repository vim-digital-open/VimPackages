namespace VimCommons.Runtime.Stacking.Worker.State
{
    public class SWorkerStep : SWorkerAbstract
    {
        public override void Enter()
        {
            if (Subject.Stack.Empty)
            {
                ChangeState<SWorkerCollect>();
            }
            else
            {
                ChangeState<SWorkerDeliver>();
            }
        }
    }
}