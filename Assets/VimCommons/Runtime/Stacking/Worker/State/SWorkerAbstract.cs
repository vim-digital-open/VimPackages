using UnityEngine;
using VimCore.Runtime.StateMachine;

namespace VimCommons.Runtime.Stacking.Worker.State
{
    public abstract class SWorkerAbstract: AState<ModelStackWorker,SWorkerAbstract>
    {
        public virtual Vector3 GetTarget() => Subject.Transform.position;
    }
}