using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VimCommons.Runtime.Stacking.Core;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Stacking.Worker.State
{
    public class SWorkerCollect: SWorkerAbstract
    {
        private static readonly Filter<BatchWorkerSource> Sources = Locator.Filter<BatchWorkerSource>();

        private static readonly Filter<BatchWorkerTarget> Targets = Locator.Filter<BatchWorkerTarget>();
        private BatchWorkerSource Source { get; set; }
        private Vector3 ChillOffset { get; set; }
        
        public override void Enter()
        {
            var chillOffset = Random.onUnitSphere;
            chillOffset.y = 0;
            ChillOffset = chillOffset.normalized;
        }

        public override Vector3 GetTarget()
        {
            if (CheckCompleted())
            {
                ChangeState<SWorkerStep>();
                return Subject.Transform.position;
            }

            if (ValidateSource()) 
                return Source.CollectPoint;
            return Subject.ChillPoint.position + ChillOffset;
        }

        private bool ValidateSource()
        {
            ForgetCurrentIfInvalid();
            FindMostFilledSourceIfNeeded();
            return Source;
        }

        private bool CheckCompleted()
        {
            if (Subject.Stack.Full) 
                return true;
            if (Subject.Stack.Count > 0)
            {
                if (!Source) return true;
                if (Source.Batch.Amount.Value < 1)
                    return true;
            }
            return false;
        }

        private void ForgetCurrentIfInvalid()
        {
            if (!Source) return;
            if (!Source.isActiveAndEnabled)
                Source = null;
            if (Source.Batch.TotalWeight >= Subject.Stack.Capacity) return;
            Source = null;
        }

        private void FindMostFilledSourceIfNeeded()
        {
            if (Source) return;
            
            var required = GetMostRequiredStackable();
            if (required.Count < 1) return;
            
            int maxCount = 0;
            BatchWorkerSource result = null;
            
            foreach (var current in Sources)
            {
                if (current.Batch.IsEmpty.Value) continue;
                if (!required.Contains(current.Batch.definition)) continue;
                var curCount = current.Batch.Amount.Value;
                if (curCount < maxCount) continue;
                maxCount = curCount;
                result = current;
            }
            Source = result;
        }

        private HashSet<StackableDefinition> GetMostRequiredStackable()
        {
            var result = new HashSet<StackableDefinition>();
            var dict = new Dictionary<StackableDefinition, int>();
            foreach (var target in Targets)
            {
                var targetDefinition = target.Batch.definition;
                if (!Subject.loadVariants.Contains(targetDefinition)) continue;
                if (targetDefinition.weight > Subject.Stack.Capacity) continue;
                
                var totalSlots = target.Batch.FreeSlots;
                if (dict.TryGetValue(targetDefinition, out var prevSlots))
                    totalSlots += prevSlots;
                dict[targetDefinition] = totalSlots;

                if (totalSlots < 1) continue;

                result.Add(targetDefinition);
            }
            return result;
        }
    }
}