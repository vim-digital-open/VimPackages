using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Stacking.Worker.State
{
    public class SWorkerDeliver: SWorkerAbstract
    {
        
        private static readonly Filter<BatchWorkerTarget> Filter = Locator.Filter<BatchWorkerTarget>();

        private BatchWorkerTarget Current { get; set; }
        public float LastAction { get; set; }
        public float PatienceFull { get; set; }

        public override Vector3 GetTarget()
        {
            
            if (CheckCompleted())
            {
                ChangeState<SWorkerStep>();
                return Subject.Transform.position;
            }

            if (ValidateTarget())
            {
                LastAction = Time.time;
                return Current.UnstackPoint;
            }
            
            if (OutOfPatience())
            {
                return Subject.RecyclePoint.position;
            }

            return Subject.transform.position;
        }

        private bool OutOfPatience() => Time.time > LastAction + Subject.patienceSeconds;

        private bool CheckCompleted() => Subject.Stack.Empty;


        private bool ValidateTarget()
        {
            ForgetCurrentIfInvalid();
            FindCurrentIfNeeded();
            return Current;
        }

        private void ForgetCurrentIfInvalid()
        {
            if (!Current) return;
            if (!Current.isActiveAndEnabled)
                Current = null;
            if (Current.Batch.IsFull.Value)
            {
                PatienceFull += Time.deltaTime;
                if (PatienceFull > Subject.patienceSeconds)
                    Current = null;
                return;
            }

            if (!Subject.Stack.ContainsAny(Current.RequiredStackable))
            {
                Current = null;
                return;
            }
        }


        private void FindCurrentIfNeeded()
        {
            if (Current) return;

            var maxPriority = int.MinValue;
            BatchWorkerTarget result = null;
            foreach (var current in Filter)
            {
                if (!Subject.Stack.ContainsAny(current.RequiredStackable)) continue;
                if (current.Batch.IsFull.Value) continue;
                if (current.priority < maxPriority) continue;
                
                maxPriority = current.priority;
                result = current;
            }

            Current = result;
        }
    }
}