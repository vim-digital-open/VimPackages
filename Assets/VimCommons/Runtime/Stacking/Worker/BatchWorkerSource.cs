using UnityEngine;
using VimCommons.Runtime.Stacking.Core;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Stacking.Worker
{
    public class BatchWorkerSource : MonoBehaviour
    {
        private static readonly Filter<BatchWorkerSource> Filter = Locator.Filter<BatchWorkerSource>();
        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);

        private ModelStackableBatch _batch;
        public ModelStackableBatch Batch => _batch ??= GetComponent<ModelStackableBatch>();
        public Vector3 CollectPoint => Batch.popPoint.position;
    }
}