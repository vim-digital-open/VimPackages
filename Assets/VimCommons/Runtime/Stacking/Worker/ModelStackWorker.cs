using UnityEngine;
using VimCommons.Runtime.Navigation.NavMeshWalker;
using VimCommons.Runtime.Progression.Building;
using VimCommons.Runtime.Stacking.Core;
using VimCommons.Runtime.Stacking.Worker.State;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.StateMachine;

namespace VimCommons.Runtime.Stacking.Worker
{
    public class ModelStackWorker : ModelNavMeshWalker
    {
        public StackableDefinition[] loadVariants;
        public float patienceSeconds = 5f;
        public ProgressionChannel<float> channelSpeed;
        public Transform ChillPoint { get; private set; }
        public Transform RecyclePoint { get; private set; }

        private readonly FSM<ModelStackWorker, SWorkerAbstract> _fsm = new();
        
        private ModelStack _stack;
        public ModelStack Stack => _stack ??= GetComponentInChildren<ModelStack>();
        
        private Transform _transform;
        public Transform Transform => _transform ??= GetComponent<Transform>();
        public ObservableData<bool> IsHolding => Stack.IsHolding;

        private void Awake()
        {
            channelSpeed.NodeLevel.AddListener(OnSpeedUpgrade);
        }

        private void OnSpeedUpgrade(int obj)
        {
            Agent.speed = channelSpeed.Level;
        }

        public void Init(Transform spawnPoint, Transform chillPoint, Transform recyclePoint)
        {
            InitializeAgent(spawnPoint.position, spawnPoint.rotation);
            ChillPoint = chillPoint;
            RecyclePoint = recyclePoint;
            _fsm.Init(this, new SWorkerStep());
        }

        protected override Vector3 Target => _fsm.State.GetTarget();
    }
}