using UnityEngine;
using VimCommons.Runtime.Stacking.Core;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Stacking.Worker
{
    public class BatchWorkerTarget : MonoBehaviour
    {
        public int priority;
        private static readonly Filter<BatchWorkerTarget> Filter = Locator.Filter<BatchWorkerTarget>();
        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);

        private ModelStackableBatch _batch;
        public ModelStackableBatch Batch => _batch ??= GetComponent<ModelStackableBatch>();

        public Vector3 UnstackPoint => Batch.pushPoint.position;
        public StackableDefinition RequiredStackable => Batch.definition;
    }
}