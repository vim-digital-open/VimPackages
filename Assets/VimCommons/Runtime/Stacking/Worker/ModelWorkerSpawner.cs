using System.Collections.Generic;
using UnityEngine;
using VimCommons.Runtime.Progression.Building;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Stacking.Worker
{
    public class ModelWorkerSpawner: ProgressionBuilding
    {
        public ModelStackWorker prefab;
        public Transform chillPoint;
        public Transform recyclePoint;

        private readonly List<ModelStackWorker> _current = new();
        private void Awake()
        {
            NodeLevel.AddListener(OnLevel);
        }

        private void OnLevel(int level)
        {
            while (_current.Count < level)
            {
                var worker = prefab.SpawnPoolable();
                worker.Init(transform, chillPoint, recyclePoint);
                _current.Add(worker);
            }
        }
    }
}