using VimCommons.Runtime.Progression.Building;
using VimCommons.Runtime.Stacking.Core;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Stacking.Showcase
{
    public class ModelShowcase : ProgressionBuilding
    {
        public ModelStackableBatch batch;
        public int capacity;

        private static readonly Filter<ModelShowcase> Filter = Locator.Filter<ModelShowcase>();
        
        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);
        
        private void Awake() => batch.Capacity.Value = capacity;
    }
}