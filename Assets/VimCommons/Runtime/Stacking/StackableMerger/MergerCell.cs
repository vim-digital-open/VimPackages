using DG.Tweening;
using UnityEngine;
using VimCommons.Runtime.Stacking.Core;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Stacking.StackableMerger
{
    public class MergerCell: MonoBehaviour
    {
        private ModelStackableMerger _merger;
        private Sequence _ez;
        public ModelStackableMerger Merger => _merger ??= GetComponentInParent<ModelStackableMerger>();

        private void OnEnable() => Merger.Cells.Add(this);
        private void OnDisable() => Merger.Cells.Remove(this);
        
        public ModelStackable Content { get; set; }
        public bool IsFree => !Content;

        public void Push(ModelStackable stackable)
        {
            Content = stackable;
            var sourcePos = stackable.Transform.position;
            _ez = DOTween.Sequence().AppendLerp(ez =>
            {
                stackable.Transform.position = Helper.LerpParabolic(sourcePos,transform.position,ez.Linear);
            });
        }

        public ModelStackable Pop()
        {
            _ez?.Kill();
            var result = Content;
            Content = null;
            return result;
        }
    }
}