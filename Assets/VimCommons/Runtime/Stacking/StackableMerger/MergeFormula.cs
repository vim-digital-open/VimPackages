using System;
using VimCommons.Runtime.Stacking.Core;

namespace VimCommons.Runtime.Stacking.StackableMerger
{
    [Serializable]
    public class MergeFormula
    {
        public StackableDefinition source;
        public StackableDefinition result;
    }
}