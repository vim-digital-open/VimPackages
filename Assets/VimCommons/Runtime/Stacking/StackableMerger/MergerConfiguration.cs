using UnityEngine;

namespace VimCommons.Runtime.Stacking.StackableMerger
{
    [CreateAssetMenu]
    public class MergerConfiguration: ScriptableObject
    {
        public MergeFormula[] formulas;
    }
}