using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using VimCommons.Runtime.Progression.Building;
using VimCommons.Runtime.Stacking.Core;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Stacking.StackableMerger
{
    public class ModelStackableMerger : ProgressionBuilding
    {
        public MergerConfiguration configuration;
        public float pickDistance = 1;
        public float timeout = 2;
        public Transform mergePoint;
        private float _cooldownTime;

        public HashSet<MergerCell> Cells { get; } = new();

        private Observable<ModelStack> ChannelStack { get; } = new();
        private Observable MergeFx { get; } = new();

        private void Awake() => ChannelStack.AddListener(OnStack);

        public void OnStack(ModelStack stack)
        {
            if (Time.realtimeSinceStartup < _cooldownTime) return;
            if (TryMerge(stack)) return;
            if (TryDrop(stack)) return;
            TryPick(stack);
        }

        private bool TryMerge(ModelStack stack)
        {
                
            // var variants = configuration.formulas 
            //     .Where(formula => Cells.Where(c=>!c.IsFree).Any(cell => cell.Content.Definition == formula.source))
            //     .Select(formula => formula.source)
            //     .ToArray();
            var variants = (
                from formula in configuration.formulas
                where (
                    from cell in Cells 
                    where !cell.IsFree
                    where cell.Content.Definition == formula.source
                    select cell
                ).Any()
                select formula.source
            ).ToArray();
            
            if (stack.TryExtractAny(variants, out var fromStack))
            {
                var cell = Cells.First(i => i.Content.Definition == fromStack.Definition);
                var fromCell = cell.Pop();
                fromCell.TweenRemove(mergePoint.position, stack.vibrate);
                fromStack.TweenRemove(mergePoint.position);
                var formula = configuration.formulas.FirstOrDefault(i => i.source == fromStack.Definition);
                SpawnMergeResult(formula.result, stack.vibrate);
                return true;
            } 
            return false;
        }

        private bool TryMerge(ModelStackable mergeResult, bool vibrate)
        {
            // var variants = configuration.formulas
            //     .Where(formula => Cells.Where(c=>!c.IsFree).Any(cell => cell.Content.Definition == formula.source))
            //     .Select(formula => formula.source)
            //     .ToArray();
            var variants = (
                from formula in configuration.formulas
                where (
                    from cell in Cells 
                    where !cell.IsFree
                    where cell.Content.Definition == formula.source
                    select cell
                ).Any()
                select formula.source
            ).ToArray();
            
            if (variants.Contains(mergeResult.Definition))
            {
                var cell = Cells.First(i => i.Content.Definition == mergeResult.Definition);
                var fromCell = cell.Pop();
                fromCell.TweenRemove(mergePoint.position, vibrate);
                mergeResult.TweenRemove(mergePoint.position);
                var formula = configuration.formulas.FirstOrDefault(i => i.source == mergeResult.Definition);
                SpawnMergeResult(formula.result, vibrate);
                return true;
            }

            return false;
        }

        private async void SpawnMergeResult(StackableDefinition resultDefinition, bool vibrate)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(0.3f));
            MergeFx.Invoke();
            var mergeResult = resultDefinition.Spawn();
            mergeResult.Init(mergePoint);
            if (TryMerge(mergeResult, vibrate)) return;
            var freeCell = Cells.First(i => i.IsFree);
            freeCell.Push(mergeResult);
        }

        private bool TryDrop(ModelStack stack)
        {
            var variants = configuration.formulas.Select(i => i.source).ToArray();
            var freeCell = Cells.FirstOrDefault(i => i.IsFree);
            if (freeCell != null)
                if (stack.TryExtractAny(variants, out var stackable))
                {
                    freeCell.Push(stackable);
                    _cooldownTime = Time.realtimeSinceStartup + timeout;
                    return true;
                }
            return false;
        }


        private void TryPick(ModelStack stack)
        {
            var busyCell = Cells.Where(i => !i.IsFree).ToArray();
            foreach (var cell in busyCell)
            {
                if (Helper.WithinRadius(cell.transform, stack.Transform, pickDistance))
                {
                    if (stack.CanAccept(cell.Content.Definition))
                    {
                        var stackable = cell.Pop();
                        stack.Append(stackable);
                        _cooldownTime = Time.realtimeSinceStartup + timeout;
                        return;
                    }
                }
            }
        }
    }
}