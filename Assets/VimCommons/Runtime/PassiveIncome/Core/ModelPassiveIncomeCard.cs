using System;
using UnityEngine;
using VimCommons.Runtime.Looting.Inventory;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCommons.Runtime.Progression.ProgressBar;

namespace VimCommons.Runtime.PassiveIncome.Core
{
    public class ModelPassiveIncomeCard : ModelBehaviour
    {
        private static ServiceInventory Inventory => Locator.Resolve<ServiceInventory>();
        private static ModelLevelProgressBar ProgressBar => Locator.Resolve<ModelLevelProgressBar>();

        public PassiveIncomeData Data { get; set; }

        private ObservableData<int> CollectTime { get; } = new();

        private ObservableData<string> LabelTitle { get; } = new();
        private ObservableData<Sprite> SpriteIcon { get; } = new();

        private ObservableData<float> FillProgress { get; } = new();
        private ObservableData<string> LabelFillProgress { get; } = new();
        private ObservableData<float> CurrentProgress => ProgressBar.BarValue;
        private ObservableData<string> LabelCurrentProgress => ProgressBar.CounterLabel;
        private ObservableData<string> LabelAccumulations { get; } = new();
        private ObservableData<bool> CanCollect { get; } = new();

        private ObservableData<bool> Completed { get; } = new();
        private ObservableData<bool> Current { get; } = new();
        private ObservableData<bool> Locked { get; } = new();

        private Observable ChannelCollect { get; } = new();

        public void Init(PassiveIncomeData data)
        {
            Data = data;
            CollectTime.ConnectPref($"CollectTime{Data.saveKey}", ComputeIntegerTimestamp());
            ChannelCollect.AddListener(OnCollect);
            
            LabelTitle.Value = data.title;
            SpriteIcon.Value = data.icon;
            
            Completed.Value = data.state == PassiveIncomeEntryState.Completed;
            Locked.Value = data.state == PassiveIncomeEntryState.Locked;
            Current.Value = data.state == PassiveIncomeEntryState.Current;
            
        }

        public void OnCollect()
        {
            var accumulations = ComputeAccumulations();
            Inventory.Receive(Data.currencyDefinition, accumulations);
            CollectTime.Value = ComputeIntegerTimestamp();
            Refresh();
        }

        private int ComputeAccumulations()
        {
            var currentTime = ComputeIntegerTimestamp();
            var deltaTime = currentTime - CollectTime.Value;
            var accumulations = deltaTime * Data.incomePerSecond;
            return Math.Min(accumulations, Data.capacity);
        }

        public void Refresh()
        {
            var accumulations = ComputeAccumulations();
            CanCollect.Value = accumulations >= Data.minGet;
            LabelAccumulations.Value = $"{accumulations.ToString()}";
            var percent = accumulations * 1f / Data.capacity;
            LabelFillProgress.Value = $"{Mathf.Ceil(percent * 100)}%";
            FillProgress.Value = percent;
        }

        private static int ComputeIntegerTimestamp()
        {
            var currentTime = DateTime.Now;
            var timeSpan = currentTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var seconds = (int)timeSpan.TotalSeconds;
            return seconds;
        }
    }
}