namespace VimCommons.Runtime.PassiveIncome.Core
{
    public enum PassiveIncomeEntryState
    {
        Completed, Current, Locked
    }
}