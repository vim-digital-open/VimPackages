using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.PassiveIncome.Core
{
    public class ModelPassiveIncome : ModelBehaviour
    {
        public List<PassiveIncomeData> data;
        public ModelPassiveIncomeCard cardPrefab;
        public LayoutGroup group;
        public ScrollRect rect;
        
        private ObservableData<bool> Visible { get; } = new();
        private Observable ChannelShow { get; } = new();
        private Observable ChannelClose { get; } = new();

        private List<ModelPassiveIncomeCard> Cards { get; } = new();

        private void Start()
        {
            foreach (var passiveIncomeEntry in data)
            {
                var card = Instantiate(cardPrefab, group.transform);
                card.Init(passiveIncomeEntry);
                Cards.Add(card);
            }
            ChannelShow.AddListener(OnShow);
            ChannelClose.AddListener(OnHide);
        }

        void Update()
        {
            if (!Visible.Value) return;

            foreach (var card in Cards)
                if (card.Data.state == PassiveIncomeEntryState.Completed)
                    card.Refresh();

        }

        private void OnShow()
        {
            Visible.Value = true;
            foreach (var card in Cards)
                card.Refresh();
            rect.normalizedPosition = Vector2.up;
        }

        private void OnHide()
        {
            Visible.Value = false;
        }
    }
}