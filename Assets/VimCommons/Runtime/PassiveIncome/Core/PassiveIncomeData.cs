using System;
using UnityEngine;
using VimCommons.Runtime.Looting.Core;

namespace VimCommons.Runtime.PassiveIncome.Core
{
    [Serializable]
    public struct PassiveIncomeData
    {
        public string saveKey;
        public string title;
        public Sprite icon;
        public LootableDefinition currencyDefinition;
        public int incomePerSecond;
        public int capacity;
        public int minGet;
        public PassiveIncomeEntryState state;
    }
}