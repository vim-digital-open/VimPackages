namespace VimCommons.Runtime.QuestQueue.Core
{
    public interface IQuestEventHandler<T>
    {
        void PushEvent(T payload);
    }
}