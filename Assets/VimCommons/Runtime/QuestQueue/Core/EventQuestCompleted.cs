namespace VimCommons.Runtime.QuestQueue.Core
{
    public class EventQuestCompleted
    {
        public int Level { get; set; }
        public int Step { get; }
        public string Name { get; }

        public EventQuestCompleted(int level, int step, string name)
        {
            Level = level;
            Step = step;
            Name = name;
        }
    }
}