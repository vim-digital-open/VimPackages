using System;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Animations;
using VimCommons.Runtime.Analytics.ServiceAnalytics;
using VimCommons.Runtime.Camera.Core;
using VimCommons.Runtime.LevelManagement.Core;
using VimCommons.Runtime.Looting.Inventory;
using VimCommons.Runtime.Progression.ProgressBar;
using VimCommons.Runtime.QuestQueue.QuestArrows;
using VimCommons.Runtime.QuestQueue.UI;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.QuestQueue.Core
{
    public class ServiceQuestQueue : MonoBehaviour, IQuestQueue
    {
        [SerializeField]
        private string saveKey = "QuestProgress";
        
        private static readonly ServiceContainer<IQuestQueue> Container = Locator.Single<IQuestQueue>();

        private static IQuestArrowSystem QuestArrowSystem => Locator.Resolve<IQuestArrowSystem>();
        private static ModelLevelProgressBar ModelLevelProgressBar => Locator.Resolve<ModelLevelProgressBar>();
        private static ServiceInventory Inventory => Locator.Resolve<ServiceInventory>();
        private static ServiceCamera Camera => Locator.Resolve<ServiceCamera>();
        private static ServiceAnalytics Analytics => Locator.Resolve<ServiceAnalytics>();
        private static ModelUIQuestQueue UIQuestQueue => Locator.Resolve<ModelUIQuestQueue>();
        private static ServiceLevelManager LevelManager => Locator.Resolve<ServiceLevelManager>();

        private AQuest[] _quests;
        private AQuest _current;

        public ObservableData<int> QuestProgress { get; } = new();

        private void Awake()
        {
            Container.Attach(this);
            _quests = GetComponentsInChildren<AQuest>();
            _quests = _quests.OrderBy(i => i.Transform.GetSiblingIndex()).ToArray();
            QuestProgress.ConnectPref(saveKey);
            InitQuest();
        }

        private void OnDestroy()
        {
            Container.Detach(this);
            if (UIQuestQueue)
                UIQuestQueue.Visible.Value = false;
        }

        private void InitQuest()
        {
            if (QuestProgress.Value >= _quests.Length)
            {
                Destroy(gameObject);
                return;
            }
          
            _current = _quests.ElementAt(QuestProgress.Value);
            if (_current.Camera)
                LookAt(_current);
            UIQuestQueue.HasCamera.Value = _current.Camera;
            UIQuestQueue.ClaimVisible.Value = false;
            UIQuestQueue.Visible.Value = _current.showWindow;
            UIQuestQueue.Title.Value = _current.message;
            UIQuestQueue.Icon.Value = _current.icon;
            UIQuestQueue.Bounce.Invoke();
            UIQuestQueue.ProgressVisible.Value = _current switch
            {
                AQuestCount => true,
                _ => false
            };
            _current.Enter();
        }

        private static async void LookAt(AQuest current)
        {
            Camera.Focus(current.Camera);
            await UniTask.Delay(TimeSpan.FromSeconds(current.cameraTime));
            Camera.Forget(current.Camera);
        }

        private void Update()
        {
            if (!_current) return;
            switch (_current)
            {
                case AQuestCount quest:
                    var newProgress =  quest.Current * 1f / quest.Target;
                    UIQuestQueue.Progress.Value = newProgress;
                    UIQuestQueue.ProgressLabel.Value = $"{quest.Current}/{quest.Target}";
                    if (Math.Abs(UIQuestQueue.Progress.Value - newProgress) < float.Epsilon) break;
                    UIQuestQueue.Bounce.Invoke();
                    break;
            }

            if (_current.Done)
            {
                UIQuestQueue.ClaimVisible.Value = true;
                QuestArrowSystem.Remove(_current.arrowTargets);
                if (_current.autoClaim) 
                    CompleteQuest(true);
            }
            else
            {
                UIQuestQueue.ClaimVisible.Value = false;
                QuestArrowSystem.Add(_current.arrowTargets);
            }
        }

        public async void CompleteQuest(bool auto = false)
        {
            ModelLevelProgressBar.AddExperience(_current.rewardExperience);
            foreach (var entry in _current.rewardLoot) 
                Inventory.Receive(entry.type, entry.amount);

            UIQuestQueue.Visible.Value = false;
            _current.Exit();
            if (Analytics)
                Analytics.Send(new EventQuestCompleted(LevelManager.Level.Value, QuestProgress.Value, _current.name));
            QuestProgress.Value += 1;
            _current = null;
            if (auto)
            {
                InitQuest();
                return;
            }

            await UniTask.Delay(TimeSpan.FromSeconds(1)); 
            InitQuest();
        }

        public void LookTarget()
        {
            if (_current.Camera)
                LookAt(_current);
        }

        public void PushEvent<T>(T payload)
        {
            var handler = _current as IQuestEventHandler<T>;
            handler?.PushEvent(payload);
        }
    }
}