using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.QuestQueue.Core
{
    public class QuestConditionalObject: MonoBehaviour
    {
        private static IQuestQueue QuestQueue => Locator.Resolve<IQuestQueue>();
        
        public int destroyStep;

        private void Awake() => QuestQueue.QuestProgress.AddListener(OnQuest);

        private void OnDestroy() => QuestQueue.QuestProgress.RemoveListener(OnQuest);

        private void OnQuest(int obj)
        {
            if (obj < destroyStep) return;
            Destroy(gameObject);
        }
    }
}