using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.QuestQueue.Core
{
    public interface IQuestQueue
    {
        ObservableData<int> QuestProgress { get; }
        void CompleteQuest(bool auto = false);
        void LookTarget();
        void PushEvent<T>(T payload);
    }
}