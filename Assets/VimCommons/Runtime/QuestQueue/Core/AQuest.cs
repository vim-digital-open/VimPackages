using UnityEngine;
using VimCommons.Runtime.Camera.Core;
using VimCommons.Runtime.Looting.Core;

namespace VimCommons.Runtime.QuestQueue.Core
{
    public abstract class AQuest: MonoBehaviour
    {
        private Transform _transform;
        public Transform Transform => _transform ??= transform;

        public bool showWindow = true;
        public string message;
        public Sprite icon;

        public float cameraTime = 1.5f;
        public Transform[] arrowTargets;

        [Header("Completion params")]
        public bool autoClaim;
        public int rewardExperience;
        public LootEntry[] rewardLoot;
        
        public CameraState Camera { get; private set; }

        private void Awake()
        {
            if (gameObject.TryGetComponent<CameraState>(out var state))
                Camera = state;
        }

        public abstract bool Done { get; }

        public virtual void Enter() => print($"QuestQueue Enter {GetType().Name}");

        public virtual void Exit() => print($"QuestQueue Exit {GetType().Name}");
    }
}