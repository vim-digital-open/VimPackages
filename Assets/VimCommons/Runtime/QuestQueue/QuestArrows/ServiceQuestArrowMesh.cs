using System.Collections.Generic;
using UnityEngine;
using VimCommons.Runtime.Camera.Core;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.QuestQueue.QuestArrows
{
    public class ServiceQuestArrowMesh : MonoBehaviour, IQuestArrowSystem
    {
        public Transform arrowPrefab;
        public float lerpForce = 10;
        
        [Header("Turning mode")]
        public float turnSpeed = 1;
        public float verticalTurningOffset = 3;
        
        [Header("Pointer mode")]
        public float verticalPointerOffset = 1;
        public float pointerDistance = 3;

        [Header("Pulse amplitude")]
        public float pulseFrequency = 1;
        public float pulseAmplitude = 1;
        
        private static readonly ServiceContainer<IQuestArrowSystem> Container = Locator.Single<IQuestArrowSystem>();
        public void Awake() => Container.Attach(this);
        private void OnDestroy() => Container.Detach(this);
        
        private static ArrowAnchor Anchor => Locator.Resolve<ArrowAnchor>();
        private static ServiceCamera Camera => Locator.Resolve<ServiceCamera>();
        
        private readonly Dictionary<Transform, Transform> _data = new();

        private void LateUpdate()
        {
            if (!Anchor) return;
            foreach (var (target, arrow) in _data)
            {
                if (!target) {
                    RemoveTarget(target);
                    return;
                }
                RunArrow(target, arrow);
            }
        }
        
        private void RunArrow(Transform target, Transform arrow)
        {
            var targetObjectPosition = target.position;
            var targetInvisible = Camera.CantSee(target);
            var pulse = Mathf.Sin(Time.time * pulseFrequency) * pulseAmplitude;
            if (targetInvisible)
            {
                var anchorPos = Anchor.transform.position;
                var targetPosition = Vector3.MoveTowards(anchorPos, targetObjectPosition, pointerDistance + pulse);
                targetPosition.y = verticalPointerOffset;
                arrow.position = Vector3.Lerp(arrow.position, targetPosition, Time.deltaTime * lerpForce);

                var targetRotation = Quaternion.LookRotation(targetObjectPosition - anchorPos, Vector3.up);
                arrow.rotation = Quaternion.Lerp(arrow.rotation, targetRotation, Time.deltaTime * lerpForce);
            }
            else
            {
                var targetPosition = targetObjectPosition;
                targetPosition.y = verticalTurningOffset + pulse;
                arrow.position = Vector3.Lerp(arrow.position, targetPosition, Time.deltaTime * lerpForce);
                
                var targetRotation = CalculateSpinningRotation();
                arrow.rotation = Quaternion.Lerp(arrow.rotation, targetRotation, Time.deltaTime * lerpForce);
            }
        }

        private Quaternion CalculateSpinningRotation()
        {
            var scaledTime = Time.time * turnSpeed;
            var x = Mathf.Sin(scaledTime);
            var z = Mathf.Cos(scaledTime);
            var turn = new Vector3(x, 0, z); 
            return Quaternion.LookRotation(Vector3.down, turn);
        }

        public void Add(params Transform[] targets)
        {
            foreach (var item in targets) 
                AddTarget(item);
        }

        private void AddTarget(Transform target)
        {
            if (_data.ContainsKey(target)) return; 
            var newArr = arrowPrefab.SpawnPoolable();
            newArr.localScale = Vector3.one;

            _data[target] = newArr;
        }

        public void Remove(params Transform[] targets)
        {
            foreach (var item in targets) 
                RemoveTarget(item);
        }

        private void RemoveTarget(Transform target)
        {
            if (_data.Remove(target, out var arrow)) 
                arrowPrefab.RemovePoolable(arrow);
        }
    }
}