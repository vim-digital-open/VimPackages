using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.QuestQueue.QuestArrows
{
    public class ArrowAnchor: MonoBehaviour
    {
        private static readonly ServiceContainer<ArrowAnchor> Container = Locator.Single<ArrowAnchor>();

        private void Awake() => Container.Attach(this);
        private void OnDestroy() => Container.Detach(this);
    }
}