using System.Collections.Generic;
using UnityEngine;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.QuestQueue.QuestArrows
{
    public class ServiceQuestArrowLine : MonoBehaviour, IQuestArrowSystem
    {
        public LineRenderer arrowPrefab;
        public float vOffset = 1;

        private static readonly ServiceContainer<IQuestArrowSystem> Container = Locator.Single<IQuestArrowSystem>();
        public void Awake() => Container.Attach(this);
        private void OnDestroy() => Container.Detach(this);
        
        private static ArrowAnchor Anchor => Locator.Resolve<ArrowAnchor>();

        private readonly Dictionary<Transform, LineRenderer> _data = new();

        private void Update()
        {
            if (!Anchor) return;
            foreach (var (target, arrow) in _data)
            {
                if (!target) {
                    RemoveTarget(target);
                    return;
                }
                RunArrow(target, arrow);
            }
        }
        
        private void RunArrow(Transform target, LineRenderer arrow)
        {

            var from = Anchor.transform.position;
            var to = target.position + Vector3.up * 3;
            
            var trimFrom = Vector3.MoveTowards(from, to, 1);
            var trimTo = Vector3.MoveTowards(to, from, 1);
            
            trimFrom.y = vOffset;
            trimTo.y = vOffset;

            arrow.SetPosition(0, trimFrom);
            arrow.SetPosition(1, trimTo);
        }

        public void Add(params Transform[] targets)
        {
            foreach (var item in targets) 
                AddTarget(item);
        }

        private void AddTarget(Transform target)
        {
            if (_data.ContainsKey(target)) return; 
            var newArr = arrowPrefab.SpawnPoolable();
            newArr.transform.localScale = Vector3.one;
            newArr.positionCount = 2;

            _data[target] = newArr;
        }

        public void Remove(params Transform[] targets)
        {
            foreach (var item in targets) 
                RemoveTarget(item);
        }

        private void RemoveTarget(Transform target)
        {
            if (_data.Remove(target, out var arrow)) 
                arrowPrefab.RemovePoolable(arrow);
        }
    }
}