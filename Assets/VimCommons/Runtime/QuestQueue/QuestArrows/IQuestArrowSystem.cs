using UnityEngine;

namespace VimCommons.Runtime.QuestQueue.QuestArrows
{
    public interface IQuestArrowSystem
    {
        void Add(params Transform[] targets);
        void Remove(params Transform[] targets);
    }
}