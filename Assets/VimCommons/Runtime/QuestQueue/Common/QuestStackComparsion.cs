using System;
using VimCommons.Runtime.QuestQueue.Core;
using VimCommons.Runtime.Stacking.Core;
using VimCore.Runtime.MVVM.ViewModels;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestStackComparsion : AQuest
    {
        public ModelStack stack;
        public ComparsionMode mode = ComparsionMode.Exact;
        public int count;
        public bool invert;
        
        public override bool Done {
            get
            {
                switch (mode)
                {
                    case ComparsionMode.Exact:
                        return invert != (stack.Count == count);
        
                    case ComparsionMode.Min:
                        return invert != (stack.Count >= count);
                    
                    case ComparsionMode.Max:
                        return invert != (stack.Count <= count);
                }

                throw new Exception("Inconsistent state!");
            }
        }
    }
}