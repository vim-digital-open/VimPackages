using System;
using Cysharp.Threading.Tasks;
using VimCommons.Runtime.QuestQueue.Core;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestDelay : AQuest
    {
        public float delaySeconds;

        private bool IsDone { get; set; }

        public override async void Enter()
        {
            await UniTask.Delay(TimeSpan.FromSeconds(delaySeconds));
            IsDone = true;
        }

        public override bool Done => IsDone;
    }
}