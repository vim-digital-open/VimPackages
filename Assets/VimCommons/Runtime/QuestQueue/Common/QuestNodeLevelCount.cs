using VimCommons.Runtime.Progression.Node;
using VimCommons.Runtime.QuestQueue.Core;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestNodeLevelCount: AQuestCount
    {
        public ProgressionNode node;
        public int nodeLevelReq = 1;
        public override int Current => node.NodeLevel.Value;
        public override int Target => nodeLevelReq;
    }
}