using System;
using System.Linq;
using VimCommons.Runtime.QuestQueue.Core;
using VimCommons.Runtime.Stacking.Core;
using VimCommons.Runtime.Stacking.StackableMerger;
using VimCore.Runtime.MVVM.ViewModels;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestMergerCondition : AQuest
    {
        public ModelStackableMerger merger;
        public StackableDefinition requiredDefinition;
        public ComparsionMode mode;
        public int amount;
        
        public override bool Done
        {
            get
            {
                var contents = merger.Cells.Where(cell=>!cell.IsFree).Select(cell => cell.Content);
                var filtered = contents.Where(stackable => stackable.Definition == requiredDefinition);
                var requiredContentAmount = filtered.Count();
                switch (mode)
                {
                    case ComparsionMode.Exact: return requiredContentAmount == amount;
                    case ComparsionMode.Min: return requiredContentAmount >= amount;
                    case ComparsionMode.Max: return requiredContentAmount <= amount;
                }

                throw new Exception("Invalid state");
            }
        }
    }
}
