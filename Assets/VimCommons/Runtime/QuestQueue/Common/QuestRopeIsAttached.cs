using VimCommons.Runtime.QuestQueue.Core;
using VimCommons.Runtime.Rope.JointRope;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestRopeIsAttached : AQuest
    {
        public ModelJointRope rope;
        public bool invert;
        public override bool Done => rope.IsAttached != invert;
    }
}
