using System;
using VimCommons.Runtime.Looting.Core;
using VimCommons.Runtime.QuestQueue.Core;
using VimCore.Runtime.MVVM.ViewModels;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestLootableBatch : AQuest
    {
        public ModelLootableBatch batch;
        public ComparsionMode mode = ComparsionMode.Exact;
        public int count;
        public bool invert;
        
        public override bool Done {
            get
            {
                switch (mode)
                {
                    case ComparsionMode.Exact:
                        return invert != (batch.Count == count);
        
                    case ComparsionMode.Min:
                        return invert != (batch.Count >= count);
                    
                    case ComparsionMode.Max:
                        return invert != (batch.Count <= count);
                }

                throw new Exception("Inconsistent state!");
            }
        }
    }
}