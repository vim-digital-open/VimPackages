using VimCommons.Runtime.Progression.Node;
using VimCommons.Runtime.QuestQueue.Core;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestNodeUpgrader : AQuest
    {
        public ProgressionNode node;

        public override bool Done
        {
            get
            {
                node.Upgrade();
                return true;
            }
        }
    }
}