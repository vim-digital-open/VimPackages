using UnityEngine;
using VimCommons.Runtime.QuestQueue.Core;
using VimCommons.Runtime.Rope.JointRope;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestRopeConnectedTo : AQuest
    {
        public ModelJointRope rope;
        public Transform anchor;
        public override bool Done => rope.IsAttachedTo(anchor);
    }
}