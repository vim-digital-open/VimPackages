using VimCommons.Runtime.Progression.Node;
using VimCommons.Runtime.QuestQueue.Core;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestNodeLevel: AQuest
    {
        public ProgressionNode node;
        public int nodeLevelReq = 1;
        public override bool Done => node.NodeLevel.Value >= nodeLevelReq;
    }
}