using VimCommons.Runtime.QuestQueue.Core;
using VimCommons.Runtime.Stacking.Core;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestStackCount : AQuestCount
    {
        public ModelStack stack;
        public int count;
        
        public StackableDefinition definition;

        public override int Current => stack.CountAny(definition);
        public override int Target => count;
    }
}