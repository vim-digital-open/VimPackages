using System;
using VimCommons.Runtime.QuestQueue.Core;
using VimCommons.Runtime.Stacking.Core;
using VimCore.Runtime.MVVM.ViewModels;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestStackableBatch : AQuest
    {
        public ModelStackableBatch batch;
        public ComparsionMode mode = ComparsionMode.Exact;
        public int count;
        public bool invert;
        
        public override bool Done {
            get
            {
                switch (mode)
                {
                    case ComparsionMode.Exact:
                        return invert != (batch.Amount.Value == count);
        
                    case ComparsionMode.Min:
                        return invert != (batch.Amount.Value >= count);
                    
                    case ComparsionMode.Max:
                        return invert != (batch.Amount.Value <= count);
                }

                throw new Exception("Inconsistent state!");
            }
        }
    }
}