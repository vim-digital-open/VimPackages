using UnityEngine;
using VimCommons.Runtime.QuestQueue.Core;
using VimCommons.Runtime.QuestQueue.QuestArrows;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestTriggerVisit : AQuest
    {
        public Transform visitor;
        
        private Collider _hitbox;
        public Collider Hitbox => _hitbox ??= GetComponent<Collider>(); 
        private static ArrowAnchor Anchor => Locator.Resolve<ArrowAnchor>();

        public override bool Done => Hitbox.bounds.Contains(visitor.position);
    }
}