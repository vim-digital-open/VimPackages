using VimCommons.Runtime.Looting.Core;
using VimCommons.Runtime.Looting.Inventory;
using VimCommons.Runtime.QuestQueue.Core;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.QuestQueue.Common
{
    public class QuestLootAmount: AQuestCount
    {
        private static ServiceInventory Inventory => Locator.Resolve<ServiceInventory>();
        
        public LootableDefinition type;
        public int requiredAmount;

        public override int Current => Inventory.GetAmount(type);
        public override int Target => requiredAmount;
    }
}