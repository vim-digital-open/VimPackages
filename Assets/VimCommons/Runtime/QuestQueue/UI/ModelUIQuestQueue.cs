using UnityEngine;
using VimCommons.Runtime.QuestQueue.Core;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.QuestQueue.UI
{
    public class ModelUIQuestQueue : ModelBehaviour
    {
        private static readonly ServiceContainer<ModelUIQuestQueue> Container = Locator.Single<ModelUIQuestQueue>();

        private static IQuestQueue QuestQueue => Locator.Resolve<IQuestQueue>();

        public ObservableData<bool> Visible { get; } = new();
        public ObservableData<string> Title { get; } = new();
        public ObservableData<Sprite> Icon { get; } = new();
        public ObservableData<bool> ProgressVisible { get; } = new();
        public ObservableData<string> ProgressLabel { get; } = new();
        public ObservableData<float> Progress { get; } = new();
        public ObservableData<bool> HasCamera { get; } = new();
        public ObservableData<bool> ClaimVisible { get; } = new();
        public Observable Bounce { get; } = new();
        private Observable BtnFind { get; } = new();
        private Observable BtnClaim { get; } = new();

        private void Awake() => Container.Attach(this);
        private void OnDestroy() => Container.Detach(this);

        private void Start()
        {
            BtnClaim.AddListener(OnClaim);
            BtnFind.AddListener(OnFind);
        }

        public void OnFind() => QuestQueue.LookTarget();

        public void OnClaim() => QuestQueue.CompleteQuest();
    }
}