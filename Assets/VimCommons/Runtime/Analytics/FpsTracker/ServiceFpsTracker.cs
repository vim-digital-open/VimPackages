using UnityEngine;

namespace VimCommons.Runtime.Analytics.FpsTracker
{
    public class ServiceFpsTracker : MonoBehaviour
    {
        private float _minDelta;
        private float _midDelta;
        private float _maxDelta;

        public void Step()
        {
            _midDelta = Time.unscaledDeltaTime;

            _minDelta = int.MaxValue;
            _maxDelta = 0;
        }
        private void Update()
        {
            var deltaTime = Time.unscaledDeltaTime;
            
            _midDelta = Mathf.Lerp(_midDelta, deltaTime, 0.1f);
            _minDelta = Mathf.Min(_minDelta, deltaTime);
            _maxDelta = Mathf.Max(_maxDelta, deltaTime);
        }
    }
}