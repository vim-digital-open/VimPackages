using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Analytics.ServiceAnalytics
{
    public class ServiceAnalytics: MonoBehaviour
    {
        private static readonly Filter<AAnalyticsProvider> Providers = Locator.Filter<AAnalyticsProvider>();

        private static readonly ServiceContainer<ServiceAnalytics> Container = Locator.Single<ServiceAnalytics>();
        private void Awake() => Container.Attach(this);
        private void OnDestroy() => Container.Detach(this);
        
        public void Send<T>(T payload)
        {
            foreach (var provider in Providers) 
                provider.Send(payload);
        }
    }

}