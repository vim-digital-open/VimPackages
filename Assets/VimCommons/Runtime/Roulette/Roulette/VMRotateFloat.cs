using UnityEngine;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Roulette.Roulette
{
    public class VMRotateFloat : AViewModel<float>
    {
        public override void OnValue(float value)
        {
            transform.localEulerAngles = Vector3.back * value;
        }
    }
}
