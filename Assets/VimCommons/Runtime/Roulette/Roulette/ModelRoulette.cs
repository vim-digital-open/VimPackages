using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using VimCommons.Runtime.Roulette.Reward;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Roulette.Roulette
{
    public class ModelRoulette : ModelBehaviour
    {
        public float spinSpeed = 512;
        public float spinDuration = 10;
        public RouletteReward[] rewards;

        private ObservableData<RouletteReward[]> Rewards { get; } = new();

        private ObservableData<bool> Visible { get; } = new();
        private ObservableData<bool> IsRun { get; } = new();
        private ObservableData<string> Message { get; } = new();
        private ObservableData<float> RouletteAngle { get; } = new();
        private Observable BtnShow { get; } = new();
        private Observable BtnHide { get; } = new();
        private Observable BtnRoll { get; } = new();

        private void Start()
        {
            BtnShow.AddListener(OnShow);
            BtnHide.AddListener(OnHide);
            BtnRoll.AddListener(OnRoll);
            Assert.AreNotEqual(rewards.Length, 0, "Rewards are not defined");
            Rewards.Value = rewards;
        }

        public void OnShow()
        {
            Message.Value = "Roll the roulette";
            Visible.Value = true;
        }

        public void OnHide()
        {
            if (IsRun.Value) return;
            Visible.Value = false;
        }

        public void OnRoll()
        {
            if (IsRun.Value) return;
            IsRun.Value = true;
            var curSpeed = spinSpeed * Random.Range(0.8f, 1.2f);
            DOTween.Sequence().AppendLerp(spinDuration, ez =>
            {
                var deceleration = 1 - ez.QuadOut;
                var spinStep = deceleration * Time.deltaTime;
                RouletteAngle.Value += spinStep * curSpeed;
            }).AppendCallback(() =>
            {
                IsRun.Value = false;
                var reward = CalculateReward();
                Message.Value = "Your reward: \n" + reward.title;
                reward.Apply();
            });
        }

        private RouletteReward CalculateReward()
        {
            var angle = RouletteAngle.Value % 360;
            var sectorSize = 360 / rewards.Length;
            var index = Mathf.FloorToInt(angle / sectorSize);
            return rewards[index];
        }
    }
}
