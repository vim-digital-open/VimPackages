using UnityEngine;
using VimCommons.Runtime.Roulette.Reward;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Roulette.Roulette
{
    public class VMSectorBuilder : AViewModel<RouletteReward[]>
    {
        public VisualSector.VisualSector prefab;


        public override void OnValue(RouletteReward[] value)
        {
            var sectorAngle = 360 / value.Length;
            for (var index = 0; index < value.Length; index++)
            {
                var reward = value[index];
                var entry = Instantiate(prefab, transform);
                entry.transform.localEulerAngles = Vector3.forward * sectorAngle * (index+0.5f);
                entry.Init(reward);
            }
        }
    }
}
