using VimCommons.Runtime.Looting.Core;
using VimCommons.Runtime.Progression.Building;

namespace VimCommons.Runtime.Looting.LootPile
{
    public class ModelLootPile : ProgressionBuilding
    {
        public int count;
        public ModelLootableBatch batch;

        private void Awake()
        {
            if (node.NodeLevel.Value > 0)
            {
                Destroy(gameObject);
                return;
            } 
            batch.Append(transform, count);
        }

        private void Update()
        {
            if (batch.Count > 0) return;
            node.Upgrade();
            Destroy(gameObject);
        }
    }
}
