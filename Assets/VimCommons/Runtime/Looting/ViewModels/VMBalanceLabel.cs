using VimCommons.Runtime.Looting.Core;
using VimCommons.Runtime.Looting.Inventory;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Looting.ViewModels
{
    public class ModelBalanceLabel : ModelBehaviour
    {
        public LootableDefinition definition;
        private static ServiceInventory Inventory => Locator.Resolve<ServiceInventory>();

        public ObservableData<bool> Visible { get; } = new();
        public ObservableData<string> Label { get; } = new();

        private void Start()
        {
            var balance = Inventory.GetBalance(definition);
            balance.AddListener(OnBalance);
        }

        private void OnDestroy()
        {
            if (!Inventory) return;
            var balance = Inventory.GetBalance(definition);
            balance.RemoveListener(OnBalance);
        }

        private void OnBalance(int value)
        {
            Visible.Value = value > 0;
            Label.Value = value.ToString();
        }
    }
}
