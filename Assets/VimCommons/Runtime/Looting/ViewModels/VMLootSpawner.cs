using UnityEngine;
using VimCommons.Runtime.Looting.Core;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Looting.ViewModels
{
    
    public class VMLootSpawner : AViewModel
    {
        [Range(1, 10)]
        public int amount;
        public LootableDefinition loot;

        public override void OnValue()
        {
            for (int i = 0; i < amount; i++) 
                SpawnLootable();
        }

        private void SpawnLootable()
        {
            var posFrom = transform.position;
            var offset =  Random.insideUnitSphere;
            offset.y = 0;
            offset.Normalize();
            var posTo = posFrom + offset;
            loot.Spawn(posFrom, posTo);
        }
    }
}