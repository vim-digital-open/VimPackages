using UnityEngine;
using VimCommons.Runtime.Looting.Core;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Looting.Inventory
{
    public class ServiceInventory: MonoBehaviour
    {
        private const string Key = "__inventory_";

        private static readonly ServiceContainer<ServiceInventory> Container = Locator.Single<ServiceInventory>();
        private void Awake() => Container.Attach(this);
        private void OnDestroy() => Container.Detach(this);

        private LootAccount _account;
        public LootAccount Account => _account ??= new LootAccount(Key);

        public ObservableData<int> GetBalance(LootableDefinition definition) => Account.GetBalance(definition);
        
        public int GetAmount(LootableDefinition type) => Account.GetBalance(type).Value;

        public bool CanPay(LootEntry[] cost) => Account.CanPay(cost);
        public bool TryPay(LootEntry[] cost) => Account.TryPay(cost);
        
        public void Receive(LootableDefinition definition, int amount) => Account.Receive(definition, amount);
    }
}