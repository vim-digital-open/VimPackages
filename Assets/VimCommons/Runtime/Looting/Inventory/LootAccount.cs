using System.Collections.Generic;
using VimCommons.Runtime.Looting.Core;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Looting.Inventory
{
    public class LootAccount
    {
        public readonly string id;

        public LootAccount(string accountId) => id = accountId;
        
        private Dictionary<LootableDefinition, ObservableData<int>> Cache { get; } = new();
        
        
        public ObservableData<int> GetBalance(LootableDefinition definition)
        {
            if (Cache.TryGetValue(definition, out var result))
            {
                return result;
            }
            var newObservable = new ObservableData<int>();
            Cache[definition] = newObservable;
            newObservable.ConnectPref(id+definition.guid);
            return newObservable;
        }

        public void Receive(LootEntry[] entries)
        {
            foreach (var entry in entries) 
                Receive(entry);
        }

        public void Receive(LootEntry entry) => Receive(entry.type, entry.amount);
        public void Receive(LootableDefinition type, int amount) => GetBalance(type).Value += amount;

        public bool CanPay(LootEntry[] entries)
        {
            foreach (var entry in entries)
            {
                if (!CanPay(entry))
                    return false;
            }
            return true;
        }
        public bool CanPay(LootEntry entry) => CanPay(entry.type, entry.amount);
        public bool CanPay(LootableDefinition type, int amount) => GetBalance(type).Value >= amount;

        public bool TryPay(params LootEntry[] entries)
        {
            if (!CanPay(entries)) return false;
            foreach (var entry in entries) 
                Pay(entry);
            return true;
        }

        private void Pay(LootEntry entry) => Pay(entry.type, entry.amount);
        private void Pay(LootableDefinition type, int amount) => GetBalance(type).Value -= amount;

        public void Clear()
        {
            foreach (var (definition, observable) in Cache) 
                observable.Value = 0;
        }
    }
}