using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using VimCommons.Runtime.Vibration.ServiceVibration;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Looting.Core
{
    [CreateAssetMenu]
    public class LootableDefinition: ScriptableObjectWithGuid
    {
        public ModelLootable prefab;
        public Sprite icon;
        public string iconLabel;
        public string TMProIcon => $"<sprite name={iconLabel}>";
        
        private static readonly Filter<ModelLootable> Filter = Locator.Filter<ModelLootable>();
        private static ServiceVibration Vibration => Locator.Resolve<ServiceVibration>();
        public ModelLootable SpawnInactive()
        {
            var instance = prefab.SpawnPoolable();
            instance.Init(this);
            return instance;
        }
        
        public void Spawn(Vector3 posFrom, Vector3 posTo)
        {
            var result = SpawnInactive();
            result.Transform.position = posFrom;
            result.Transform.localScale = Vector3.zero;
            
            DOTween.Sequence().AppendLerp(0.3f, ez => {
                result.Transform.localPosition = Helper.LerpParabolic(posFrom, posTo, ez.Linear, 2);
                result.Transform.localScale = Vector3.one * ez.BackOut;
            }).AppendCallback(() => Filter.Add(result));
        }

        internal void Remove(ModelLootable lootable, Transform target, bool vibrate = false)
        {
            if (lootable == null || lootable.Transform == null) return;

            Filter.Remove(lootable);
            var from = lootable.Transform.position;
            DOTween.Sequence().AppendLerp(ez =>
            {
                lootable.Transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, ez.Linear);
                lootable.Transform.position = Helper.LerpParabolic(from, target.position + Vector3.up, ez.Linear);
            }).AppendCallback(() =>
            {
                prefab.RemovePoolable(lootable);
                if (vibrate)
                    Vibration?.Medium();
            });
        }
        

        public async void Animate(int amount, Transform src, Transform dest)
        {
            var count = Mathf.Min(amount, 50);
            var delay = 100;
            for (var i = 0; i < count; i++)
            {
                await UniTask.Delay(delay, DelayType.UnscaledDeltaTime);
                delay -= delay/5;
                Animate(src, dest);
            }
        }

        private void Animate(Transform from, Transform to)
        {
            var item = SpawnInactive();

            if (item.transform == null) return;

            var srcPos = from.position + Vector3.up;
            var srcRot = Random.rotation;

            item.Transform.position = srcPos;
            item.Transform.rotation = srcRot;
            item.Transform.localScale = Vector3.zero;

            var spread = Random.insideUnitSphere;
            spread *= spread.magnitude;

            var middlePos = Vector3.Lerp(from.position, to.position, 0.5f) + Vector3.up * 4 + spread * 2;
            var middleRot = Random.rotation;

            var destPos = to.position + Vector3.up;
            var destRot = Random.rotation;

            DOTween.Sequence().AppendLerp(ez =>
            {
                item.Transform.rotation = Quaternion.SlerpUnclamped(srcRot, middleRot, ez.QuadOut);
                item.Transform.position = Helper.LerpParabolic(srcPos, middlePos, ez.QuadOut);
                item.Transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, ez.Linear);
            }).AppendLerp(ez =>
            {
                item.Transform.position = Helper.LerpParabolic(middlePos, destPos, ez.QuadOut);
                item.Transform.rotation = Quaternion.SlerpUnclamped(middleRot, destRot, ez.QuadOut);
                item.Transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, ez.Linear);
            }).AppendCallback(() => prefab.RemovePoolable(item));
        }
    }
}