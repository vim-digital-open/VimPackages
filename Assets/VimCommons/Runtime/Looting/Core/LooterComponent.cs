using UnityEngine;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Looting.Core
{
    public class LooterComponent:  AViewModel<SignalLoot>
    {
        public float lootDistance = 2;
        public bool vibrateOnLoot;

        private static readonly Filter<ModelLootable> Lootables = Locator.Filter<ModelLootable>();
        private static readonly Filter<ModelLootableBatch> Batches = Locator.Filter<ModelLootableBatch>();

        private Transform _transform;
        public Transform Transform => _transform ??= transform;

        internal void Loot(LootableDefinition definition, int amount = 1) => Observable.Invoke(new SignalLoot(definition, amount));

        public void Update()
        {
            foreach (var lootable in Lootables) lootable.Tick(this);
            foreach (var batch in Batches) batch.Tick(this);
        }
    }
}