using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Looting.Core
{
    public class SignalLoot
    {
        public LootableDefinition Definition { get; }
        public int Amount { get; }

        public SignalLoot(LootableDefinition definition, int amount)
        {
            Definition = definition;
            Amount = amount;
        }

    }
}