using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;
using Random = UnityEngine.Random;

namespace VimCommons.Runtime.Looting.Core
{
    public class ModelLootableBatch: ModelBehaviour
    {       
        public int maxAmount = 100; 
        public Vector3 size = new(0.6f,0.15f,1.2f);
        public Vector2Int table = new(2, 4);
        [FormerlySerializedAs("type")]
        public LootableDefinition definition;
        
        private static readonly Filter<ModelLootableBatch> Filter = Locator.Filter<ModelLootableBatch>();
        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);
        
        private Transform _transform;
        public Transform Transform => _transform ??= transform;

        private Collider _hitbox;
        public Collider Hitbox => _hitbox ??= GetComponent<Collider>(); 
        
        
        private readonly Stack<ModelLootable> _stack = new();
        public int Count => _stack.Count;

        private ObservableData<int> Amount { get; } = new();
        private Observable Bounce { get; } = new();
        private ObservableData<string> Label { get; } = new();

        private void Awake() => Amount.AddListener(OnAmount);
        
        private void OnDestroy()
        {
            foreach (var lootable in _stack) 
                lootable.Remove(Transform);
            _stack.Clear();
            Amount.Value = 0;
        }

        private void OnAmount(int obj)
        {
            Label.Value = obj.ToString();
            Bounce.Invoke();
        }

        public void Append(Transform srcPos, int amount)
        {
            for (var i = 0; i < amount; i++) 
                AppendItem(srcPos);
        }

        private void AppendItem(Transform srcPos)
        {
            Amount.Value += 1;
            if (Amount.Value >= maxAmount) return;
            
            var index = _stack.Count;
            var coin = definition.SpawnInactive();
            var posFrom = srcPos.position + Vector3.up;
            var posTo = Transform.position + Transform.rotation * GridPos(index, table.x, table.y);
            var rotFrom = Random.insideUnitSphere.normalized;
            var rotTo =  Transform.eulerAngles + Vector3.up * Random.Range(-5, 5);
            coin.Transform.localScale = Vector3.zero;
            _stack.Push(coin);
            DOTween.Sequence().AppendLerp(ez =>
            {
                coin.Transform.localPosition = Helper.LerpParabolic(posFrom, posTo, ez.Linear);
                coin.Transform.eulerAngles = Vector3.SlerpUnclamped(rotFrom, rotTo, ez.Linear);
                coin.Transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, ez.Linear);
            });
        }

        private Vector3 GridPos(int a, int dimB, int dimC)
        {
            var b = a % dimB;
            a /= dimB;
            var c = a % dimC;
            a /= dimC;

            var grid = new Vector3(
                c - 0.5f * (dimC - 1), 
                a, 
                b - 0.5f * (dimB - 1));

            return Vector3.Scale(grid,size);
        }

        public void Tick(LooterComponent looter)
        {
            if (Count < 1) return;
            if (!Hitbox.bounds.Contains(looter.Transform.position)) return;
            Collect(looter);
        }
        
        public async void Collect(LooterComponent looter)
        {
            while (_stack.TryPop(out var item))
            {
                item.Remove(looter.Transform, looter.vibrateOnLoot);
                await UniTask.Yield();
            }
            looter.Loot(definition, Amount.Value);
            Amount.Value = 0;
        }
    }
}