using UnityEngine;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Looting.Core
{
    public class ModelLootable : ModelBehaviour
    {
        public LootableDefinition Definition { get; private set; }

        private Transform _transform;
        public Transform Transform => _transform ??= transform;
        
        private static readonly Filter<ModelLootable> Filter = Locator.Filter<ModelLootable>();        
        
        public void Init(LootableDefinition definition)
        {
            Definition = definition;
            Transform.localScale = Vector3.one;
            Transform.rotation = Quaternion.identity;
        }

        public void Tick(LooterComponent looter)
        {
            if (!Helper.WithinRadius(Transform, looter.Transform, looter.lootDistance)) return;
            looter.Loot(Definition);
            Remove(looter.Transform, looter.vibrateOnLoot);
        }

        public void Remove(Transform target, bool vibrate = false) => Definition.Remove(this, target, vibrate);
    }
}
