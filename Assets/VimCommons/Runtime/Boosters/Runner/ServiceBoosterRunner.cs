using UnityEngine;
using VimCommons.Runtime.Boosters.Core;
using VimCommons.Runtime.Boosters.UICard;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Boosters.Runner
{
    public class ServiceBoosterRunner : MonoBehaviour
    {
        
        private static readonly Filter<Booster> Boosters = Locator.Filter<Booster>();
        private static readonly Filter<ModelBoosterCard> Cards = Locator.Filter<ModelBoosterCard>();

        private void Update()
        {
            foreach (var booster in Boosters) booster.Tick();
            foreach (var card in Cards) card.Tick();
        }
    }
}