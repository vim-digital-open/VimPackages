using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

namespace VimCommons.Runtime.Progression.UIProgressionList
{
    [Serializable]
    public class DataProgressionCardGroup
    {
        [FormerlySerializedAs("header")]
        public string headerLabel;
        public Color headerColor;
        
        public List<DataProgressionCard> cards;
        public bool IsUnlocked => cards.Any(i => i.IsUnlocked);
    }
}