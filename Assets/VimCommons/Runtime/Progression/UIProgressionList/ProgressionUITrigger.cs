using VimCommons.Runtime.Triggers.TriggerSystem;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Progression.UIProgressionList
{
    public class ProgressionUITrigger : ModelBehaviour
    {
        public DataProgressionCardList content;
        private static ModelUILevelProgression UILevelProgression => Locator.Resolve<ModelUILevelProgression>();
        public Observable<SignalTrigger> ChannelVisit { get; } = new();

        private void Awake() => ChannelVisit.AddListener(OnVisit);

        public void OnVisit(SignalTrigger sig)
        {
            if(sig.State == TriggerState.Enter)
                UILevelProgression.Show(content);
        }
    }
}