using UnityEngine;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Progression.UIProgressionList
{
    public class ProgressionHeader: ModelBehaviour
    {
        private ObservableData<string> HeaderLabel { get; } = new();
        private ObservableData<Color> HeaderColor { get; } = new();
        public void Init(DataProgressionCardGroup group)
        {
            HeaderLabel.Value = group.headerLabel;
            HeaderColor.Value = group.headerColor;
        }
    }
}