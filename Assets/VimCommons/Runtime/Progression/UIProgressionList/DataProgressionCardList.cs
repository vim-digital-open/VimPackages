using System.Collections.Generic;
using UnityEngine;

namespace VimCommons.Runtime.Progression.UIProgressionList
{
    [CreateAssetMenu]
    public class DataProgressionCardList : ScriptableObject
    {
        public string titleLabel;
        public Color titleColor;
        public List<DataProgressionCardGroup> groups;
    }
}