using System;
using System.Threading.Tasks;
using UnityEngine;
using VimCommons.Runtime.Ads.Rewarded;
using VimCommons.Runtime.Looting.Inventory;
using VimCommons.Runtime.Progression.Node;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Progression.UIProgressionList
{
    public class ProgressionCard: ModelBehaviour
    {
        private static ServiceInventory Inventory => Locator.Resolve<ServiceInventory>();
        private static IRewarded Rewarded => Locator.Resolve<IRewarded>();

        private ModelUILevelProgression _uiLevelProgression;
        public ModelUILevelProgression UILevelProgression => _uiLevelProgression ??= GetComponentInParent<ModelUILevelProgression>(); 

        private ProgressionNode _node;
        
        private ObservableData<string> Description { get; } = new();
        private ObservableData<string> LevelLabel { get; } = new();
        private ObservableData<string> CostLabel { get; } = new();
        private ObservableData<bool> BuyVisible { get; } = new();
        private ObservableData<bool> RwVisible { get; } = new();
        private ObservableData<Sprite> Icon { get; } = new();
        private ObservableData<string> Title { get; } = new();
        private Observable BtnBuy { get; } = new();
        private Observable BtnRewarded { get; } = new();

        private void Awake()
        {
            BtnBuy.AddListener(OnBuy);
            BtnRewarded.AddListener(OnRewarded);
        }

        public void Init(DataProgressionCard data)
        {
            _node = data.node;
            Icon.Value = data.icon;
            Title.Value = $"{data.title} {_node.NodeLevel.Value+1}";
            Description.Value = data.description;
            UpdateList();
        }

        public void UpdateList()
        {
            var curLevel = _node.NodeLevel.Value;
            LevelLabel.Value = $"{curLevel}/{_node.upgrades.Count}";
            if (curLevel >= _node.upgrades.Count)
            {
                CostLabel.Value = "MAX";
                BuyVisible.Value = false;
                RwVisible.Value = false;
                return;
            }
            var upgradeInfo = _node.NextUpgrade;
            CostLabel.Value = $"{upgradeInfo.BuildUpgradeLabel()}";
            var cost = _node.NextUpgrade.cost;
            var canPay = Inventory.CanPay(cost);
            BuyVisible.Value = canPay;
            var showRewarded = Rewarded!= null&& Rewarded.Ready.Value;
            RwVisible.Value = showRewarded;
        }

        private async void OnBuy()
        {
            var curLevel = _node.NodeLevel.Value;
            if (curLevel >= _node.upgrades.Count) return;
            var cost = _node.NextUpgrade.cost;
            if (Inventory.TryPay(cost))
            {
                _node.Upgrade();
                await Task.Delay(100);
                UILevelProgression.Refresh();
            }
        }

        private void OnRewarded()
        {
            var curLevel = _node.NodeLevel.Value;
            if (curLevel >= _node.upgrades.Count) return;
            Rewarded.Show(OnReward);
        }

        private async void OnReward()
        {
            _node.Upgrade();
            await Task.Delay(100);
            UILevelProgression.Refresh();
        }
    }
}