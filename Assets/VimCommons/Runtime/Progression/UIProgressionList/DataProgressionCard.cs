using System;
using UnityEngine;
using VimCommons.Runtime.Progression.Node;

namespace VimCommons.Runtime.Progression.UIProgressionList
{
    [Serializable]
    public struct DataProgressionCard
    {
        public Sprite icon;
        public string title;
        [Multiline]
        public string description;
        public ProgressionNode node;
        public bool IsUnlocked => node.IsUnlocked;
    }
}