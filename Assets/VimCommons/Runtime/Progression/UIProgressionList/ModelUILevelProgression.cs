using System.Collections.Generic;
using UnityEngine;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Progression.UIProgressionList
{
    public class ModelUILevelProgression: ModelBehaviour
    {
        public ProgressionCard cardPrefab;
        public ProgressionHeader headerPrefab;
        public Transform listParent;

        private static readonly ServiceContainer<ModelUILevelProgression> Container = Locator.Single<ModelUILevelProgression>();

        private ObservableData<bool> Visible { get; } = new();
        private ObservableData<string> TitleLabel { get; } = new();
        private ObservableData<Color> TitleColor { get; } = new();
        private Observable BtnClose { get; } = new();
        
        private void Awake()
        {
            Container.Attach(this);
            BtnClose.AddListener(OnClose);
        }

        private void OnDestroy() => Container.Detach(this);

        
        private List<GameObject> Cards { get; } = new();
        public DataProgressionCardList Content { get; set; }

        private void OnClose() => Hide();

        public void Show(DataProgressionCardList content)
        {
            Content = content;
            Refresh();
            Visible.Value = true;
        }
        
        public void Refresh()
        {
            Clean();
            TitleLabel.Value = Content.titleLabel;
            TitleColor.Value = Content.titleColor;
            foreach (var groupData in Content.groups)
            {
                
                if (groupData.IsUnlocked)
                {
                    var header = Instantiate(headerPrefab, listParent);
                    header.Init(groupData);
                    Cards.Add(header.gameObject);
                }
                foreach (var cardData in groupData.cards)
                {
                    if (cardData.IsUnlocked)
                    {
                        var card = Instantiate(cardPrefab, listParent);
                        card.Init(cardData);
                        Cards.Add(card.gameObject);
                    }
                }
            }
        }

        private void Clean()
        {
            foreach (var card in Cards)
                Destroy(card);
            Cards.Clear();
        }

        private void Hide() => Visible.Value = false;
    }
}