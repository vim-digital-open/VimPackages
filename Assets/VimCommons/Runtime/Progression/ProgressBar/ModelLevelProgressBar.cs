using UnityEngine;
using VimCommons.Runtime.Progression.Node;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Progression.ProgressBar
{
    public class ModelLevelProgressBar : ModelBehaviour
    {
        public string saveId;
        public int totalExperience;
        public ProgressionNode upgradeOnComplete;

        private static readonly ServiceContainer<ModelLevelProgressBar> Container = Locator.Single<ModelLevelProgressBar>();
        private void Awake() => Container.Attach(this);
        private void OnDestroy() => Container.Detach(this);

        public ObservableData<int> Progress { get; } = new();
        public ObservableData<float> BarValue { get; } = new();
        public ObservableData<string> CounterLabel { get; } = new();

        private void Start()
        {
            Progress.ConnectPref(saveId);
            AddExperience(0);
        }

        public void AddExperience(int amount)
        {
            Progress.Value = Mathf.Min(Progress.Value + amount, totalExperience);
            var percent = Progress.Value * 1f / totalExperience;
            BarValue.Value = percent;
            CounterLabel.Value = $"{Mathf.Ceil(percent * 100)}%";
            if (Progress.Value == totalExperience)
                upgradeOnComplete.Upgrade();
            PlayerPrefs.Save();
        }
    }
}