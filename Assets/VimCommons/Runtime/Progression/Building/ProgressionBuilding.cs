using VimCommons.Runtime.Progression.Node;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Progression.Building
{
    public class ProgressionBuilding : ModelBehaviour
    {
        public ProgressionNode node;
        protected ObservableData<int> NodeLevel => node.NodeLevel;
        protected Observable NodeUpgraded => node.NodeUpgraded;
    }
}