using System;
using System.Linq;
using UnityEngine;
using VimCommons.Runtime.Progression.Node;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Progression.Building
{
    [Serializable]
    public struct ProgressionChannel<TLevelData>
    {
        [SerializeField]
        private ProgressionNode node;
        [SerializeField]
        private TLevelData[] levels;

        public ObservableData<int> NodeLevel => node.NodeLevel;
        public Observable NodeUpgraded => node.NodeUpgraded;

        public TLevelData Level => levels.ElementAtOrDefault(NodeLevel.Value);
        public TLevelData[] Levels => levels;
    }
}
