using System;
using UnityEngine;
using VimCommons.Runtime.Looting.Core;
using VimCommons.Runtime.Looting.Inventory;

namespace VimCommons.Runtime.Progression.Node
{
    [Serializable]
    public class NodeLevelInfo
    {
        public LootEntry[] cost;

        public string BuildUpgradeLabel()
        {
            if (cost == null) return "";
            var result = "";
            foreach (var entry in cost)
            {
                if (entry.amount < 1) continue;
                result += $"{entry.amount}<sprite name={entry.type.iconLabel}>\n";
            }

            result = result.Trim();
            if (result.Length < 1) return "FREE";
            return result;
        }
        
        public string BuildEstimateLabel(LootAccount account)
        {
            if (cost == null) return "";
            var result = "";
            foreach (var entry in cost)
            {
                var balance = account.GetBalance(entry.type);
                var delta = entry.amount - balance.Value;
                if (delta < 1) continue;
                result += $"{delta}<sprite name={entry.type.iconLabel}>\n";
            }

            result = result.Trim();
            if (result.Length < 1) return "FREE";
            return result;
        }

        public string BuildRewardedBonus(float part = 1)
        {
            if (cost == null) return "";
            var result = "";
            foreach (var entry in cost)
            {
                if (entry.amount < 1) continue;
                result += $"{Mathf.CeilToInt(entry.amount * part)}<sprite name={entry.type.ToString().ToLower()}>\n";
            }
            return result.Trim();
        }

        public float CalculateProgress(LootAccount account)
        {
            var current = 0f;
            var total = 0f;
            foreach (var entry in cost)
            {
                var balance = account.GetBalance(entry.type);
                current += balance.Value;
                total += entry.amount;
            }
            return current / total;
        }
    }
}