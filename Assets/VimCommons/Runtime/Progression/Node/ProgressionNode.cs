using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using VimCommons.Runtime.Analytics.ServiceAnalytics;
using VimCommons.Runtime.Vibration.ServiceVibration;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Progression.Node
{
    [CreateAssetMenu]
    public class ProgressionNode : ScriptableObjectWithGuid
    {
        private List<ProgressionNode> ChildNodes { get; } = new();

        public ProgressionNode parent;
        public int parentLevel = 1;
        public int initialLevel;
        public List<NodeLevelInfo> upgrades;
        
        private static ServiceVibration Vibration => Locator.Resolve<ServiceVibration>();
        private static ServiceAnalytics Analytics => Locator.Resolve<ServiceAnalytics>();
        public ObservableData<int> NodeLevel { get; } = new();
        public Observable NodeUpgraded { get; } = new();
        public NodeLevelInfo NextUpgrade => upgrades.ElementAtOrDefault(NodeLevel.Value);
        public bool IsUnlocked => parent == null || parent.NodeLevel.Value >= parentLevel;

        private void OnEnable()
        {
            if (parent) 
                parent.ChildNodes.Add(this);
            NodeLevel.ConnectPref($"node_{guid}");
            NodeLevel.AddListener(OnLevel);
            OnParentLevel();
        }
        
        private void OnLevel(int obj)
        {
            if (!IsUnlocked) return;
            ChildNodes.ForEach(i => i.OnParentLevel());
        }

        private void OnParentLevel()
        {
            if (!IsUnlocked) return;
            if (NodeLevel.Value < initialLevel) 
                NodeLevel.Value = initialLevel;
            else
                NodeLevel.Touch();
        }

        public async void Upgrade()
        {
            if (!IsUnlocked) return;
            if (NodeLevel.Value > upgrades.Count - 1) return;
            NodeLevel.Value += 1;
            if (Vibration)
                Vibration.Heavy();
            if (Analytics)
                Analytics.Send(new EventNodeUpgraded(name, NodeLevel.Value));
            await UniTask.Yield();
            NodeUpgraded.Invoke();
        }
    }
}