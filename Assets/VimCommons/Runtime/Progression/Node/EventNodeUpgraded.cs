namespace VimCommons.Runtime.Progression.Node
{
    public class EventNodeUpgraded
    {
        public string Name { get; }
        public int Level { get; }

        public EventNodeUpgraded(string name, int level)
        {
            Name = name;
            Level = level;
        }
    }
}