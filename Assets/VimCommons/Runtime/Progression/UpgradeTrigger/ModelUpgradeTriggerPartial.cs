using UnityEngine;
using VimCommons.Runtime.Ads.InterstitialRunner;
using VimCommons.Runtime.Ads.Rewarded;
using VimCommons.Runtime.Looting.Core;
using VimCommons.Runtime.Looting.Inventory;
using VimCommons.Runtime.Progression.Node;
using VimCommons.Runtime.Triggers.TriggerSystem;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Progression.UpgradeTrigger
{
    public class ModelUpgradeTriggerPartial : ModelBehaviour
    {      
        public ProgressionNode node;
        
        public string label = "BUILD";
        public Sprite icon;
        public float payAcceleration = 0.02f;

        private static IInterstitialRunner Interstitial => Locator.Resolve<IInterstitialRunner>();
        private static IRewarded Rewarded => Locator.Resolve<IRewarded>();
        private static ServiceInventory Inventory => Locator.Resolve<ServiceInventory>();

        public ObservableData<int> NodeLevel => node.NodeLevel;
        public ObservableData<bool> UpgradeVisible { get; } = new();
        public ObservableData<bool> CanUpgrade { get; } = new();
        public ObservableData<string> UpgradeLabel { get; } = new();
        public ObservableData<float> UpgradeProgress { get; } = new();
        public ObservableData<string> LevelLabel { get; } = new();
        public ObservableData<string> UpgradeType { get; } = new();
        public ObservableData<Sprite> UpgradeImage { get; } = new();
        private Observable<SignalTrigger> ChannelTrigger { get; } = new();

        private NodeLevelInfo NextUpgrade => node.NextUpgrade;
        private LootAccount _account;
        public LootAccount Account => _account ??= new LootAccount($"upgrade_{node.guid}");

        private Vector3 ActivatorPos { get; set; }

        public float PayStep { get; set; }

        private void Start()
        {
            NodeLevel.AddListener(OnProgress);
            ChannelTrigger.AddListener(OnTrigger);
        }

        private void OnProgress(int obj)
        {
            CanUpgrade.Value = node.IsUnlocked && obj < node.upgrades.Count;
            LevelLabel.Value = obj < 1 ? label : $"LEVEL {obj}";
            UpgradeImage.Value = icon;
            UpgradeType.Value = label;
            if (NextUpgrade != null)
            {
                UpgradeLabel.Value = NextUpgrade.BuildEstimateLabel(Account);
                UpgradeProgress.Value = NextUpgrade.CalculateProgress(Account);
            }
        }
        
        public void OnTrigger(SignalTrigger sig)
        {
            switch (sig.State)
            {
                case TriggerState.Enter:
                    OnEnter(sig.Invoker);
                    break;
                
                case TriggerState.Stay:
                    OnStay(sig.Invoker);
                    break;
                
                case TriggerState.Exit:
                    OnExit(sig.Invoker);
                    break;
            }
        }
        
        public void OnEnter(TriggerInvoker signal)
        {
            if (!CanUpgrade.Value) return;
            if (NextUpgrade == null) return;
            UpgradeVisible.Value = true;
            PayStep = 1;
        }

        public void OnStay(TriggerInvoker signal)
        {
            var lastPos = ActivatorPos;
            ActivatorPos = signal.transform.position;
            if (!Helper.WithinRadius(lastPos, ActivatorPos, 0.01f)) return;
            if (!CanUpgrade.Value) return;
            if (NextUpgrade == null) return;
            if (!UpgradeVisible.Value) return;
            PayStep += payAcceleration;
            TickPay(signal);
        }
        
        public void OnExit(TriggerInvoker signal)
        {
            UpgradeVisible.Value = false;
        }

        private void TickPay(TriggerInvoker invoker)
        {
            Interstitial?.ResetIdle();
            var upgradeCost = NextUpgrade.cost;
            var sourcePoint = invoker.transform;
            foreach (var entry in upgradeCost)
            {
                var accumulatedBalance = Account.GetBalance(entry.type);
                var requirement = entry.amount - accumulatedBalance.Value;
                var inventoryBalance = Inventory.Account.GetBalance(entry.type);
                var batchAmount = Mathf.Min(PayStep, requirement, inventoryBalance.Value);
                if (batchAmount < 1) continue;
                var batch = new LootEntry(entry.type, Mathf.FloorToInt(batchAmount));
                if (Inventory.Account.TryPay(batch))
                {
                    Account.Receive(batch);
                    batch.Animate(sourcePoint, transform);
                    if (NextUpgrade != null)
                    {
                        UpgradeLabel.Value = NextUpgrade.BuildEstimateLabel(Account);
                        UpgradeProgress.Value = NextUpgrade.CalculateProgress(Account);
                    }
                    break;
                }
            }

            if (!Account.TryPay(upgradeCost)) return;
            if (NextUpgrade != null)
            {
                UpgradeLabel.Value = NextUpgrade.BuildEstimateLabel(Account);
                UpgradeProgress.Value = NextUpgrade.CalculateProgress(Account);
            }

            PayStep = 1;                
            node.Upgrade();
        }
    
        private void OnValidate()
        {
            if (node) name = $"Trigger{node.name}";
        }
    }
}