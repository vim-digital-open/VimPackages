using UnityEngine;
using VimCommons.Runtime.Ads.InterstitialRunner;
using VimCommons.Runtime.Ads.Rewarded;
using VimCommons.Runtime.Looting.Core;
using VimCommons.Runtime.Looting.Inventory;
using VimCommons.Runtime.Progression.Node;
using VimCommons.Runtime.Triggers.TriggerSystem;
using VimCommons.Runtime.Vibration.ServiceVibration;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Progression.UpgradeTrigger
{
    
    public class ModelUpgradeTrigger : ModelBehaviour
    {      
        public ProgressionNode node;
        
        public string label = "BUILD";
        public Sprite icon;

        public float duration = 3f;
        public float vibrationStep = 0.5f;

        private static readonly Filter<ModelUpgradeTrigger> Filter = Locator.Filter<ModelUpgradeTrigger>();
        private void OnEnable()
        {
            Filter.Add(this);
        }

        private void OnDisable() => Filter.Remove(this);
        
        private static IInterstitialRunner Interstitial => Locator.Resolve<IInterstitialRunner>();
        private static IRewarded Rewarded => Locator.Resolve<IRewarded>();
        private static ServiceInventory Inventory => Locator.Resolve<ServiceInventory>();
        private static ServiceVibration Vibration => Locator.Resolve<ServiceVibration>();

        private Collider _hitbox;
        public Collider Hitbox => _hitbox ??= GetComponent<Collider>(); 

        public ObservableData<int> NodeLevel => node.NodeLevel;
        public ObservableData<float> Accumulation { get; } = new();
        public ObservableData<bool> UpgradeVisible { get; } = new();
        public ObservableData<bool> CanPay { get; } = new();
        public ObservableData<bool> CanUpgrade { get; } = new();
        public ObservableData<string> UpgradeLabel { get; } = new();
        public ObservableData<string> LevelLabel { get; } = new();
        public ObservableData<string> RewardedBonus { get; } = new();
        public ObservableData<string> UpgradeType { get; } = new();
        public ObservableData<Sprite> UpgradeImage { get; } = new();
        private Observable<SignalTrigger> ChannelTrigger { get; } = new();

        private NodeLevelInfo NextUpgrade => node.NextUpgrade;

        private Vector3 ActivatorPos { get; set; }
        private void Awake() => ChannelTrigger.AddListener(OnTrigger);

        private void Start()
        {
            NodeLevel.AddListener(OnProgress);
        }

        private void OnProgress(int obj)
        {
            CanUpgrade.Value = node.IsUnlocked && obj < node.upgrades.Count;
            LevelLabel.Value = obj < 1 ? label : $"LEVEL {obj}";
            UpgradeImage.Value = icon;
            UpgradeType.Value = label;
            UpgradeLabel.Value = NextUpgrade?.BuildUpgradeLabel();
            RewardedBonus.Value = NextUpgrade?.BuildRewardedBonus(0.25f);
        }

        private float HapticTimer { get; set; }

        public void OnTrigger(SignalTrigger sig)
        {
            switch (sig.State)
            {
                case TriggerState.Enter:
                    OnEnter(sig.Invoker);
                    break;
                
                case TriggerState.Stay:
                    OnStay(sig.Invoker);
                    break;
                
                case TriggerState.Exit:
                    OnExit(sig.Invoker);
                    break;
            }
        }
        public void OnEnter(TriggerInvoker invoker)
        {
            if (!CanUpgrade.Value) return;
            if (NextUpgrade == null) return;
            var canPay = Inventory.CanPay(NextUpgrade.cost);
            CanPay.Value = canPay;
            UpgradeVisible.Value = true;
            Accumulation.Value = 0;
            HapticTimer = 0;
        }

        public void OnStay(TriggerInvoker invoker)
        {
            var lastPos = ActivatorPos;
            ActivatorPos = invoker.transform.position;
            if (!Helper.WithinRadius(lastPos, ActivatorPos, 0.01f)) return;
            if (NextUpgrade == null) return;
            if (!UpgradeVisible.Value) return;
            HapticTimer += Time.deltaTime;
            if (HapticTimer > vibrationStep)
            {
                HapticTimer = 0;
                Vibration.Light();
            }
            Accumulation.Value += Time.deltaTime / duration;
            if (Accumulation.Value < 1) return;
            OnExit(invoker);
            if(CanPay.Value)
                Buy(invoker);
            else
                ShowRewarded(invoker);
        }
        
        public void OnExit(TriggerInvoker invoker)
        {
            Accumulation.Value = 0;
            UpgradeVisible.Value = false;
        }
        
        private void Buy(TriggerInvoker invoker)
        {
            var playerPos = invoker.transform;
            var upgradeCost = NextUpgrade.cost;
            if (!Inventory.TryPay(upgradeCost)) return;
            foreach (var entry in upgradeCost)
                entry.Animate(playerPos, transform);

            node.Upgrade();
            Interstitial?.ResetIdle();
        }
    
        private void ShowRewarded(TriggerInvoker invoker) => Rewarded?.Show(()=>OnReward(invoker));

        private void OnReward(TriggerInvoker invoker)
        {
            var playerPos = invoker.transform;
            var upgradeCost = NextUpgrade.cost;
            node.Upgrade();
            foreach (var entry in upgradeCost)
            {
                var bonus = new LootEntry(entry.type, Mathf.CeilToInt(entry.amount * 0.25f));
                bonus.Animate(playerPos, playerPos);
                Inventory.Receive(bonus.type, bonus.amount);
            }
        }

        private void OnValidate()
        {
            if (node) name = $"Trigger{node.name}";
        }
    }
}
