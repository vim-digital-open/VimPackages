using DG.Tweening;
using TMPro;
using UnityEngine;
using VimCommons.Runtime.Camera.Core;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Transients.TransientLabels
{
    public class ServiceTransientLabelSystem : MonoBehaviour, ITransientLabelSystem
    {
        private static readonly ServiceContainer<ITransientLabelSystem> Container = Locator.Single<ITransientLabelSystem>();

        private static ServiceCamera Camera => Locator.Resolve<ServiceCamera>();
        
        public TMP_Text prefab;
        
        private void Awake() => Container.Attach(this);

        public void Emit(Vector3 position, string text, Color color, float size)
        {
            var from = position;
            var to = from + Vector3.up;

            var colorFrom = color;
            colorFrom.a = 0;
            var colorTo = color;
            
            var label = prefab.SpawnPoolable();
            label.transform.position = from;
            label.text = text;
            label.fontSize = size;
            label.transform.rotation = Camera.Transform.rotation;
            DOTween.Sequence().AppendLerp(ez =>
            {
                label.color = Color.Lerp(colorFrom, colorTo, ez.QuintOut);
                label.transform.position = Vector3.LerpUnclamped(from, to, ez.BackOut);
            }).AppendInterval(1).AppendLerp(ez =>
            {
                var t = ez.Linear;
                label.color = Color.Lerp(colorFrom, colorTo, 1-t);
            }).AppendCallback(() =>
            {
                prefab.RemovePoolable(label);
            });
        }
    }
}