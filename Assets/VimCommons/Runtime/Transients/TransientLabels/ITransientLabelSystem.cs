using UnityEngine;

namespace VimCommons.Runtime.Transients.TransientLabels
{
    public interface ITransientLabelSystem
    {
        void Emit(Vector3 position, string text, Color color = default, float size = 4);
    }
}