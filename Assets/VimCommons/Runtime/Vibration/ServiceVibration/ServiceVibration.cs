using UnityEngine;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Vibration.ServiceVibration
{
    public sealed class ServiceVibration: ModelBehaviour
    {
        private static readonly ServiceContainer<ServiceVibration> Container = Locator.Single<ServiceVibration>();

        private float _ready;

        public ObservableData<bool> IsActive { get; } = new();

        private void Awake()
        {
            Container.Attach(this);
            IsActive.ConnectPref("vibrationActive", true);
        }

        public bool Ready
        {
            get
            {
                if (Application.isEditor) return false;
                if (!IsActive.Value) return false;
                var now = Time.realtimeSinceStartup;
                if (now < _ready) return false;
                _ready = now + 0.03f;
                return true;
            }
        }

        public void Light()
        {
            if (!Ready) return;
            Taptic.Light();
        }

        public void Medium()
        {
            if (!Ready) return;
            Taptic.Medium();
        }

        public void Heavy()
        {
            if (!Ready) return;
            Taptic.Heavy();
        }

        
        public void Selection()
        {
            if (!Ready) return;
            Taptic.Selection();
        }

        public void Success()
        {
            if (!Ready) return;
            Taptic.Success();
        }

        public void Warning()
        {
            if (!Ready) return;
            Taptic.Warning();
        }

        public void Failure()
        {
            if (!Ready) return;
            Taptic.Failure();
        }
    }
}