using UnityEngine;
using UnityEngine.Playables;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Cutscene
{
    public class ModelCutscene : ModelBehaviour
    {
        public PlayableDirector Director { get; private set; }
        
        public ObservableData<bool> IsPlaying { get; } = new();
        public ObservableData<bool> FocusCamera { get; } = new();

        private void Awake()
        {
            Director = GetComponent<PlayableDirector>();
            Director.stopped += OnStop;
            OnStop(null);
        }

        private void OnStop(PlayableDirector obj)
        {
            gameObject.SetActive(false);
            IsPlaying.Value = false;
            FocusCamera.Value = false;
        }

        [ContextMenu("Play")]
        public void Play(bool focusCamera = false)
        {
            gameObject.SetActive(true);
            IsPlaying.Value = true;
            FocusCamera.Value = focusCamera;
            Director.Play();
        }
    }
}