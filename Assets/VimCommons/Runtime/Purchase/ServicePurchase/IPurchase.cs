using UnityEngine.Purchasing;
using VimCommons.Runtime.Purchase.IapProducts;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Purchase.ServicePurchase
{
    public interface IPurchase
    {
        void Purchase(ProductInfo productInfo);
        ObservableData<ProductCollection> ProductCollection { get; }
    }
}