namespace VimCommons.Runtime.Purchase.UIStore
{
    public interface IUIStore
    {
        void Show();
        void Hide();
    }
}