using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Purchase.UIStore
{
    public class ModelUIStore : ModelBehaviour, IUIStore
    {
        private static readonly ServiceContainer<IUIStore> Container = Locator.Single<IUIStore>();
        
        private ObservableData<bool> Visible { get; } = new();
        private Observable BtnHide { get; } = new();
        private void Awake() => Container.Attach(this);
        private void OnDestroy() => Container.Detach(this);
        private void Start() => BtnHide.AddListener(OnHide);
        
        public void Show() => Visible.Value = true;
        public void Hide() => Visible.Value = false;

        private void OnHide() => Hide();
    }
}