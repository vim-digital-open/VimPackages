using UnityEngine.Purchasing;
using VimCommons.Runtime.Purchase.ServicePurchase;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Purchase.IapProducts
{
    public class ModelProductCard: ModelBehaviour
    {
        public ProductInfo productInfo;
        
        private static IPurchase Purchase => Locator.Resolve<IPurchase>();

        private ObservableData<string> Name { get; } = new();
        private ObservableData<string> Description { get; } = new();
        private ObservableData<string> FakePrice { get; } = new();
        private ObservableData<string> Price { get; } = new();
        private Observable BtnPurchase { get; } = new();

        private void Awake() => BtnPurchase.AddListener(OnPurchase);

        private void Start() => Purchase.ProductCollection.AddListener(OnProductLoaded);
        private void OnDestroy() => Purchase.ProductCollection.RemoveListener(OnProductLoaded);

        private void OnProductLoaded(ProductCollection productCollection)
        {
            if (productCollection == null) return;
            var product = productCollection.WithID(productInfo.id);
            if (product == null) return;
            Name.Value = product.metadata.localizedTitle;
            Description.Value = product.metadata.localizedDescription;
            var currencyCode = product.metadata.isoCurrencyCode;
            var cost = product.metadata.localizedPrice;
            Price.Value = $"{cost} {currencyCode}";
            FakePrice.Value = $"{cost * 10 / 4} {currencyCode}";
        }

        private void OnPurchase() => Purchase.Purchase(productInfo);
    }
}