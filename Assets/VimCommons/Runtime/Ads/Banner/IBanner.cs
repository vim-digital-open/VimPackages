namespace VimCommons.Runtime.Ads.Banner
{
    public interface IBanner
    {
        bool Enabled { get; set; }
    }
}