namespace VimCommons.Runtime.Ads.InterstitialRunner
{
    public interface IInterstitialRunner
    {
        void AddCooldown();
        
        void ResetIdle();
        void UpdateLastAd();
    }
}