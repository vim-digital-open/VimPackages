using UnityEngine;
using UnityEngine.EventSystems;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Ads.Rewarded
{
    public class VMRewardedClick : AViewModel,
        IPointerDownHandler,
        IPointerUpHandler,
        IPointerClickHandler
    {
        private CanvasRenderer _renderer;
        public CanvasRenderer Renderer => _renderer ??= GetComponent<CanvasRenderer>(); 
        private static IRewarded Rewarded => Locator.Resolve<IRewarded>();
        public void OnPointerDown(PointerEventData eventData) => Renderer.SetColor(Color.gray);
        public void OnPointerUp(PointerEventData eventData) => Renderer.SetColor(Color.white);
        public void OnPointerClick(PointerEventData eventData) => Rewarded.Show(OnReward);

        private void OnReward() => Observable.Invoke();
    }
}