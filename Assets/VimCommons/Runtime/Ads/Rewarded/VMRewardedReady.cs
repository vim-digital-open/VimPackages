using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Ads.Rewarded
{
    public class VMRewardedReady : MonoBehaviour
    {
        private static IRewarded Rewarded => Locator.Resolve<IRewarded>();
        private void Awake() => Rewarded.Ready.AddListener(OnReady);
        private void OnReady(bool state) => gameObject.SetActive(state);
        private void OnDestroy()
        {
            if (Rewarded == null) return;
            Rewarded.Ready.RemoveListener(OnReady);
        }
    }
}