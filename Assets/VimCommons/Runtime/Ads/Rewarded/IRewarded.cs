using System;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Ads.Rewarded
{
    public interface IRewarded
    {
        ObservableData<bool> Ready { get; }
        void Show(Action callback);
    }
}