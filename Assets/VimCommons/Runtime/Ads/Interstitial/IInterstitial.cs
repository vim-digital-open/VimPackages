namespace VimCommons.Runtime.Ads.Interstitial
{
    public interface IInterstitial
    {
        void Show();
    }
}