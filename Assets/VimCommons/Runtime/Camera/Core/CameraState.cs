using UnityEngine;
using VimCommons.Runtime.Camera.CameraZoomArea;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Camera.Core
{
    public class CameraState : MonoBehaviour
    {
        public float speed = 3;
        public float distance = 20;

        private static ServiceCamera Camera => Locator.Resolve<ServiceCamera>();        
        private static readonly Filter<ComponentCameraZoomArea> ZoomAreas = Locator.Filter<ComponentCameraZoomArea>();


        private float _aspectRatio;
        public float Zoom { get; set; } = 1;
        public float Distance => distance * _aspectRatio * Zoom;

        private Transform _transform;
        public Transform Transform => _transform ??= transform;

        private void Awake() => _aspectRatio = Display.main.renderingHeight * 1f / Display.main.renderingWidth;

        private void OnDestroy() => Forget();

        public void Forget()
        {
            if (Camera)
                Camera.Forget(this);
        }

        public void Focus()
        {
            if (Camera)
                Camera.Focus(this);
        }

        public void Warp()
        {
            if (Equals(Camera, null)) return;
            Camera.Focus(this);
            Camera.Transform.position = Transform.position - Transform.forward * Distance;
            Camera.Transform.rotation = Transform.rotation;
        }

        public void Track(ServiceCamera cam)
        {
            if (Transform == null) return;

            var tm = speed * Time.deltaTime;
            UpdateZoom(tm);
            cam.Transform.position = Vector3.Lerp(cam.Transform.position, Transform.position - Transform.forward * Distance, tm);
            cam.Transform.rotation = Quaternion.LerpUnclamped(cam.Transform.rotation, Transform.rotation, tm);
        }

        private void UpdateZoom(float tm)
        {
            foreach (var area in ZoomAreas)
            {
                if (area.Hitbox.bounds.Contains(Transform.position))
                {
                    Zoom = Mathf.Lerp(Zoom, area.zoom, tm);
                    return;
                }
            }
            Zoom = Mathf.Lerp(Zoom, 1, tm);
        }
    }
}