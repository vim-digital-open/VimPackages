using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Camera.Core
{
    public class VMFocusCameraState: AViewModel<bool>
    {
        private CameraState _state;
        public CameraState State => _state ??= GetComponent<CameraState>();

        public override void OnValue(bool value)
        {
            if (value)
                State.Focus();
            else
                State.Forget();
        }
    }
}