using System.Collections.Generic;
using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Camera.Core
{
    public class ServiceCamera : MonoBehaviour
    {
        private static readonly ServiceContainer<ServiceCamera> Container = Locator.Single<ServiceCamera>();
        private void Awake() => Container.Attach(this);
        private void OnDestroy() => Container.Detach(this);

        
        private Transform _transform;
        public Transform Transform => _transform ??= transform;
        
        private UnityEngine.Camera _camera;
        public UnityEngine.Camera Camera => _camera ??= GetComponent<UnityEngine.Camera>();
        
        private LinkedList<CameraState> States { get; } = new();

        private void LateUpdate()
        {
            if (States.Count < 1) return;
            States.First.Value.Track(this);
        }

        public void Focus(CameraState cameraState) => States.AddFirst(cameraState);
        public void Forget(CameraState cameraState) => States.Remove(cameraState);

        public bool CantSee(Transform point)
        {
            var screenPoint = Camera.WorldToViewportPoint(point.position);
            return screenPoint.y is < 0 or > 1 || screenPoint.x is < 0 or > 1;
        }
    }
}