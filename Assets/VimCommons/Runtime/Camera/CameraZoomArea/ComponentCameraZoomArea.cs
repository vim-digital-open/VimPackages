using UnityEngine;
using VimCommons.Runtime.Camera.Core;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Camera.CameraZoomArea
{
    public class ComponentCameraZoomArea : MonoBehaviour
    {
        public float zoom = 1;
        
        private static readonly Filter<ComponentCameraZoomArea> Filter = Locator.Filter<ComponentCameraZoomArea>();

        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);
        
        private static ServiceCamera Camera => Locator.Resolve<ServiceCamera>();

        private Collider _hitbox;
        public Collider Hitbox => _hitbox ??= GetComponent<Collider>();
    }
}