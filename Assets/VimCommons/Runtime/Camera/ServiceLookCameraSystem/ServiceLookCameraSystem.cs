using System.Linq;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;
using VimCommons.Runtime.Camera.Core;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Camera.ServiceLookCameraSystem
{
    public class ServiceLookCameraSystem: MonoBehaviour
    {
        private static readonly Filter<LookCameraComponent> LookCameraComponents = Locator.Filter<LookCameraComponent>();
        private static ServiceCamera Camera => Locator.Resolve<ServiceCamera>();
        
        public static bool IsDirty { get; set; }

        
        private JobHandle _jobHandle;
        private TransformAccessArray _native;
        private void Start() => TickNative();

        private void OnDestroy() => _native.Dispose();

        private void Update()
        {
            var job = new LookCameraJob
            {
                Rotation = Camera.Transform.rotation,
            };
            _jobHandle = job.Schedule(_native);
        }

        private void LateUpdate()
        {
            _jobHandle.Complete();
            var oddFrame = (Time.frameCount % 16) < 1;
            if (oddFrame|| (!IsDirty)) return;
            IsDirty = false;
            TickNative();
        }


        private void TickNative()
        {
            var arr = LookCameraComponents.Select(i => i.Transform).ToArray();
            if(_native.isCreated)
                _native.Dispose();
            _native = new TransformAccessArray(arr);
        }
    }
}