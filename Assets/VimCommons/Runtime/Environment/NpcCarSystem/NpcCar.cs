using UnityEngine;
using UnityEngine.Splines;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Environment.NpcCarSystem
{
    public class NpcCar: ModelBehaviour
    {
        private const float Speed = 10;

        public Transform[] visualVariants;
        
        private static readonly Filter<NpcCar> Filter = Locator.Filter<NpcCar>();

        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);

        public NpcCar Prefab { get; set; }
        public SplineContainer Path { get; set; }
        public float Length { get; set; }

        public float Progress { get; set; }
        private ObservableData<bool> IsRide { get; } = new();

        private void Awake()
        {
            var prefab = visualVariants.GetRandomItem();
            var model = Instantiate(prefab, transform);
            model.localPosition = Vector3.zero;
            model.localRotation = Quaternion.identity;
            
        }

        public void Init(NpcCar prefab, SplineContainer spline)
        {
            Prefab = prefab;
            Path = spline;
            Length = Path.CalculateLength();
            Progress = 0;
            IsRide.Value = true;
        }



        public void Tick()
        {
            if (Progress < 1)
            {
                Progress += Speed * Time.deltaTime / Length;
                transform.position = Path.EvaluatePosition(Progress);
                transform.forward = Path.EvaluateTangent(Progress);
                return;
            }
            Remove();
        }

        private void Remove() => Prefab.RemovePoolable(this);
    }
}