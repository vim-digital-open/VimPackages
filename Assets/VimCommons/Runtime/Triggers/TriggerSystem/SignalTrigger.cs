namespace VimCommons.Runtime.Triggers.TriggerSystem
{
    public class SignalTrigger
    {
        public TriggerInvoker Invoker { get; }
        public TriggerState State { get; }

        public SignalTrigger(TriggerArea triggerArea, TriggerInvoker invoker, TriggerState state)
        {
            Invoker = invoker;
            State = state;
        }
    }
}