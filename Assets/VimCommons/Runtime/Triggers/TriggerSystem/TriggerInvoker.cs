using UnityEngine;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Triggers.TriggerSystem
{
    public class TriggerInvoker: AViewModel<SignalTrigger>
    {
        private Transform _transform;
        public Transform Transform => _transform ??= transform;
        
        private static readonly Filter<TriggerInvoker> Filter = Locator.Filter<TriggerInvoker>();
        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);

        public void Emit(SignalTrigger signal) => Observable.Invoke(signal);
    }
}