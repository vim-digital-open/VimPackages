namespace VimCommons.Runtime.Triggers.TriggerSystem
{
    public enum TriggerState { Enter, Stay, Exit }
}