using System.Collections.Generic;
using UnityEngine;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Triggers.TriggerSystem
{
    public class TriggerArea : AViewModel<SignalTrigger>
    {
        private static readonly Filter<TriggerInvoker> Invokers = Locator.Filter<TriggerInvoker>();
        
        private Collider _hitbox;
        public Collider Hitbox => _hitbox ??= GetComponentInChildren<Collider>(true);

        private HashSet<TriggerInvoker> InvokersInside { get; } = new();
        
        public void Update()
        {
            var bounds = Hitbox.bounds;
            foreach (var invoker in Invokers)
            {
                var invokerPos = invoker.Transform.position;
                if (bounds.Contains(invokerPos))
                {
                    if (InvokersInside.Add(invoker))
                        Emit(this, invoker, TriggerState.Enter);
                    else
                        Emit(this, invoker, TriggerState.Stay);
                }
                else
                {
                    if (InvokersInside.Remove(invoker))
                        Emit(this, invoker, TriggerState.Exit);
                }
            }

            foreach (var invoker in InvokersInside)
            {
                if (Invokers.Contains(invoker)) continue;
                InvokersInside.Remove(invoker);
                Emit(this, invoker, TriggerState.Exit);
                return;
            }
        }

        private void Emit(TriggerArea area, TriggerInvoker invoker, TriggerState state)
        {
            var signal = new SignalTrigger(area, invoker, state);
            Observable.Invoke(signal);
            invoker.Emit(signal);
        }
    }
}