using System.Collections.Generic;
using UnityEngine;

namespace VimCommons.Runtime.LevelManagement.Bootstrapper
{
    [CreateAssetMenu]
    public class GlobalServices : ScriptableObject
    {
        public GameObject[] global;
        public GameObject core;
        public bool debugMode;
        public GameObject[] debug;

        private GameObject Core { get; set; }

        public void Init()
        {
            if (Core) return;
            SpawnServices(global);
            Core = Spawn(core);

            if (debugMode || Application.isEditor)
                SpawnServices(debug);
        }
        private static void SpawnServices(IEnumerable<GameObject> services)
        {
            foreach (var service in services) 
                Spawn(service);
        }

        private static GameObject Spawn(GameObject service)
        {
            var instance = Instantiate(service);
            instance.name = $"~ {service.name} ~";
            DontDestroyOnLoad(instance);
            return instance;
        }
    }
}