using UnityEngine;

namespace VimCommons.Runtime.LevelManagement.Bootstrapper
{
    [DefaultExecutionOrder(int.MinValue)]
    public class GlobalServicesLoader : MonoBehaviour
    {
        public GlobalServices services;
        private void Awake()
        {
            services.Init();
        }
    }
}