using VimCommons.Runtime.LevelManagement.Core;
using VimCommons.Runtime.Progression.Building;
using VimCommons.Runtime.Triggers.TriggerSystem;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.LevelManagement.NextLevel
{
    public class ModelNextLevelBuilding : ProgressionBuilding
    {
        private static ServiceLevelManager Core => Locator.Resolve<ServiceLevelManager>();
        public Observable<SignalTrigger> ChannelNextLevelTrigger { get; } = new();

        private void Awake()
        {
            
            ChannelNextLevelTrigger.AddListener(OnNextLevelTrigger);
        }

        private void OnNextLevelTrigger(SignalTrigger signal)
        {
            switch (signal.State)
            {
                case TriggerState.Enter:
                           Core.FinishLevel();
                    break;
            }
        }
    }
}