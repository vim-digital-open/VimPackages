using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using VimCommons.Runtime.LevelManagement.Core.States;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.StateMachine;

namespace VimCommons.Runtime.LevelManagement.Core
{
    public class ServiceLevelManager: MonoBehaviour
    {
        public int loaderIndex;
        
        private static readonly ServiceContainer<ServiceLevelManager> Container = Locator.Single<ServiceLevelManager>();

        private readonly FSM<ServiceLevelManager, SLevelManagerAbstract> _fsm = new();

        public ObservableData<int> Level { get; } = new();

        private void Awake()
        {
            Container.Attach(this);
            
            Level.ConnectPref("Level");
            _fsm.Init(this, new SLevelManagerInit());
        }
        
        private int LevelCount => SceneManager.sceneCountInBuildSettings - loaderIndex - 1;

        public async Task LoadLevel(int level)
        {
            var sceneIndex = loaderIndex + 1 + level % LevelCount;
            if (SceneManager.GetActiveScene().buildIndex == sceneIndex) return;
            if (SceneManager.GetActiveScene().buildIndex > 0) 
                await Unload();
            var loading = SceneManager.LoadSceneAsync(sceneIndex);
            while (!loading.isDone) 
                await UniTask.Delay(100, DelayType.UnscaledDeltaTime);
            await UniTask.Delay(1000, DelayType.UnscaledDeltaTime);
        }

        public async Task Unload()
        {
            var loading = SceneManager.LoadSceneAsync(loaderIndex);
            while (!loading.isDone) 
                await UniTask.Delay(100, DelayType.UnscaledDeltaTime);
            await UniTask.Delay(1000, DelayType.UnscaledDeltaTime);
        }

        public void FinishLevel()
        {
            _fsm.State.FinishLevel();
        }
    }
}