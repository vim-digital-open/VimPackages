using VimCore.Runtime.StateMachine;

namespace VimCommons.Runtime.LevelManagement.Core.States
{
    public abstract class SLevelManagerAbstract: AState<ServiceLevelManager,SLevelManagerAbstract>
    {
        public virtual void FinishLevel()
        {
        }
    }
}