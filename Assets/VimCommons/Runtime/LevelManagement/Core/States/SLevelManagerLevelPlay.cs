
namespace VimCommons.Runtime.LevelManagement.Core.States
{
    public class SLevelManagerLevelPlay : SLevelManagerAbstract
    {
        public override void FinishLevel() => ChangeState<SLevelManagerLevelWin>();

    }
}