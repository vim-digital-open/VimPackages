namespace VimCommons.Runtime.LevelManagement.Core.States
{
    public class SLevelManagerLevelStart: SLevelManagerAbstract
    {
        public override void Enter() => ChangeState<SLevelManagerLevelPlay>();
    }
}