using UnityEngine;
using VimCommons.Runtime.LevelManagement.Splashscreen;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.LevelManagement.Core.States
{
    public class SLevelManagerInit : SLevelManagerAbstract
    {
        private static ModelSplashscreen Splashscreen => Locator.Resolve<ModelSplashscreen>();

        public override async void Enter()
        {
            if (Splashscreen)
                await Splashscreen.Show("PLEASE WAIT");
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;
            Application.backgroundLoadingPriority = ThreadPriority.Low;
            ChangeState<SLevelManagerLevelLoad>();
        }
    }
}