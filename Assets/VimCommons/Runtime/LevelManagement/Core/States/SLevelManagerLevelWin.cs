using UnityEngine;

namespace VimCommons.Runtime.LevelManagement.Core.States
{
    public class SLevelManagerLevelWin : SLevelManagerAbstract
    {
        public override void Enter()
        {
            Subject.Level.Value += 1;
            PlayerPrefs.Save();
            ChangeState<SLevelManagerLevelLoad>();
        }
    }
}