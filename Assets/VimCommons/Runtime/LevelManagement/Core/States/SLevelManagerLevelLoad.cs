using Cysharp.Threading.Tasks;
using VimCommons.Runtime.LevelManagement.Splashscreen;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.LevelManagement.Core.States
{
    public class SLevelManagerLevelLoad : SLevelManagerAbstract
    {
        private static ModelSplashscreen Splashscreen => Locator.Resolve<ModelSplashscreen>();

        public override async void Enter()
        {
            if (Splashscreen)
                await Splashscreen.Show("PLEASE WAIT");
            await Subject.LoadLevel(Subject.Level.Value);
            await UniTask.Delay(1000, DelayType.UnscaledDeltaTime);
            if (Splashscreen)
                Splashscreen.Hide();
            ChangeState<SLevelManagerLevelStart>();
        }
    }
}