using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.LevelManagement.Splashscreen
{
    
    public class ModelSplashscreen: ModelBehaviour
    {
        private static readonly ServiceContainer<ModelSplashscreen> Container = Locator.Single<ModelSplashscreen>();
        private ObservableData<bool> Visible { get; } = new();
        private ObservableData<string> Message  { get; } = new();
        private ObservableData<float> Progress  { get; } = new();

        public void Awake()
        {
            Container.Attach(this);
        }

        private void OnDestroy()
        {
            Container.Detach(this);
        }

        public async Task Show(string label)
        {
            Message.Value = label;
            if (!Visible.Value)
            {
                Visible.Value = true;
                Progress.Value = 0;
                DOTween.Sequence().AppendLerp(5, data =>
                {
                    Progress.Value = data.QuadOut;
                });
            }
            await UniTask.Delay(200, DelayType.UnscaledDeltaTime);
        }
        
        public void Hide() => Visible.Value = false;
    }
}