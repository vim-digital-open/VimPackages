namespace VimCommons.Runtime.Navigation.NavMeshSurfaceWrapper
{
    public interface INMSurfaceWrapper
    {
        void SetDirty();
    }
}