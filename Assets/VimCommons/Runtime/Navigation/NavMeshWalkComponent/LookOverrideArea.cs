using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Navigation.NavMeshWalkComponent
{
    internal class LookOverrideArea: MonoBehaviour
    {
        public Transform lookTarget;
        private static readonly Filter<LookOverrideArea> Filter = Locator.Filter<LookOverrideArea>();
        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);
        private Collider _hitbox;
        public Collider Hitbox => _hitbox ??= GetComponent<Collider>();
    }
}