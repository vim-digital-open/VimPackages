using UnityEngine;
using UnityEngine.AI;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Navigation.NavMeshWalker
{
    public abstract class ModelNavMeshWalker: ModelBehaviour
    {
        private NavMeshAgent _agent;
        public NavMeshAgent Agent => _agent ??= GetComponent<NavMeshAgent>();
        
        public ObservableData<float> Speed { get; } = new();

        public void Update()
        {
            if (!Agent.isOnNavMesh) return;
            Speed.Value = Agent.velocity.magnitude / Agent.speed;
            if (Agent.velocity.magnitude > 0.5f)
            {
                var lookDir = Agent.velocity;
                lookDir.y = 0;
                transform.forward = Vector3.Lerp(transform.forward, lookDir, 0.1f);
            }

            var targetPos = Target;
            if (!Agent.isActiveAndEnabled) return;
            Agent.SetDestination(targetPos);
        }

        protected abstract Vector3 Target { get; }

        protected void InitializeAgent(Vector3 pos, Quaternion rot)
        {
            if (_agent == null)
                _agent = GetComponent<NavMeshAgent>();

            Agent.Warp(pos);
            transform.rotation = rot;
        }
    }
}