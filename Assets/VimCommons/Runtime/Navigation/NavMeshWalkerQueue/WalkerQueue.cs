using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Splines;
using VimCommons.Runtime.Navigation.NavMeshWalker;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.Navigation.NavMeshWalkerQueue
{
    public class WalkerQueue : MonoBehaviour
    {
        public float step = 0.2f;

        private SplineContainer _spline;
        private SplineContainer Spline => _spline ??= GetComponent<SplineContainer>(); 
        
        private List<ModelNavMeshWalker> Queue { get; } = new();
        
        public void Enqueue(ModelNavMeshWalker walker)
        {
            if (Queue.Contains(walker)) return;
            Queue.Add(walker);
        }

        public void Dequeue(ModelNavMeshWalker walker) => Queue.Remove(walker);

        public int IndexOf(ModelNavMeshWalker walker) => Queue.IndexOf(walker);

        public bool CanCheckout(ModelNavMeshWalker walker)
        {
            var index = IndexOf(walker);
            if (index > 0) return false;
            var pos = Spline.EvaluatePosition(0);
            return Helper.WithinRadius(pos, walker.transform.position, walker.Agent.stoppingDistance + 0.1f);
        }

        public bool CheckoutReady()
        {
            if (Queue.Count < 1) return false;
            var walker = Queue[0];
            var pos = Spline.EvaluatePosition(0);
            return Helper.WithinRadius(pos, walker.transform.position, walker.Agent.stoppingDistance + 0.1f);
        }

        public Vector3 WaitPosition(ModelNavMeshWalker walker)
        {
            var index = IndexOf(walker);
            return Spline.EvaluatePosition(index * step);
        }

        public int Length => Queue.Count;
    }
}