using System.Linq;
using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.ClothInteraction
{
    public class ClothHandler : MonoBehaviour
    {
        private static readonly Filter<ClothEffector> Effectors = Locator.Filter<ClothEffector>();
        private void Awake() => Effectors.AddListener(OnEffectors);
        private void OnDestroy() => Effectors.RemoveListener(OnEffectors);

        private Cloth _cloth;
        public Cloth Cloth => _cloth ??= GetComponent<Cloth>(); 

        private void OnEffectors(ClothEffector[] effectors)
        {
            Cloth.sphereColliders = effectors
                .Select(effector => new ClothSphereColliderPair(effector.Collider, effector.Collider))
                .ToArray();
        }
    }
}