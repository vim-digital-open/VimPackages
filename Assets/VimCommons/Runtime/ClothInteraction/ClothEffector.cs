using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.ClothInteraction
{
    public class ClothEffector : MonoBehaviour
    {
        private static readonly Filter<ClothEffector> Filter = Locator.Filter<ClothEffector>();
        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);
        
        private SphereCollider _collider;
        public SphereCollider Collider => _collider ??= GetComponent<SphereCollider>(); 
    }
}