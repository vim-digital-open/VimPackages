using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.FollowerSystem.Core
{
    public class ModelFollowerRecycler : ModelBehaviour
    {
        private Observable<ModelFollowable> ChannelFollowable { get; } = new();

        private void Awake() => ChannelFollowable.AddListener(OnFollowable);

        public void OnFollowable(ModelFollowable followable)
        {
            if (followable.TryPop(out var follower)) 
                follower.Remove();
        }
    }
}