using UnityEngine;
using UnityEngine.AI;

namespace VimCommons.Runtime.FollowerSystem.Core
{
    public class ModelFollower : MonoBehaviour
    {
        public FollowerDefinition Definition { get; set; }
        private NavMeshAgent _agent;
        public NavMeshAgent Agent => _agent ??= GetComponent<NavMeshAgent>(); 
        public void Build(FollowerDefinition definition)
        {
            Definition = definition;
        }

        public void Warp(Vector3 point) => Agent.Warp(point);

        public void SetTarget(ModelFollowable followable)
        {
            Target = followable;
        }

        public ModelFollowable Target { get; set; }

        private void Update()
        {
            if (Target)
                Agent.SetDestination(Target.Transform.position);
        }

        public void Remove()
        {
            Target = null;
        }
    }
}