using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.FollowerSystem.Core
{
    public class ModelFollowable : MonoBehaviour
    {
        private Transform _transform;
        public Transform Transform => _transform ??= GetComponent<Transform>(); 
        
        private static readonly Filter<FollowableInteractor> Interactors = Locator.Filter<FollowableInteractor>();

        private static readonly HashSet<ModelFollower> Followers = new();

        private void Update()
        {
            
            foreach (var interactor in Interactors) 
                interactor.Interact(this);
        }

        public bool TryPush(ModelFollower follower)
        {
            follower.SetTarget(this);
            Followers.Add(follower);
            return true;
        }

        public bool TryPop(out ModelFollower result)
        {
            result = null;
            if (Followers.Count < 1) return false;
            result = Followers.First();
            Followers.Remove(result);
            return true;
        }
    }
}