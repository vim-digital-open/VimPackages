using UnityEngine;
using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.FollowerSystem.Core
{
    public class FollowableInteractor : AViewModel<ModelFollowable>
    {
        private static readonly Filter<FollowableInteractor> Filter = Locator.Filter<FollowableInteractor>();
        private void OnEnable() => Filter.Add(this);
        private void OnDisable() => Filter.Remove(this);

        private Collider _hitbox;
        public Collider Hitbox => _hitbox ??= GetComponent<Collider>();


        public void Interact(ModelFollowable followable)
        {
            if (!Hitbox.bounds.Contains(followable.Transform.position)) return;
            Observable.Invoke(followable);
        }
    }
}