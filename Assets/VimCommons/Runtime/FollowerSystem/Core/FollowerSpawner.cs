using System.Collections.Generic;
using UnityEngine;
using VimCore.Runtime.MVVM;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.FollowerSystem.Core
{
    public class FollowerSpawner : ModelBehaviour
    {
        public FollowerDefinition definition;
        
        public int maxAmount = 5;

        public float minTimeout = 1;
        public float maxTimeout = 2;
        
        public float pickDistance = 1;

        private float _timeout;

        private static readonly HashSet<ModelFollower> Content = new();
        
        private Collider _spawnArea;
        public Collider SpawnArea => _spawnArea ??= GetComponent<Collider>();

        private Observable<ModelFollowable> ChannelFollowable { get; } = new();

        private void Awake() => ChannelFollowable.AddListener(OnFollowable);

        private void Update()
        {
            if (Content.Count >= maxAmount) return;

            _timeout -= Time.deltaTime;
            if (_timeout > 0) return;
            _timeout = Random.Range(minTimeout, maxTimeout);
            Spawn();
        }

        private void Spawn()
        {
            var minion = definition.Spawn();
            Content.Add(minion);

            var point = SpawnArea.bounds.RandomPointInBounds();
            minion.Warp(point);
        }

        public void OnFollowable(ModelFollowable followable)
        {
            var anchor = followable.Transform.position;
            foreach (var follower in Content)
            {
                if (Helper.WithinRadius(anchor, follower.transform.position, pickDistance))
                {
                    if (followable.TryPush(follower))
                    {
                        Content.Remove(follower);
                        return;
                    }
                }
            }
        }
    }
}