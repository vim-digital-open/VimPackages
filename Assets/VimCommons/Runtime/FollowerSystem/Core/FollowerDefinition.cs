using UnityEngine;
using VimCore.Runtime.Pooling;
using VimCore.Runtime.Utils;

namespace VimCommons.Runtime.FollowerSystem.Core
{
    [CreateAssetMenu]
    public class FollowerDefinition : ScriptableObjectWithGuid
    {
        public ModelFollower prefab;
        
        public ModelFollower Spawn()
        {
            var instance = prefab.SpawnPoolable();
            instance.Build(this);
            return instance;
        }
        
        public void Remove(ModelFollower instance)
        {
            prefab.RemovePoolable(instance);
        }
    }
}