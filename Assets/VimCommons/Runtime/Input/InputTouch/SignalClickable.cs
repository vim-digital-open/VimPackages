namespace VimCommons.Runtime.Input.InputTouch
{
    public interface IClickable
    {
        public void OnClick();
    }
}