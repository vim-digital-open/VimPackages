using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using VimCommons.Runtime.Camera.Core;
using VimCore.Runtime.DependencyManagement;

namespace VimCommons.Runtime.Input.InputTouch
{
    public class ServiceInputTouch : MonoBehaviour, 
        IPointerDownHandler, 
        IPointerUpHandler, 
        IPointerMoveHandler,
        IPointerClickHandler
    {
        private static readonly ServiceContainer<ServiceInputTouch> Container = Locator.Single<ServiceInputTouch>();

        private static ServiceCamera Camera => Locator.Resolve<ServiceCamera>();

        public float sensitivity = 5f;
        public RectTransform anchor, stick;

        private bool _pressed;
        private bool _btnPressed;
        private JoystickState _joyValue;
        
        private LinkedList<Action<JoystickState>> Listeners { get; } = new();
        public void Register(Action<JoystickState> listener)
        {
            Release();
            Listeners.AddFirst(listener);
        }

        public void Release(Action<JoystickState> listener)
        {
            Release();
            Listeners.Remove(listener);
        }

        private Action<JoystickState> FocusedListener
        {
            get
            {
                if (Listeners.Count>0)
                    return Listeners.First.Value;
                return null;
            }
        }

        private void Awake()
        {
            Container.Attach(this);
            anchor.gameObject.SetActive(false);
            stick.gameObject.SetActive(false);
        }

        private void OnDestroy() => Container.Detach(this);

        public void OnPointerDown(PointerEventData eventData)
        {
            _pressed = true;
            _joyValue.action = JoystickEvent.Press;
            _joyValue.value = Vector2.zero;
            anchor.gameObject.SetActive(true);
            stick.gameObject.SetActive(true);
            anchor.anchoredPosition = eventData.position;
            stick.anchoredPosition = eventData.position;
            FocusedListener?.Invoke(_joyValue);
        }

        public void OnPointerMove(PointerEventData eventData)
        {
            if (!_pressed) return;
            _joyValue.action = JoystickEvent.Move;
            _joyValue.value = (eventData.position - eventData.pressPosition) * 0.001f * sensitivity;
            _joyValue.value = Vector2.ClampMagnitude(_joyValue.value,1); 
            var stickpos = Vector3.MoveTowards(eventData.pressPosition, eventData.position, 100);
            stick.anchoredPosition = stickpos;
            FocusedListener?.Invoke(_joyValue);
        }

        public void OnPointerUp(PointerEventData eventData)
        {           
            _pressed = false;
            _joyValue.action = JoystickEvent.Release;
            _joyValue.value = Vector2.zero;
            anchor.gameObject.SetActive(false);
            stick.gameObject.SetActive(false);
            FocusedListener?.Invoke(_joyValue);
        }
        
        public void OnPointerClick(PointerEventData eventData)
        {
            var ray = Camera.Camera.ScreenPointToRay(eventData.position);
            var hit = Physics.Raycast(ray, out var info);
            if (!hit) return;
            if (info.collider.TryGetComponent<IClickable>(out var clickable)) 
                clickable.OnClick();
        }


        public void Release() => OnPointerUp(null);

        private void Update()
        {
            if (Equals(null, FocusedListener)) return;
            var x = UnityEngine.Input.GetAxis("Horizontal");
            var y = UnityEngine.Input.GetAxis("Vertical");
            var joy = new Vector2(x, y);
            
            switch (_btnPressed, joy.sqrMagnitude)
            {
                case (true,0):
                    _btnPressed = false;
                    _joyValue.action = JoystickEvent.Release;
                    _joyValue.value = Vector2.zero;
                    FocusedListener?.Invoke(_joyValue);
                    break;
                
                case (true,>0):
                    _joyValue.value = joy;
                    _joyValue.action = JoystickEvent.Move;
                    FocusedListener?.Invoke(_joyValue);
                    break;
                
                case (false,>0):
                    _btnPressed = true;
                    _joyValue.action = JoystickEvent.Press;
                    _joyValue.value = Vector2.zero;
                    FocusedListener?.Invoke(_joyValue);
                    break;
            }
        }
    }
}