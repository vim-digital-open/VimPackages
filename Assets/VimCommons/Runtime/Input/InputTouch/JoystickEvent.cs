using System;

namespace VimCommons.Runtime.Input.InputTouch
{
    [Serializable]
    public enum JoystickEvent
    {
        Press,
        Move,
        Release
    }
}