using VimCore.Runtime.DependencyManagement;
using VimCore.Runtime.MVVM;

namespace VimCommons.Runtime.Input.InputTouch
{
    public class VMTouchInputLocker : AViewModel<bool>
    {
        private static ServiceInputTouch Input => Locator.Resolve<ServiceInputTouch>();
        public override void OnValue(bool value)
        {
            if (value)
                Input.Register(OnJoystick);
            else
                Input.Release(OnJoystick);
        }

        private void OnJoystick(JoystickState obj){ }
    }
}