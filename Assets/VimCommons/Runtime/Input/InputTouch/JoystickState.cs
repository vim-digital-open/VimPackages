using System;
using UnityEngine;

namespace VimCommons.Runtime.Input.InputTouch
{
    [Serializable]
    public struct JoystickState
    { 
        public Vector2 value;
        public JoystickEvent action;
    }
}