using UnityEditor;
using UnityEngine;
using VimCommons.Runtime.Camera.Core;

namespace VimCommons.Editor.Camera
{
    [CustomEditor(typeof(CameraState),true)]
    [CanEditMultipleObjects]
    public class CameraStateEditor : UnityEditor.Editor
    {
        
        public override void OnInspectorGUI()
        {
            var state = target as CameraState;
            if (GUILayout.Button("Preview"))
            {
                var cam = SceneView.lastActiveSceneView;
                var pos = state.transform.position;
                var rot = state.transform.rotation;
                cam.LookAt(pos, rot, state.distance/3);
            }
            DrawDefaultInspector();
        }
    }
}